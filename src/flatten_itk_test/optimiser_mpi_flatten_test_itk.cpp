/*
  code here is for testing case, in the MPI condition of mloptimiser
  of autorefine
  in RELION
*/

// testing section for the solventFlatten function using ITK library:

void MlOptimiserMpi::solventFlatten()
{

#ifdef DEBUG
	std::cerr << "Entering MlOptimiser::solventFlatten" << std::endl;
#endif

	// If not use ITK library in this function, then perform normal solvent flatten function process
	if(!use_itk_flatten)
	{
		// First read solvent mask from disc, or pre-calculate it
		Image<DOUBLE> Isolvent, Isolvent2;
	    Isolvent().resize(mymodel.Iref[0]);
		Isolvent().setXmippOrigin();
		Isolvent().initZeros();
		if (fn_mask.contains("None"))
		{
			DOUBLE radius = particle_diameter / (2. * mymodel.pixel_size);
			DOUBLE radius_p = radius + width_mask_edge;
			FOR_ALL_ELEMENTS_IN_ARRAY3D(Isolvent())
			{
				DOUBLE r = sqrt((DOUBLE)(k*k + i*i + j*j));
				if (r < radius)
					A3D_ELEM(Isolvent(), k, i, j) = 1.;
				else if (r > radius_p)
					A3D_ELEM(Isolvent(), k, i, j) = 0.;
				else
				{
					A3D_ELEM(Isolvent(), k, i, j) = 0.5 - 0.5 * cos(PI * (radius_p - r) / width_mask_edge );
				}
			}
		}
		else
		{
			Isolvent.read(fn_mask);
			Isolvent().setXmippOrigin();

			if (Isolvent().computeMin() < 0. || Isolvent().computeMax() > 1.)
				REPORT_ERROR("MlOptimiser::solventFlatten: ERROR solvent mask should contain values between 0 and 1 only...");
		}

		// Also read a second solvent mask if necessary
		if (!fn_mask2.contains("None"))
		{
			Isolvent2.read(fn_mask2);
			Isolvent2().setXmippOrigin();
			if (!Isolvent2().sameShape(Isolvent()))
				REPORT_ERROR("MlOptimiser::solventFlatten ERROR: second solvent mask is of incorrect size.");
		}

		for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
		{

			// Then apply the expanded solvent mask to the map
			mymodel.Iref[iclass] *= Isolvent();

			// Apply a second solvent mask if necessary
			// This may for example be useful to set the interior of icosahedral viruses to a constant density value that is higher than the solvent
			// Invert the solvent mask, so that an input mask can be given where 1 is the masked area and 0 is protein....
			if (!fn_mask2.contains("None"))
				softMaskOutsideMap(mymodel.Iref[iclass], Isolvent2(), true);

		} // end for iclass
	} // end if not use itk in solvent flatten
	else if(use_itk_flatten ) // && (node->rank == 2 ) ) // || node->rank == 2) )
	{
		/*
		    This section using the ITK library to perform solvent flatten for reconstructed model
		  NEEDS further REVISION!!!
		    Because in the not last iteration reconstruction process, models are independently reconstructed in either two halves, so 
		  the function only needs to flatten TWO models from each random subset NODE (rank 1 and rank 2), then send flattened models to other
		  nodes.
		    This will use the relion_MPI_Send and relion_MPI_Recv functions
		    In the last iteration, the final reconstructed model from combined particles will be finally flattened at the master node (??????)
		      ---> At the last iteration, we cannot perform solventFlatten to the reconstructed model!!!

		  So, there will be a big change in the following ITK flatten section and the judging condition!!!

		*/

		/*
		  and after the otsu thresholding to gradient magnitude map, the function should perform properly dilation
		  to the output binary map
		*/

		// Temporary, the solventflatten function using ITK only used in autorefine process
		// so the number of classes to be refined only to be 1

		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			int reconstruct_rank1 = (iclass % (node->size - 1)) + 1;
			int reconstruct_rank2 = (iclass % (node->size - 1)) + 2;


			if(node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2)
			{
				std::cerr << "iteration: " << iter << std::endl;
				std::cerr << "ITK in solventFlatten function, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
				// If use the ITK library in this solventflatten function, then we need to create and find a mask
				// for multiplicating it to the reconstructed model

				mymodel.Iref[iclass].zinit = mymodel.Iref[iclass].yinit = mymodel.Iref[iclass].xinit = 0;

				Image<DOUBLE> ITK_Isolvent; // this variable will create the final binary mask for reconstructed model
				ITK_Isolvent().resize(mymodel.Iref[iclass]);
				// ITK_Isolvent().setXmippOrigin();
				ITK_Isolvent().initZeros();

				// Next, use the ITK library's functions and methods:
				//==========

				//===== First, create some normal types for input density map assign to ITK image type,
				// and then assign the value of input map to input_image type
#define DEBUG_ITK
#ifdef DEBUG_ITK
		std::cerr << "In the ITK assignment section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif
				typedef 	DOUBLE InputPixelType;
				typedef 	DOUBLE OutputPixeltype;

				const unsigned int Dimension = 3;  // the density map is 3 dimensional
				typedef 	itk::Image< InputPixelType, Dimension > 	InputImageType;
				typedef 	itk::Image< OutputPixeltype, Dimension > 	OutputImageType;

				InputImageType::Pointer input_image = InputImageType::New();
				const InputImageType::SizeType input_size = {{ XSIZE(mymodel.Iref[iclass]), YSIZE(mymodel.Iref[iclass]), ZSIZE(mymodel.Iref[iclass]) }};
				const InputImageType::IndexType input_start = {{ 0, 0, 0 }};

				// region, spacing, and other information of the input image:
				InputImageType::RegionType 	input_region;
				input_region.SetSize( input_size );
				input_region.SetIndex( input_start );

				input_image->SetRegions( input_region );
				input_image->Allocate( true );
				InputImageType::SpacingType 	input_spacing;
				input_spacing[0] = input_spacing[1] = input_spacing[2] = mymodel.pixel_size;

				input_image->SetSpacing( input_spacing );

				// assign the input density map to the input image, using iterator
				typedef 	itk::ImageRegionIteratorWithIndex< InputImageType > 	InputIteratorType;
				InputIteratorType input_image_It( input_image, input_region );

				// mymodel.Iref[0].zinit = mymodel.Iref[0].yinit = mymodel.Iref[0].xinit = 0;

#ifdef DEBUG_ITK
// to print out the starting index of mymodel.Iref[0]
std::cerr << "node: " << node->rank << " mymodel.Iref[" << iclass << "] starting x, y, z: " 
     << STARTINGX(mymodel.Iref[iclass]) << " " << STARTINGY(mymodel.Iref[iclass]) << " " << STARTINGZ(mymodel.Iref[iclass]) <<std::endl;

#endif
				for( input_image_It.GoToBegin(); !input_image_It.IsAtEnd(); ++input_image_It )
				{
					InputImageType::IndexType input_idx = input_image_It.GetIndex();
					int input_idx_x = (int)(input_idx[0]); // x index
					int input_idx_y = (int)(input_idx[1]); // y index
					int input_idx_z = (int)(input_idx[2]); // z index

					input_image_It.Set( A3D_ELEM(mymodel.Iref[iclass], input_idx_z, input_idx_y, input_idx_x) );
				}
				//==== end assign the value of input map to input image

				//==== Second, perform smoothing to the input image:
				// Second, do Gaussian smoothing to the input_image
				DOUBLE gaussian_sigma = 3.8/mymodel.pixel_size;
				DOUBLE gaussian_kernel_width = 3.5 * gaussian_sigma;

				OutputImageType::Pointer smooth_output_image = OutputImageType::New();

				GaussianFilterMapProcess( input_image, smooth_output_image, gaussian_sigma, gaussian_kernel_width );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK smoothing section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

				// void GaussianFilterMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap_new,
	    		// DOUBLE GaussianKernelSigma, DOUBLE GaussianMaxKernelWidth )

	    		// Third, compute voxels inide the user defined sphere, for gaussian smoothing result map

				NewImageType::Pointer new_1d_map_gauss_in_sphere_for_threshold = NewImageType::New();
				DOUBLE sphere_radius = particle_diameter/2;

				// input map for this function is result of gaussian smoothing
				DensityMapinDefinedSphereRadiusProcess( smooth_output_image, new_1d_map_gauss_in_sphere_for_threshold,
									sphere_radius );

				// void DensityMapinDefinedSphereRadiusProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 1 > *outputimage,
	    		// DOUBLE user_defined_sphere_radius )

	    		// Fourth, compute the 1D image for 99.74% information focused region( ONLY under the Gaussian smoothing
	    		// condition)

				DOUBLE info_left_height_sum, info_right_height_sum;
				DOUBLE info_left_bound_val, info_right_bound_val;

				NewImageType::Pointer new_1d_map_gauss_in_sphere_and_info_focused = NewImageType::New();
				InformationFocusedRegionDensityProcess( new_1d_map_gauss_in_sphere_for_threshold, new_1d_map_gauss_in_sphere_and_info_focused
						0.9974, info_left_height_sum, info_right_height_sum,
						info_left_bound_val, info_right_bound_val, false );

	    		// void InformationFocusedRegionDensityProcess( itk::Image<DOUBLE,1> *input_image, itk::Image<DOUBLE,1> *output_image,
				// DOUBLE man_centre_fraction_threshold, DOUBLE & left_height_sum, DOUBLE & right_height_sum, 
				// DOUBLE & computed_left_bound, DOUBLE & computed_right_bound, bool is_gradient_mag = false )


	    		// Fifth, compute the Otsu's and Kittler-Illingworth's method threshold value
	    		// for Gaussian smoothing result

	    			// A. compute the otsu thresholding value for gaussian smoothing result map
				DOUBLE otsu_thre_val_gauss_map = 0.;

				// use the function to compute otsu's threshold value:
				// firstly set the number of histogram bins of otsu filter to be 8000
				// the input image for this filter should be information focused and voxels' value 
				// confined in user defined sphere
				OtsuFilterThresholdProcess( new_1d_map_gauss_in_sphere_and_info_focused, 8000, 0., 1.,
									otsu_thre_val_gauss_map );

				// void OtsuFilterThresholdProcess( itk::Image< DOUBLE, 1 > *inputimage,
				// unsigned long int numberofHistogramBins, DOUBLE insideValue = 0., DOUBLE outsideValue = 1.,
				// DOUBLE & thres_val=0. )

					// B. compute the Kittler-Illingworth thresholding vlaue for gaussian smoothing result map
				DOUBLE kittler_thre_val_gauss_map = 0.;

				// use the function to compute Kittler-Illingworth threshold value
				// and firstly, set the number of histogram bins of kittler filter to be 8000
				KittlerIllingWorthFilterThresholdProcess( new_1d_map_gauss_in_sphere_and_info_focused,
										 8000, kittler_thre_val_gauss_map );

				// void KittlerIllingWorthFilterThresholdProcess( itk::Image< DOUBLE, 1 > *inputimage,
				// unsigned long int numberofHistogramBins, DOUBLE & thres_val = 0. )

	    		// Sixth, this will be the binarization processing section for Gaussian smoothing result map
				// for both otsu and kittler-Illingworth computed threshold value

	    		OutputImageType::Pointer gaussian_otsu_threshold_binary_map = OutputImageType::New();
	    		OutputImageType::Pointer gaussian_kittler_threshold_binary_map = OutputImageType::New();

	    		// the input for binarization function should be result of gaussian smoothing filter
	    		// binarization for Otsu thresholding result:
	    		ThresholdBinarizeDensityMapProcess( smooth_output_image, gaussian_otsu_threshold_binary_map,
	    												otsu_thre_val_gauss_map );
				
				// void ThresholdBinarizeDensityMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap
				//     DOUBLE binarize_threshold_value )
	    		
	    		// binarization for Kittler-Illingworth thresholding result:
	    		ThresholdBinarizeDensityMapProcess( smooth_output_image, gaussian_kittler_threshold_binary_map,
	    												kittler_thre_val_gauss_map );

	    		// ==============
	    			// After the binarization of the result map of gaussian smoothing,
	    			// we need to do some properly binary dilation to those binary map

	    			// This is in the binary map dilating section, firstly, set the radius of structuring element
	    			// this is an initial variable, should be further modified in the future
	    		int gaussian_smoothed_thresholded_binary_dilate_element_radius = 1;

	    			// set the input and output binary map to be the same -----> should not be the same one!

	    		OutputImageType::Pointer gaussian_otsu_threshold_dilated_binary_map = OutputImageType::New();
	    		OutputImageType::Pointer gaussian_kittler_threshold_dilated_binary_map = OutputImageType::New();

	    		BinaryDilationDensityProcess( gaussian_otsu_threshold_binary_map, gaussian_otsu_threshold_dilated_binary_map,
	    										gaussian_smoothed_thresholded_binary_dilate_element_radius );

	    		BinaryDilationDensityProcess( gaussian_kittler_threshold_binary_map, gaussian_kittler_threshold_dilated_binary_map,
	    										gaussian_smoothed_thresholded_binary_dilate_element_radius );

	    		// void BinaryDilationDensityProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
	 			// int binary_dilating_radius = 1)


	    		// ==============
	    		// Seventh, compute the gradient magnitude density map, the input map is
	    		// result of Gaussian smoothing

	    		OutputImageType::Pointer grad_mag_output_image = OutputImageType::New();
	    		GradientMagnitudeFilterMapProcess( smooth_output_image, grad_mag_output_image );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK edge detection section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// void GradientMagnitudeFilterMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap )

	    		// Eighth, computing voxels confined inside user defined sphere, for gradient magnitude density map
	    		// after we obtain the 1d image with pixels inside the confined sphere, respective to original 3D
	    		// density map, we should then do thresholding computation process

	    		NewImageType::Pointer new_1d_map_grad_mag_in_sphere_for_threshold = NewImageType::New();
	    		DensityMapinDefinedSphereRadiusProcess( grad_mag_output_image, new_1d_map_grad_mag_in_sphere_for_threshold,
	    													sphere_radius );

	    		// Ninth, compute the first Otsu's method thresholding value for gradient magnitude map,
	    		// with voxels confined in the user defined sphere
	    		DOUBLE otsu_first_thres_val = 0.;
	    		OtsuFilterThresholdProcess( new_1d_map_grad_mag_in_sphere_for_threshold, 8000, 0.
	    											1., otsu_first_thres_val);

	    		// Tenth, compute the second Otsu's method thresholding value( local mask???) for gradient magnitude map,
	    		// with voxels confined in user defined sphere, and also located in the VALUE ZONE: [0, first otsu threshold value]

	    		// we need to get a 1D image with pixel values confined in specific value zone:
	    		NewImageType::Pointer new_1d_map_grad_mag_in_sphere_range_for_threshold = 
	    			NewImageType::New();
	    		VoxelValuesinDefinedValueRangeImageProcess( new_1d_map_grad_mag_in_sphere_for_threshold,
	    			new_1d_map_grad_mag_in_sphere_range_for_threshold, 0., otsu_first_thres_val );

	    		DOUBLE otsu_second_thres_val = 0.;
	    		// do the otsu thresholding process for the second time( local mask?)
	    		OtsuFilterThresholdProcess( new_1d_map_grad_mag_in_sphere_range_for_threshold, 8000, 0.,
	    											1., otsu_second_thres_val );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK Otsu thresholding section in gradient magnitude processing part, node: " 
					<< node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// Eleventh, do binarization to gradient magnitude map, according to computed threshold value
	    		// ===========
	    		// Notice!!!! This section includes many process steps, do not make any mistake!!!

	    			// first otsu thresholding binary map:
	    		OutputImageType::Pointer grad_mag_first_otsu_binary_map = OutputImageType::New();
	    		ThresholdBinarizeDensityMapProcess( grad_mag_output_image, grad_mag_first_otsu_binary_map,
	    					otsu_first_thres_val );

	    			// second otsu thresholding binary map: (the local mask)
	    		OutputImageType::Pointer grad_mag_second_otsu_binary_map = OutputImageType::New();
	    		BinarizeDensityMapinRangeProcess( grad_mag_output_image, grad_mag_second_otsu_binary_map,
	    					otsu_first_thres_val, otsu_second_thres_val );

	    			// After finishing binarization of first and second local otsu for gradient magnitude map, respectively
	    			// we need to perform addition and thresholding of these two binary map, to generate a binary map
	    			// then, we should perform properly dilation to the resulting binary map of the previous step
	    		OutputImageType::Pointer grad_mag_final_otsu_binary_map = OutputImageType::New();
	    		BinaryMapsAdditionandThresholdingProcess( grad_mag_first_otsu_binary_map, grad_mag_second_otsu_binary_map,
	    										grad_mag_final_otsu_binary_map );
	    			// after the binary addition, we will perform properly dilation of added binary map

	    			// firstly, set the binary dilating ball element radius, (temporary to set 1 )
	    		int grad_mag_output_otsu_binary_dilate_radius = 1;

	    		// the output of binary dilation process for the previous step
	    		OutputImageType::Pointer grad_mag_final_otsu_dilated_binary_map = OutputImageType::New();

	    		BinaryDilationDensityProcess( grad_mag_final_otsu_binary_map, grad_mag_final_otsu_dilated_binary_map,
	    										grad_mag_output_otsu_binary_dilate_radius );

	    		// Then, because of the thresholded gradient magnitude density map, there might be some holes inside
	    		// the binary map, so we need to perform watersheds segmentation method to fill out holes

	    		OutputImageType::Pointer watershed_grad_mag_map = OutputImageType::New();
	    		// output of the watershed segment function should be a binary map
	    		WatershedSegmentDensityProcess( grad_mag_final_otsu_dilated_binary_map, watershed_grad_mag_map,
	    			watershed_threshold, watershed_level );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK watersheds segmenting section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

		    		// void WatershedSegmentDensityProcess( itk::Image< DOUBLE, 3 > * inputmap, itk::Image< DOUBLE, 3 > * outputmap,
		    		// DOUBLE watershed_threshold = 0., DOUBLE watershed_level = 1. )

	    			// Then, dilate the output of watershed filtering function, set
	    			// the structuring element radius just be 1
	    		OutputImageType::Pointer watershed_grad_mag_dilated_map = OutputImageType::New();
	    		BinaryDilationDensityProcess( watershed_grad_mag_map, watershed_grad_mag_dilated_map, 1 );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK binary dilating section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    			// after the dilation of watershed output map, we should add this result with
	    			// the dilated map of addition of two times of otsu thresholding result

	    		// define the final result of otsu thresholding method image pointer
	    		OutputImageType::Pointer final_otsu_method_binary_map = OutputImageType::New();
	    		BinaryMapsAdditionandThresholdingProcess( grad_mag_final_otsu_dilated_binary_map,watershed_grad_mag_dilated_map,
	    							final_otsu_method_binary_map );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK adding section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// =====
	    		// Twelfth, addition the gradient magnitude final binary map with
	    		// gaussian final binary map, generate the final mask binary map, WITHOUT SOFT EDGE!!!
	    		OutputImageType::Pointer final_ITK_mask_binary_map = OutputImageType::New();

	    		if( otsu_thre_val_gauss_map < kittler_thre_val_gauss_map )
	    			BinaryMapsAdditionandThresholdingProcess( gaussian_otsu_threshold_binary_map, final_otsu_method_binary_map,
	    								 final_ITK_mask_binary_map );
	    		else if( kittler_thre_val_gauss_map <= otsu_thre_val_gauss_map )
	    			BinaryMapsAdditionandThresholdingProcess( gaussian_kittler_threshold_binary_map, final_otsu_method_binary_map,
	    								 final_ITK_mask_binary_map );

	    		// =====
	    		// Thirteenth, after getting the final ITK mask map, we need to assign its voxels value to 
	    		// MultidimArray map, and make multiplication to mymodel.Iref[iclass]!!!

	    		OutputIteratorType final_ITK_mask_binary_mapIt( final_ITK_mask_binary_map,
	    							 final_ITK_mask_binary_map->GetRequestedRegion() );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK assignment value to density map section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		for( final_ITK_mask_binary_mapIt.GoToBegin(); !final_ITK_mask_binary_map.IsAtEnd(); ++final_ITK_mask_binary_mapIt )
	    		{
	    			OutputImageType::IndexType mask_map_idx = final_ITK_mask_binary_mapIt.GetIndex();
	    			// x, y, z coordinate:
					int idx_x = (int)(mask_map_idx[0]); // x index
					int idx_y = (int)(mask_map_idx[1]); // y index
					int idx_z = (int)(mask_map_idx[2]); // z index

					A3D_ELEM(ITK_Isolvent(), idx_z, idx_y, idx_x) = final_ITK_mask_binary_map->GetPixel( mask_map_idx );
	    		}

#ifdef DEBUG_ITK
		std::cerr << "End ITK assigning value to density map section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

				// for(addition_It.GoToBegin(); !addition_It.IsAtEnd(); ++addition_It)
				// {
				// 	// get the index of the iterator, for precisely locate final assignment of output map
				// 	InputImageType::IndexType addition_image_idx = addition_It.GetIndex();
				// 	// x, y, z coordinate
				// 	int idx_x = (int)(addition_image_idx[0]); // x index
				// 	int idx_y = (int)(addition_image_idx[1]); // y index
				// 	int idx_z = (int)(addition_image_idx[2]); // z index

				// 	A3D_ELEM(ITK_Isolvent(), idx_z, idx_y, idx_x) = addition_image->GetPixel(addition_image_idx);
				// }

	    		// Before final apply ITK_Isolvent() to multiply with mymodel.Iref[iclass], we have to make 
	    		// generate a soft mask map of ITK_Isolvent() map

	    		// ========================

	    		// define the image of the output result of soft mask function
	    		Image< DOUBLE > ITK_Isolvent_soft;
	    		ITK_Isolvent_soft().setXmippOrigin();

	    		// Because we need to make a soft mask, we need to know the soft edge of final soft mask
	    		// temporarily, we set it to 4 pixels, this needs further modified in the future
	    		// IMPORTANT!!!
	    		// and I think this should be parsed from the command line of the parser object!!!
	    		DOUBLE soft_edge_width = 4.;

	    		// temporarily set the binary extend radius to 2.0 pixel, this parameter also
	    		// needs to be parsed from the command line in the future!!!! IMPORTANT!
	    		DOUBLE itk_solvent_mask_extend = 2.;

	    		ITK_Isolvent().setXmippOrigin();

	    		// actually implement the automask function
	    		automask( ITK_Isolvent(), ITK_Isolvent_soft(),
	    			0.3, itk_solvent_mask_extend, soft_edge_width, true ); // set the extend width to 0 because the 

#ifdef DEBUG_ITK
		std::cerr << "In the ITK soft edge making section, before multiplication, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif


	    		mymodel.Iref[iclass] *= ITK_Isolvent_soft();

#ifdef DEBUG_ITK
		std::cerr << "In the ITK multiplicating section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// mymodel.Iref[iclass] *= ITK_Isolvent();
	    		// finishing applying mask to reconstructed model
	    		// finishing one iteration of the loop










/*				// set back to the Xmipporigin:
				mymodel.Iref[iclass].setXmippOrigin(); // because after maximization function, mymodel.Iref is already set to xmipporigin
				ITK_Isolvent().setXmippOrigin();
				ITK_Isolvent_soft().setXmippOrigin();
				// mymodel.Iref[0] = mymodel.Iref[0]* ITK_Isolvent();
				mymodel.Iref[iclass] = mymodel.Iref[iclass]* ITK_Isolvent_soft();
*/

				//==== end multiplication

				// Then, after the multiplication section, the program should also write out the mask map to disk
				// to as a reference; and should be two mask maps in total every iteration in the autorefine process

					// ========
					// IMPORTANT!!!
					// I think if we want to write out mask map, we need to wait the program finish
					// the final broadcasting process!!!

					// FileName fn_root, fn_tmp;

					// fn_root.compose(fn_out+"_it", iter, "", 3);

					// Image<DOUBLE> img;
					// // Set the correct voxel size in the mask map mrc file header:
					// img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size);
					// img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size);
					// img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size);

					// for(int iiclass = 0; iiclass < mymodel.nr_classes; iiclass++)
					// {
					// 	fn_tmp.compose(fn_root + "_half_flatten_mask" + integerToString(node->rank) + "_class", iiclass+1, "mrc", 3);
					// 	img() = mymodel.Iref[iiclass];
					// 	img.write(fn_tmp);
					// }


				/*
				End of using ITK library in the SolventFlatten function, this is a temporary testing code.
				The final output mask map is ITK_Isolvent MultidimArray, and the final multiplicated output
				model density map is mymodel.Iref[0].
				*/
				//==========

			} // end if node in which rank

		} // end the loop for, iclass



		// before do process mpisend and mpirecv, we have to wait for all nodes sychronized!
		MPI_Barrier(MPI_COMM_WORLD);

		// finally, after the end of multiplication, send flattened model to all other slave nodes
		// have to make sure salve nodes at the same subset!!!

#define DEBUG_ITK_MPI
		
		// Strategy of the subsequent sending is referred from line 1282 -...., in maximization function
		// because there might be some classes of reference model, we need to use the loop iteration

		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			MPI_Status status;
			int nr_subsets = 2;

			for(int isubset = 1; isubset <= nr_subsets; isubset++)
			{
				if(node->myRandomSubset() == isubset)
				{
					int reconstruct_rank = (iclass % (node->size - 1)) + isubset; // the REAL reconstruct rank ,first is subset1, second will be subset2
					int my_first_recv = node->myRandomSubset();

					for(int recv_node = my_first_recv; recv_node < node->size; recv_node += nr_subsets)
					{
						// the sending node and receiving node rank judgement:
						if(node->rank == reconstruct_rank && recv_node != node->rank)
						{
#ifdef DEBUG_ITK_MPI
			std::cerr << "isubset= " << isubset << " iclass: " << iclass << ", Sending node: " << reconstruct_rank << ", receiving node: " << recv_node << std::endl;
#endif						
							// send flattened model from reconstruct rank to same subset slave nodes
							node->relion_MPI_Send(MULTIDIM_ARRAY(mymodel.Iref[iclass]), MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, recv_node, MPITAG_IMAGE, MPI_COMM_WORLD);
						}
						else if(node->rank != reconstruct_rank && recv_node == node->rank)
						{
							// the other slave nodes at the same subset reveive flattened model from reconstruct node
							node->relion_MPI_Recv(MULTIDIM_ARRAY(mymodel.Iref[iclass]), MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPITAG_IMAGE, MPI_COMM_WORLD, status);
#ifdef DEBUG_ITK_MPI
			std::cerr << "isubset= " << isubset << " iclass: " << iclass << ", receiving from: " << reconstruct_rank << " at node: " << node->rank << std::endl;
#endif					
						}
					}
				}
			}

			// wait until all nodes have done their work!
			// no one will continue until we are all at here
			MPI_Barrier(MPI_COMM_WORLD);

		}

		// will user the for loop to write out half mask maps, which will under certain condition
		// node at specific rank, will write out the mask, which would be used in the solventFlatten function

		// the root and tmp file name:
		FileName fn_root, fn_tmp;
		fn_root.compose( fn_out+"_it", iter, "", 3 );

		Image< DOUBLE > img;

		// set the voxel size of the mask map in the mrc header:
		img.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
		img.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
		img.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			int reconstruct_rank1 = (iclass % (node->size - 1)) + 1;
			int reconstruct_rank2 = (iclass % (node->size - 1)) + 2;

			// if( node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2 )
			if( node->rank == 1 || node->rank == 2 )
			{
				// in the random subset, half set rank, should write out the mask map
				// and because this is inside the loop, so we will obtain the iclass number according to the iteration
				fn_tmp.compose( fn_root+"_half_flatten_soft_mask"+integerToString(node->rank),iclass+1, "mrc", 3 );
				img() = mymodel.Iref[iclass];
				img.write(fn_tmp);
			}
		} // end for loop write out soft mask in specific node rank
/*

		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			int reconstruct_rank1 = (iclass % (node->size - 1)) + 1;
			int reconstruct_rank2 = (iclass % (node->size - 1)) + 2;


			if(node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2)
			{


*/

	} // end else if use_itk_flatten

#ifdef DEBUG
	std::cerr << "Leaving MlOptimiser::solventFlatten" << std::endl;
#endif

}

//=====================
