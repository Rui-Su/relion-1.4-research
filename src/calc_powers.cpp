


#include "src/calc_powers.h"

void calculate_Power::read(int argc, char **argv)
{

	parser.setCommandLine(argc, argv);
	int gen_section = parser.addSection("General options");
	fn_exp_map_in = parser.getOption("--exp_map", "Input experimental 3D map for calculate power spectrum", "None");
	fn_simulate_map_in = parser.getOption("--simulate_map", "Input simulated 3D map for calculate power spectrum", "None");
	fn_general_out = parser.getOption("--output", "Output rootname", "calculate_power");
	pixel_size_exp = textToFloat(parser.getOption("--angpix_exp", "Pixel size (in Angstroms) from experimental map"));
	pixel_size_simulate = textToFloat(parser.getOption("--angpix_noexp", "Pixel size (in Angstroms) from simulated map"));
	do_rescale_map = parser.checkOption("--rescale_map", "Whether to perform rescale of the input non-experimental refined dendity map?");

}

void calculate_Power::usage()
{
	parser.writeUsage(std::cerr);
}


void calculate_Power::calcinitialise()
{
	//do_write_bild = false;  // prepare for mymodel writting output

	//mymodel_exp.nr_classes = 1;   // set to 1 because of there is only ONE 3D map of each experimental condition
	//mymodel_simulate.nr_classes = 1;

	//mymodel_exp.tau2_class.resize();
	//mymodel_simulate.initialise();

	// Reading in the input maps:
	I_exp.read(fn_exp_map_in);
	I_simulate.read(fn_simulate_map_in);
	I_exp().setXmippOrigin();
	I_simulate().setXmippOrigin();

	ori_size_exp_map = XSIZE(I_exp());
	ori_size_simulate_map = XSIZE(I_simulate());

	// resize and initialise the power spectrum arrays
	// because the power spectrum arrays are only one dim:
	exp_power_spectrum.resize(ori_size_exp_map);
	simulate_power_spectrum.resize(ori_size_simulate_map);

	if(do_rescale_map)
	{
	// For rescaled power spectrum array and Real Space 3D map:
	rescaled_map().resize(XSIZE(I_simulate()), YSIZE(I_simulate()), ZSIZE(I_simulate()));
	rescaled_power_spectrum.resize(ori_size_simulate_map);
	}
}



void calculate_Power::calculate_powerspectra(MultidimArray<DOUBLE> &Image_exp, MultidimArray<DOUBLE> &Image_simulate,
		MultidimArray<DOUBLE> &exp_spectrum_in, MultidimArray<DOUBLE> &simulate_spectrum_in,
		MultidimArray<DOUBLE> &result_rescaled_map, MultidimArray<DOUBLE> &result_rescaled_power_spectrum)
{
	MultidimArray<DOUBLE> ind_spectrum_exp, ind_spectrum_simulate, count_exp, count_simulate;
	
	// for storation 3D map in Fourier Space
	MultidimArray<Complex> Faux_exp, Faux_simulate;

	// for storation power spctrum in Fourier Space
	//MultidimArray<DOUBLE> spectrum_exp, spectrum_simulate;

	// resize spectrum and count array: =====================> to be cancelled in the future
	//spectrum_exp.initZeros(XSIZE(Image_exp));
	//spectrum_simulate.initZeros(XSIZE(Image_simulate));
	count_exp.initZeros(XSIZE(Image_exp));
	count_simulate.initZeros(XSIZE(Image_simulate));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// resize spectrum array:
	exp_spectrum_in.initZeros(XSIZE(Image_exp));
	simulate_spectrum_in.initZeros(XSIZE(Image_simulate));
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// // set FourierTransformer type variables:
	// FourierTransformer transformer_exp, transformer_simulate;

	// do forward FFTs to two 3D maps:
	Image_exp.setXmippOrigin();
	Image_simulate.setXmippOrigin();
	//CenterFFT(Image_exp, true);
	//CenterFFT(Image_simulate, true);
	//transformer_exp.FourierTransform(Image_exp, Faux_exp, false);
	//transformer_simulate.FourierTransform(Image_simulate, Faux_simulate, false)

	// norm factor of FFT:
	DOUBLE normfft_exp, normfft_simulate;

// debugging part
#define DEBUG
#ifdef DEBUG
	std::cerr << "Size of input experimental map:" << "xsize: " <<XSIZE(Image_exp) << " ysize: " << YSIZE(Image_exp) << " zsize: " << ZSIZE(Image_exp) << std::endl;
	std::cerr << "Size of input simulated map:" << "xsize: " <<XSIZE(Image_simulate) << " ysize: " << YSIZE(Image_simulate) << " zsize: " << ZSIZE(Image_simulate) << std::endl;
	std::cerr << "Size of power spectrum array for experimental map: " << "xsize: " << XSIZE(exp_spectrum_in) << std::endl;
	std::cerr << "Size of power spectrum array for simulated map: " << "xsize: " << XSIZE(simulate_spectrum_in) << std::endl;
#endif


	DOUBLE ori_size_exp = XSIZE(Image_exp), ori_size_simulate = XSIZE(Image_simulate);
	if(ori_size_exp != YSIZE(Image_exp) || ori_size_exp != ZSIZE(Image_exp) 
		|| ori_size_exp != ori_size_simulate || ori_size_simulate != YSIZE(I_simulate())
		|| ori_size_simulate != ZSIZE(I_simulate()))
	{
		std::cout << "ori_size_exp: " << ori_size_exp << "ori_size_simulate: " << ori_size_simulate << std::endl;
		std::cerr << "Size of input experimental map:" << "xsize: " <<XSIZE(Image_exp) << " ysize: " << YSIZE(Image_exp) << " zsize: " << ZSIZE(Image_exp) << std::endl;
		std::cerr << "Size of input simulated map:" << "xsize: " <<XSIZE(Image_simulate) << " ysize: " << YSIZE(Image_simulate) << " zsize: " << ZSIZE(Image_simulate) << std::endl;
		REPORT_ERROR("ERROR: There are not same dim in every dim of the two input maps!!!");
	}

	if(ori_size_exp != ori_size_exp_map || ori_size_exp != ori_size_simulate || ori_size_simulate != ori_size_simulate_map)
	{
		REPORT_ERROR("ERROR: The ori_size variables are not all the same!!!");
	}

// debugging
#define DEBUG
#ifdef DEBUG
	std::cerr << "Member variable ori_size_exp_map: " << ori_size_exp_map << std::endl;
	std::cerr << "Member variable ori_size_simulate_map: " << ori_size_simulate_map << std::endl;
	std::cerr << "Temporary variable ori_size_exp: " << ori_size_exp << std::endl;
	std::cerr << "Temporary variable ori_size_simulate: " << ori_size_simulate << std::endl;
#endif

	// do calculate power spectrum of input maps
	normfft_exp = (DOUBLE)(ori_size_exp * ori_size_exp);
	normfft_simulate = (DOUBLE)(ori_size_simulate * ori_size_simulate);
	//getSpectrum(Image_exp, spectrum_exp, POWER_SPECTRUM);
	//getSpectrum(Image_simulate, spectrum_simulate, POWER_SPECTRUM);

	//spectrum_exp *= normfft_exp / 2.;
	//spectrum_simulate *= normfft_simulate / 2. ;

	getSpectrum(Image_exp, exp_spectrum_in, POWER_SPECTRUM);
	getSpectrum(Image_simulate, simulate_spectrum_in, POWER_SPECTRUM);
	exp_spectrum_in *= normfft_exp / 2.;
	simulate_spectrum_in *= normfft_simulate / 2.;

	//mymodel_exp.resize(mymodel_exp.nr_classes, spectrum_exp);
	//mymodel_simulate.resize(mymodel_simulate.nr_classes, spectrum_simulate);

	//mymodel_exp.tau2_class[0] = spectrum_exp;
	//mymodel_simulate.tau2_class[0] = spectrum_simulate;

	if(do_rescale_map)
	{
		//++++++++++++++++++ next do rescale the power of simulated 3D map to the same as experimental map ++++++++++++++++
		// and also the inverse Fourier Transform and calculation of power spectrum
		//+++++++++++++++++ rescaling step: ++++++++++++++

		// set FourierTransformer type variables:
		FourierTransformer transformer_exp, transformer_simulate;
		transformer_exp.FourierTransform(Image_exp, Faux_exp, false);
		transformer_simulate.FourierTransform(Image_simulate, Faux_simulate, false);

		FOR_ALL_ELEMENTS_IN_FFTW_TRANSFORM(Faux_simulate)
		{
			long int idx = ROUND(sqrt(kp*kp + ip*ip + jp*jp));
			// if the power spectrum at this resolution comes to zero:
			if (exp_spectrum_in(idx) < 1e-14 && simulate_spectrum_in(idx) < 1e-14) 
			{
				dAkij(Faux_simulate, k, i, j) =  dAkij(Faux_simulate, k, i, j);
			}
			else
				dAkij(Faux_simulate, k, i, j) =  dAkij(Faux_simulate, k, i, j)*(sqrt((exp_spectrum_in(idx) / simulate_spectrum_in(idx))));
		}

		//+++++++++++++++++
		//+++++++++++++++ inverse Fourier Transfrom and calculation of power spectrum step: +++++++

		FourierTransformer transformer_rescaled;
		result_rescaled_map.setXmippOrigin();
		//transformer_rescaled.setReal(result_rescaled_map);
		// do the inverse FFT:
		transformer_rescaled.inverseFourierTransform(Faux_simulate, result_rescaled_map);
		//result_rescaled_map.setXmippOrigin();
		//CenterFFT(result_rescaled_map, false);
		CenterFFT(result_rescaled_map, false);
		//result_rescaled_map /= ori_size_simulate_map; // the normfft ??????

		result_rescaled_map.setXmippOrigin();
		// do calculate power spectrum
		getSpectrum(result_rescaled_map, result_rescaled_power_spectrum, POWER_SPECTRUM);
		result_rescaled_power_spectrum *= normfft_simulate / 2.;

		CenterFFT(result_rescaled_map, false); // to shift back to origin coordinates
		//+++++++++++++++++++++++

	}

}

void calculate_Power::WriteOuput()
{
	// Write TWO output STAR file with POWER SPECTRUM curves according to the resolution in the Fourier Space:
	std::ofstream fh_exp, fh_simulate;
	FileName fn_tmp_exp, fn_tmp_simulate;

	fn_tmp_exp =  fn_general_out + "_experimental_map_power.star";
	fn_tmp_simulate = fn_general_out + "_simulated_map_power.star";

	fh_exp.open((fn_tmp_exp).c_str(), std::ios::out);
	fh_simulate.open((fn_tmp_simulate).c_str(), std::ios::out);
	if ((!fh_exp) || (!fh_simulate))
		REPORT_ERROR( (std::string)"calculate_Power::WriteOuput: Cannot write file");

	MetaDataTable MDpower_exp, MDpower_simulate;

	// First, write the experimental map's power spectrum:
	// Write the command line as a comment in the header:
	fh_exp << "# RELION calculate_Power" << std::endl;
	fh_exp << "#";
	parser.writeCommandLine(fh_exp);

	int iclass = 0;

	// Write radial average of tau2_class of the experimental derived 3D map:
	//for (int iclass = 0; iclass < mymodel_exp.nr_classes; iclass++)
		//{
			MDpower_exp.clear();
			MDpower_exp.setName("model_class_" + integerToString(iclass+1));
			//for (int ii = 0; ii < XSIZE(mymodel_exp.tau2_class[iclass]); ii++)
			for (int ii = 0; ii < XSIZE(exp_power_spectrum); ii++)
			{
				MDpower_exp.addObject();
				MDpower_exp.setValue(EMDL_SPECTRAL_IDX, ii);
				MDpower_exp.setValue(EMDL_RESOLUTION, getResolutionfromexperiment(ii));
				MDpower_exp.setValue(EMDL_RESOLUTION_ANGSTROM, getResolutionAngstromfromexperiment(ii));
				//MDpower_exp.setValue(EMDL_MLMODEL_TAU2_REF, mymodel_exp.tau2_class[iclass](ii));
				MDpower_exp.setValue(EMDL_MLMODEL_TAU2_REF, exp_power_spectrum(ii));
			}
			MDpower_exp.write(fh_exp);
		//}

	// Second, write the simulated map's power spectrum:
	fh_simulate << "# RELION calculate_Power" <<std::endl;
	fh_simulate << "#";
	parser.writeCommandLine(fh_simulate);

	// write radial average of tau2_class of the PDB derived 3D map:
	//for (int iclass = 0; iclass < mymodel_simulate.nr_classes; iclass++)
		//{
			MDpower_simulate.clear();
			MDpower_simulate.setName("model_class_" + integerToString(iclass+1));
			//for (int ii = 0; ii < XSIZE(mymodel_simulate.tau2_class[iclass]); ii++)
			for (int ii = 0; ii < XSIZE(simulate_power_spectrum); ii++)
			{
				MDpower_simulate.addObject();
				MDpower_simulate.setValue(EMDL_SPECTRAL_IDX, ii);
				MDpower_simulate.setValue(EMDL_RESOLUTION, getResolutionfromsimulation(ii));
				MDpower_simulate.setValue(EMDL_RESOLUTION_ANGSTROM, getResolutionAngstromfromsimulation(ii));
				//MDpower_simulate.setValue(EMDL_MLMODEL_TAU2_REF, mymodel_simulate.tau2_class[iclass](ii));
				MDpower_simulate.setValue(EMDL_MLMODEL_TAU2_REF, simulate_power_spectrum(ii));
			}
			MDpower_simulate.write(fh_simulate);
		//}

	// then, id do rescale the power of the density map, do write out the rescaled result dendity map:
	if (do_rescale_map)
	{
		std::ofstream fh_rescaled_result;
		FileName fn_tmp_rescaled_result, fn_tmp_rescaled_result_real_space_map;

		fn_tmp_rescaled_result = fn_general_out + "_rescaled_map_power_spectrum.star";
		fh_rescaled_result.open((fn_tmp_rescaled_result).c_str(), std::ios::out);
		MetaDataTable MDpower_rescaled_result;

		fh_rescaled_result << "# RELION calculate the rescaled map power spctrum: " << std::endl;
		fh_rescaled_result << "#";

		MDpower_rescaled_result.clear();
		MDpower_rescaled_result.setName("model_class_power_spectrum_rescaled_result");

		// write out the power spectrum of resaled map
		for (int ii = 0; ii < XSIZE(rescaled_power_spectrum); ii++)
		{
			MDpower_rescaled_result.addObject();
			MDpower_rescaled_result.setValue(EMDL_SPECTRAL_IDX, ii);
			MDpower_rescaled_result.setValue(EMDL_RESOLUTION, getResolutionfromsimulation(ii));
			MDpower_rescaled_result.setValue(EMDL_RESOLUTION_ANGSTROM, getResolutionAngstromfromsimulation(ii));
			//MDpower_simulate.setValue(EMDL_MLMODEL_TAU2_REF, mymodel_simulate.tau2_class[iclass](ii));
			MDpower_rescaled_result.setValue(EMDL_MLMODEL_TAU2_REF, rescaled_power_spectrum(ii));
		}
		MDpower_rescaled_result.write(fh_rescaled_result);

		// write out the rescaled Real Space 3D map:
		rescaled_map.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, pixel_size_simulate);
		rescaled_map.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, pixel_size_simulate);
		rescaled_map.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, pixel_size_simulate);
		fn_tmp_rescaled_result_real_space_map = fn_general_out + "rescaled_map.mrc";
		rescaled_map.write(fn_tmp_rescaled_result_real_space_map);
	}
}

void calculate_Power::run()
{

	calculate_powerspectra(I_exp(), I_simulate(), exp_power_spectrum, simulate_power_spectrum, rescaled_map(), rescaled_power_spectrum);

	WriteOuput();
}














