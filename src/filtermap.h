

// To compute 3D convolution of a density with a kernel

#ifndef FILTERMAP_H_
#define FILTERMAP_H_

#include "src/image.h"
#include "src/fftw.h"
#include <math.h>
// Testing using the ITK library for processing 3D density map:
#include "itkImage.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImportImageFilter.h"
// header files for image IO:
// #include "itkImageFileWriter.h"
//#include "itkMRCImageIO.h"
//#include "itkMRCImageIOFactory.h"
#include "itkTestingMacros.h"
// Gaussian image filter:
#include "itkDiscreteGaussianImageFilter.h"

#include "itkRescaleIntensityImageFilter.h"
#include "itkBilateralImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"

#include "itkVotingBinaryIterativeHoleFillingImageFilter.h"

#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryThresholdImageFilter.h"

#include "itkWatershedImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkNumericTraits.h"

class FilterMap_base
{
public:

	IOParser parser;

	// input and output image rootnames:
	FileName fn_in, fn_out;

	// Images for input and resulting map:
	Image<DOUBLE> I_in, I_result;

	// pixel size in angstrom:
	DOUBLE pixel_size;

	// the radius of convolution kernel;
	int conv_kernel_radius;

	// sigma of Gaussian smooth filter kernel:
	DOUBLE gauss_kernel_sigma;

	// judge whether to calculate power spectrum of smoothing filter kernel:
	bool calc_fft_convkernel_power;

	// judge whether to compute average smoothing or Gaussian Smoothing:
	bool calc_average_smooth, calc_gauss_smooth;

	MultidimArray<DOUBLE> convkernel_power_spectrum;

	// The subsequent variable to judge whether to use sobel operator to detect the edge of the density map
	bool do_Sobel_computation;

	// whether to perform prewitt operator to input density map for edge detection
	bool do_Prewitt_computation;

	// Next are for ITK using parameters:
	// Whether do use the itk smoothing methods on 3D images:
	bool do_use_itk_smooth;
	  // whether to do itk gaussian filtration:
	bool do_itk_gauss_smooth;
	  // width of gaussian filter kernel
	int itk_gauss_width;
	  // sigma of gaussian filter kernel
	DOUBLE itk_gauss_sigma;


public:

	// read in commandline parameters:
	void read(int argc, char **argv);

	// print usage instructions:
	void usage();

	void initialise();

	// do filter target map with convolution kernel
	void ComputeFiltermap(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &vol_out, int &kernel_radius);

	// compute power spectrum of convolution kernel
	void ComputeKernelpower(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &power_spectrum);

	//+++++++++++++++++++++++++++++++++++++++++

	// get average smooth filter kernel, from the input initialised zeros parameter (convolution) array, in Real Space:
	void GetAverageSmoothFilterKernel(MultidimArray<DOUBLE> &filter_kernel, int &kernel_radius);

	// get Gaussian smooth filter kernel, in Real Space:
	void GetGaussianFilterKernel(MultidimArray<DOUBLE> &filter_kernel, int &kernel_radius, DOUBLE sigma);

	//+++++++++++++++++++++++++++++++++++++++++
	// do SOBEL operation to the input density map for DETECTION OF EDGE of the density map, and this function will compute the final resulting map with apparent edge density map
	void SobelOperationtoDensityMap(MultidimArray<DOUBLE> &input_map, MultidimArray<DOUBLE> &output_map);

	// do Prewitt operation to the input density map:
	void PrewittOperationtoDensityMap(MultidimArray<DOUBLE> &input_map, MultidimArray<DOUBLE> &output_map);

	//+++++++++++++++++++++++++++++++++++++++++

	//+++++++++++++++++++++++++++++++++++++++++
	// the testing function using the ITK library for processing 3D density map:
	
	void ITKTestingProcessingFunction(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &vol_out);

	//+++++++++++++++++++++++++++++++++++++++++

	// write results to outputs
	void WriteOutput();

	// running
	void run();

};

#endif
