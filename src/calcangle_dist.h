

#ifndef CALCANGLE_DIST_H_
#define CALCANGLE_DIST_H_

#include "/home/pxuser/surui/apps/relion/relion-1.4/src/healpix_sampling.h"
#include "/home/pxuser/surui/apps/relion/relion-1.4/src/exp_model.h"

#define METADATA_LINE_LENGTH 15

#define METADATA_ROT 0
#define METADATA_TILT 1
#define METADATA_PSI 2

#define DO_WRITE_DATA true

class calcangledistance
{
public:
	// I/O Parser
	IOPaeser parser;

	// Likelihood and always cc -> 2 Input STAR files and output result file
	FileName fn_in1, fn_in2, fn_out;

	// Experimental metadata model
	Experiment mydata_LL, mydata_cc;

	// To store the result metadata table
	Experiment mydata_results;

	//HEALPix sampling object for calculate angular distances
	HalpixSampling sampling;

	long int exp_nr_images;

	// For storing angular distances between two methods of each particle in the loop:
	DOUBLE angulardistances;

	// To finally store angular distance between two methods of each particle:
	std::vector<DOUBLE> angdist;

	MultidimArray<DOUBLE> exp_metadata_LL, exp_metadata_cc;

public:
	//Read command line arguments
	void read(int argc, char **argv);

	//Print usage instructions
	void usage();

	// Initialise some member stuff
	void initialise();

	// Actually calculate angular distances between two methods, and set the distance results to metadata table
	void calculateangulardistances();

	// Obtain orientational angular information to exp_metadata array
	void getMetadataSubset(int first_ori_particle_id, int last_ori_particle_id)

	// Output STAR file with angular distances of each particle between two methods
	void writeOutput();

	// Actually Running to calculate angular distances
	void run();
};



#endif