

#ifndef CALC_POWERS_H_
#define CALC_POWERS_H_

#include "src/ml_model.h"
#include "src/fftw.h"
#include "src/image.h"

class calculate_Power
{
public:
	// I/O parser
	IOParser parser;

	// judge whether to rescale the input no-original density map:
	bool do_rescale_map;

	// Input files
	FileName fn_exp_map_in, fn_simulate_map_in;

	// Output files:
	FileName fn_general_out, fn_exp_out, fn_simulate_out;


	// Images for reading in two 3D maps
	Image<DOUBLE> I_exp, I_simulate;

	// power spectrum arrays
	MultidimArray<DOUBLE> exp_power_spectrum, simulate_power_spectrum;

	// power spectrum of rescaled map in Fourier Space:
	MultidimArray<DOUBLE> rescaled_power_spectrum;

	// rescaled 3D map in Real Space:
	Image<DOUBLE> rescaled_map;

	// Current ML model
	//MlModel mymodel_exp, mymodel_simulate;

	int ori_size_exp_map, ori_size_simulate_map;

	DOUBLE pixel_size_exp, pixel_size_simulate;

public:
	// Read commandling parameters
	void read(int argc, char **argv);

	// Print usage
	void usage();

	// Initialise some stuff after reading
	void calcinitialise();

	// To calculate power spectrum of two input maps, and rescale the power spectrum of simulated map to be the same as experimental map
	// and convert it to Real Space for output, and also the same as the resulting rescaled power spectrum of rescaled map
	void calculate_powerspectra(MultidimArray<DOUBLE> &Image_exp, MultidimArray<DOUBLE> &Image_simulate, 
		MultidimArray<DOUBLE> &exp_spectrum_in, MultidimArray<DOUBLE> &simulate_spectrum_in, 
		MultidimArray<DOUBLE> &result_rescaled_map, MultidimArray<DOUBLE> &result_rescaled_power_spectrum);
	// waiting for define formal parameter in this function to replace mlmodel tau2class !!!!

	// the next two functions are copied from ml_model.h file inorder to write resolution to output star files
	DOUBLE getResolutionfromexperiment(int ipix)	{ return (DOUBLE)ipix/(pixel_size_exp * ori_size_exp_map); }
	DOUBLE getResolutionfromsimulation(int ipix)	{ return (DOUBLE)ipix/(pixel_size_simulate * ori_size_simulate_map); }

	DOUBLE getResolutionAngstromfromexperiment(int ipix)	{ return (ipix==0) ? 999. : (pixel_size_exp * ori_size_exp_map)/(DOUBLE)ipix; }
	DOUBLE getResolutionAngstromfromsimulation(int ipix)	{ return (ipix==0) ? 999. : (pixel_size_simulate * ori_size_simulate_map)/(DOUBLE)ipix; }


	// write the results to outputs
	void WriteOuput();


	// General Running
	void run();
};

#endif
