/***************************************************************************
 *
 * Author: "Sjors H.W. Scheres"
 * MRC Laboratory of Molecular Biology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
#include <mpi.h>
#include "src/ml_optimiser_mpi.h"
#include "src/ml_optimiser.h"
// #include "src/backprojector.h"

//#define DEBUG
//#define DEBUG_MPIEXP2
void MlOptimiserMpi::read(int argc, char **argv)
{
#ifdef DEBUG
    std::cerr<<"MlOptimiserMpi::read Entering "<<std::endl;
#endif

    // Define a new MpiNode
    node = new MpiNode(argc, argv);

    // First read in non-parallelisation-dependent variables
    MlOptimiser::read(argc, argv, node->rank);
    fn_scratch = parser.getOption("--scratchdir", "Directory (with absolute path, and visible to all nodes) for temporary files", "");

    // Don't put any output to screen for mpi slaves
    verb = (node->isMaster()) ? 1 : 0;

    // TMP for debugging only
    //if (node->rank==1)
    //	verb = 1;
    // Possibly also read parallelisation-dependent variables here

#ifdef DEBUG
    std::cerr<<"MlOptimiserMpi::read done"<<std::endl;
#endif

}
void MlOptimiserMpi::finalise()
{
	delete node;
}

void MlOptimiserMpi::initialise()
{

#ifdef DEBUG
    std::cerr<<"MlOptimiserMpi::initialise Entering"<<std::endl;
#endif

    // Print information about MPI nodes:
    printMpiNodesMachineNames(*node, nr_threads);

    MlOptimiser::initialiseGeneral(node->rank);

    initialiseWorkLoad();

	if (fn_sigma != "")
	{
		// Read in sigma_noise spetrum from file DEVELOPMENTAL!!! FOR DEBUGGING ONLY....
		MetaDataTable MDsigma;
		DOUBLE val;
		int idx;
		MDsigma.read(fn_sigma);
		FOR_ALL_OBJECTS_IN_METADATA_TABLE(MDsigma)
		{
			MDsigma.getValue(EMDL_SPECTRAL_IDX, idx);
			MDsigma.getValue(EMDL_MLMODEL_SIGMA2_NOISE, val);
			if (idx < XSIZE(mymodel.sigma2_noise[0]))
				mymodel.sigma2_noise[0](idx) = val;
		}
		if (idx < XSIZE(mymodel.sigma2_noise[0]) - 1)
		{
			if (verb > 0) std::cout<< " WARNING: provided sigma2_noise-spectrum has fewer entries ("<<idx+1<<") than needed ("<<XSIZE(mymodel.sigma2_noise[0])<<"). Set rest to zero..."<<std::endl;
		}
		// Use the same spectrum for all classes
		for (int igroup = 0; igroup< mymodel.nr_groups; igroup++)
			mymodel.sigma2_noise[igroup] =  mymodel.sigma2_noise[0];

	}
	else if (do_calculate_initial_sigma_noise || do_average_unaligned)
	{
		MultidimArray<DOUBLE> Mavg;
		// Calculate initial sigma noise model from power_class spectra of the individual images
		// This is done in parallel
		//std::cout << " Hello world1! I am node " << node->rank << " out of " << node->size <<" and my hostname= "<< getenv("HOSTNAME")<< std::endl;
		calculateSumOfPowerSpectraAndAverageImage(Mavg);

		// Set sigma2_noise and Iref from averaged poser spectra and Mavg
		if (!node->isMaster())
			MlOptimiser::setSigmaNoiseEstimatesAndSetAverageImage(Mavg);
		//std::cout << " Hello world3! I am node " << node->rank << " out of " << node->size <<" and my hostname= "<< getenv("HOSTNAME")<< std::endl;
	}

   MlOptimiser::initialLowPassFilterReferences();

   // to write out low-pass filtered reference model to disk file
   // std::cerr << "After initial lowpass filter reference function." << std::endl;
   // std::cerr << "node: " << node->rank << std::endl;

   if(node->isMaster())
   {
   	// master node write out low pass filtered reference model
		for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
		{
			std::cerr << "iclass: " << iclass << std::endl;
			std::cerr << "node: " << node->rank << std::endl;

// #define DEBUG_LOW_PASS
#ifdef DEBUG_LOW_PASS
			std::cerr << "====== after low pass filtering function =====" << std::endl;
			std::cerr << "node: " << node->rank << std::endl;
			std::cerr << "zinit: " << mymodel.Iref[iclass].zinit << ", xinit: "
				<< mymodel.Iref[iclass].xinit << ", yinit: " << mymodel.Iref[iclass].yinit << std::endl;
#endif

			// write out low pass filtered model to disk file:
			FileName fn_root, fn_tmp;
			fn_root.compose( fn_out+"_it", iter, "",3 );

			Image< DOUBLE > img_low_passed;
			img_low_passed.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
			img_low_passed.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
			img_low_passed.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

			fn_tmp.compose( fn_root+"_initial_low_pass_filtered_"+integerToString(ini_high)+"A"+"_map_class", iclass+1, "mrc", 3 );
			// mymodel.Iref[iclass].setXmippOrigin();

#ifdef DEBUG_LOW_PASS
			std::cerr << "zinit: " << mymodel.Iref[iclass].zinit << ", xinit: "
				<< mymodel.Iref[iclass].xinit << ", yinit: " << mymodel.Iref[iclass].yinit << std::endl;
			std::cerr << "=============" << std::endl;
#endif

			img_low_passed() = mymodel.Iref[iclass];
			img_low_passed.write(fn_tmp);
		}

   }

	// Initialise the data_versus_prior ratio to get the initial current_size right
	if (iter == 0)
		mymodel.initialiseDataVersusPrior(fix_tau); // fix_tau was set in initialiseGeneral

	//std::cout << " Hello world! I am node " << node->rank << " out of " << node->size <<" and my hostname= "<< getenv("HOSTNAME")<< std::endl;

	// Only master writes out initial mymodel (do not gather metadata yet)
	int nr_subsets = (do_split_random_halves) ? 2 : 1;
	if (node->isMaster())
		MlOptimiser::write(DONT_WRITE_SAMPLING, DO_WRITE_DATA, DONT_WRITE_OPTIMISER, DONT_WRITE_MODEL, node->rank);
	else if (node->rank <= nr_subsets)
	{
		//Only the first_slave of each subset writes model to disc
		MlOptimiser::write(DO_WRITE_SAMPLING, DONT_WRITE_DATA, DO_WRITE_OPTIMISER, DO_WRITE_MODEL, node->rank);

		bool do_warn = false;
		for (int igroup = 0; igroup< mymodel.nr_groups; igroup++)
		{
			if (mymodel.nr_particles_group[igroup] < 5 && node->rank == 1) // only warn for half1 to avoid messy output
			{
				if (nr_subsets == 1)
					std:: cout << "WARNING: There are only " << mymodel.nr_particles_group[igroup] << " particles in group " << igroup + 1 << std::endl;
				else
					std:: cout << "WARNING: There are only " << mymodel.nr_particles_group[igroup] << " particles in group " << igroup + 1 << " of half-set " << node->rank << std::endl;
				do_warn = true;
			}
		}
		if (do_warn)
		{
			std:: cout << "WARNING: You may want to consider joining some micrographs into larger groups to obtain more robust noise estimates. " << std::endl;
			std:: cout << "         You can do so by using the same rlnMicrographName for particles from multiple different micrographs in the input STAR file. " << std::endl;
            std:: cout << "         It is then best to join micrographs with similar defocus values and similar apparent signal-to-noise ratios. " << std::endl;
		}
	}

	// Do this after writing out the model, so that still the random halves are written in separate files.
	if (do_realign_movies)
	{
		// Resolution seems to decrease again after 1 iteration. Therefore, just perform a single iteration until we figure out what exactly happens here...
		has_converged = true;
		// Then use join random halves
		do_join_random_halves = true;

		// If we skip the maximization step, then there is no use in using all data
		if (!do_skip_maximization)
		{
			// Use all data out to Nyquist because resolution gains may be substantial
			do_use_all_data = true;
		}
	}


#ifdef DEBUG
    std::cerr<<"MlOptimiserMpi::initialise Done"<<std::endl;
#endif
}

void MlOptimiserMpi::initialiseWorkLoad()
{

    if (do_split_random_halves && node->size <= 2)
    	REPORT_ERROR("MlOptimiserMpi::initialiseWorkLoad: at least 3 MPI processes are required when splitting data into random halves");
    else if(node->size <= 1)
    	REPORT_ERROR("MlOptimiserMpi::initialiseWorkLoad: at least 2 MPI processes are required, otherwise use the sequential program");

	// Get the same random number generator seed for all mpi nodes
	if (random_seed == -1)
	{
		if (node->isMaster())
		{
			random_seed = time(NULL);
	        for (int slave = 1; slave < node->size; slave++)
	        	node->relion_MPI_Send(&random_seed, 1, MPI_INT, slave, MPITAG_RANDOMSEED, MPI_COMM_WORLD);
		}
		else
		{
			MPI_Status status;
			node->relion_MPI_Recv(&random_seed, 1, MPI_INT, 0, MPITAG_RANDOMSEED, MPI_COMM_WORLD, status);
		}
	}

    // First split the data into two random halves and then randomise the particle order
	if (do_split_random_halves)
		mydata.divideOriginalParticlesInRandomHalves(random_seed);

	// Randomise the order of the particles
    mydata.randomiseOriginalParticlesOrder(random_seed, do_split_random_halves);

    // Also randomize random-number-generator for perturbations on the angles
    init_random_generator(random_seed);


	if (node->isMaster())
	{
		// The master never participates in any actual work
		my_first_ori_particle_id = 0;
		my_last_ori_particle_id = -1;
	}
	else
	{
		if (do_split_random_halves)
		{
	    	int nr_slaves_subset1 = (node->size - 1) / 2;
	    	int nr_slaves_subset2 = nr_slaves_subset1;
	    	if ( (node->size - 1) % 2 != 0)
	    		nr_slaves_subset1 += 1;
	    	if (node->myRandomSubset() == 1)
	    	{
	    		// Divide first half of the images
	    		divide_equally(mydata.numberOfOriginalParticles(1), nr_slaves_subset1, node->rank / 2, my_first_ori_particle_id, my_last_ori_particle_id);
	    	}
	    	else
	    	{
	    		// Divide second half of the images
	    		divide_equally(mydata.numberOfOriginalParticles(2), nr_slaves_subset2, node->rank / 2 - 1, my_first_ori_particle_id, my_last_ori_particle_id);
	    		my_first_ori_particle_id += mydata.numberOfOriginalParticles(1);
	    		my_last_ori_particle_id += mydata.numberOfOriginalParticles(1);
	    	}
		}
		else
		{
			int nr_slaves = (node->size - 1);
			divide_equally(mydata.numberOfOriginalParticles(), nr_slaves, node->rank - 1, my_first_ori_particle_id, my_last_ori_particle_id);
		}

	}
//#define DEBUG_WORKLOAD
#ifdef DEBUG_WORKLOAD
	std::cerr << " node->rank= " << node->rank << " my_first_ori_particle_id= " << my_first_ori_particle_id << " my_last_ori_particle_id= " << my_last_ori_particle_id << std::endl;
#endif
}

void MlOptimiserMpi::calculateSumOfPowerSpectraAndAverageImage(MultidimArray<DOUBLE> &Mavg)
{

	// First calculate the sum of all individual power spectra on each subset
	MlOptimiser::calculateSumOfPowerSpectraAndAverageImage(Mavg, node->rank == 1);
	//std::cout << " Hello world22! I am node " << node->rank << " out of " << node->size <<" and my hostname= "<< getenv("HOSTNAME")<< std::endl;

	// When splitting the data into two random halves, perform two passes: one for each subset
	int nr_subsets = (do_split_random_halves) ? 2 : 1;

	MultidimArray<DOUBLE> Msum, MsumI;
	MultidimArray<int> Mnr, Msumnr;
	DOUBLE dsum;
	int isum;
	MPI_Status status;


	for (int isubset = 1; isubset <= nr_subsets; isubset++)
	{
		int my_first_slave = isubset; // first pass subset1: my_first_rank = 1, second pass subset2: my_first_rank = 2
		int my_first_other_slave = XMIPP_MIN(my_first_slave + nr_subsets, node->size);
		//std::cerr << " my_first_slave= " << my_first_slave << " my_first_other_slave= " << my_first_other_slave << std::endl;
		// Initialise on the first slave
		if (node->rank == my_first_slave)
		{
			Msum = wsum_model.sigma2_noise[0];
			MsumI = Mavg;
			dsum = wsum_model.sumw_group[0];
			Mnr.resize(mymodel.nr_groups);
			FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(Mnr)
			{
				DIRECT_A1D_ELEM(Mnr, i) = mymodel.nr_particles_group[i];
			}
			Msumnr = Mnr;

    	}
		for (int other_slave = my_first_other_slave; other_slave < node->size; other_slave += nr_subsets)
		{
			if (node->rank == other_slave)
			{
				//std::cerr << "Sending from "<<other_slave<< " to "<<my_first_slave << std::endl;
				node->relion_MPI_Send(MULTIDIM_ARRAY(wsum_model.sigma2_noise[0]), MULTIDIM_SIZE(wsum_model.sigma2_noise[0]), MY_MPI_DOUBLE, my_first_slave, MPITAG_PACK, MPI_COMM_WORLD);
				node->relion_MPI_Send(MULTIDIM_ARRAY(Mavg), MULTIDIM_SIZE(Mavg), MY_MPI_DOUBLE, my_first_slave, MPITAG_IMAGE, MPI_COMM_WORLD);
				Mnr.resize(mymodel.nr_groups);
				FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(Mnr)
				{
					DIRECT_A1D_ELEM(Mnr, i) = mymodel.nr_particles_group[i];
        		}
				node->relion_MPI_Send(MULTIDIM_ARRAY(Mnr), MULTIDIM_SIZE(Mnr), MPI_INT, my_first_slave, MPITAG_METADATA, MPI_COMM_WORLD);
				node->relion_MPI_Send(&wsum_model.sumw_group[0], 1, MY_MPI_DOUBLE, my_first_slave, MPITAG_DOUBLE, MPI_COMM_WORLD);

    		}
			else if (node->rank == my_first_slave)
			{
				//std::cerr << "Receiving at "<<my_first_slave<< " from "<<other_slave<<std::endl;
				node->relion_MPI_Recv(MULTIDIM_ARRAY(wsum_model.sigma2_noise[0]), MULTIDIM_SIZE(wsum_model.sigma2_noise[0]), MY_MPI_DOUBLE, other_slave, MPITAG_PACK, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(Mavg), MULTIDIM_SIZE(Mavg), MY_MPI_DOUBLE, other_slave, MPITAG_IMAGE, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(Mnr), MULTIDIM_SIZE(Mnr), MPI_INT, other_slave, MPITAG_METADATA, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(&wsum_model.sumw_group[0], 1, MY_MPI_DOUBLE, other_slave, MPITAG_DOUBLE, MPI_COMM_WORLD, status);
				// Add the contribution of this other slave
				Msum  += wsum_model.sigma2_noise[0];
				MsumI += Mavg;
				Msumnr += Mnr;
				dsum += wsum_model.sumw_group[0];
    		}
		}

		// Now the first slave has the sum of all other slaves in its Msum
		// Send this sum to all relevant other slaves
		for (int other_slave = my_first_other_slave; other_slave < node->size; other_slave += nr_subsets)
		{
			if (node->rank == my_first_slave)
			{
				//std::cerr << "Sending from my_first_slave"<<my_first_slave<< " to other_slave "<<other_slave<< std::endl;
				node->relion_MPI_Send(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, other_slave, MPITAG_PACK, MPI_COMM_WORLD);
				node->relion_MPI_Send(MULTIDIM_ARRAY(Msumnr), MULTIDIM_SIZE(Msumnr), MPI_INT, other_slave, MPITAG_METADATA, MPI_COMM_WORLD);
				node->relion_MPI_Send(MULTIDIM_ARRAY(MsumI), MULTIDIM_SIZE(MsumI), MY_MPI_DOUBLE, other_slave, MPITAG_IMAGE, MPI_COMM_WORLD);
				node->relion_MPI_Send(&dsum, 1, MY_MPI_DOUBLE, other_slave, MPITAG_DOUBLE, MPI_COMM_WORLD);
			}
			else if (node->rank == other_slave)
			{
				//std::cerr << "Receiving at other_slave "<<other_slave<<" from my_first_slave "<<my_first_slave<< std::endl;
				node->relion_MPI_Recv(MULTIDIM_ARRAY(wsum_model.sigma2_noise[0]), MULTIDIM_SIZE(wsum_model.sigma2_noise[0]), MY_MPI_DOUBLE, my_first_slave, MPITAG_PACK, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(Mnr), MULTIDIM_SIZE(Mnr), MPI_INT, my_first_slave, MPITAG_METADATA, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(Mavg), MULTIDIM_SIZE(Mavg), MY_MPI_DOUBLE, my_first_slave, MPITAG_IMAGE, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(&wsum_model.sumw_group[0], 1, MY_MPI_DOUBLE, my_first_slave, MPITAG_DOUBLE, MPI_COMM_WORLD, status);
				// Unpack Mnr on the other slave
				FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(Mnr)
				{
					mymodel.nr_particles_group[i] = DIRECT_A1D_ELEM(Mnr, i);
				}
			}
		}

		// Also set sums on the first slave
		if (node->rank == my_first_slave)
		{
			wsum_model.sigma2_noise[0] = Msum;
			Mavg = MsumI;
			FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(Msumnr)
			{
				mymodel.nr_particles_group[i] = DIRECT_A1D_ELEM(Msumnr, i);
			}
			wsum_model.sumw_group[0] = dsum;
		}

	}
	//std::cout << " Hello world23! I am node " << node->rank << " out of " << node->size <<" and my hostname= "<< getenv("HOSTNAME")<< std::endl;

}

void MlOptimiserMpi::expectation()
{

#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::expectation: Entering " << std::endl;
#endif

	MultidimArray<long int> first_last_nr_images(6);
	MultidimArray<DOUBLE> metadata;
	int first_slave = 1;
	// Use maximum of 100 particles for 3D and 10 particles for 2D estimations
	int n_trials_acc = (mymodel.ref_dim==3 && mymodel.data_dim != 3) ? 100 : 10;
	n_trials_acc = XMIPP_MIN(n_trials_acc, mydata.numberOfOriginalParticles());
	MPI_Status status;

	// Initialise some stuff
	// A. Update current size (may have been changed to ori_size in autoAdjustAngularSampling) and resolution pointers
	updateImageSizeAndResolutionPointers();

	// B. Set the PPref Fourier transforms, initialise wsum_model, etc.
	// The master only holds metadata, it does not set up the wsum_model (to save memory)
	if (!node->isMaster())
	{
		MlOptimiser::expectationSetup();

		// All slaves no longer need mydata.MD tables
		mydata.MDimg.clear();
		mydata.MDmic.clear();

		// Many small new's are not returned to the OS upon free-ing them. To force this, use the following call
		// from http://stackoverflow.com/questions/10943907/linux-allocator-does-not-release-small-chunks-of-memory
#if !defined(__APPLE__)
		malloc_trim(0);
#endif

	}

	// C. Calculate expected angular errors
	// Do not do this for maxCC
	// Only the first (reconstructing) slave (i.e. from half1) calculates expected angular errors
	////TMP if (!(iter==1 && do_firstiter_cc) && (mymodel.data_dim == 2) && !do_skip_align )
	if (!(iter==1 && do_firstiter_cc) && !(do_skip_align || do_skip_rotate) )
	{
		int my_nr_images, length_fn_ctf;
		if (node->isMaster())
		{
			// Master sends metadata (but not imagedata) for first 100 particles to first_slave (for calculateExpectedAngularErrors)
			MlOptimiser::getMetaAndImageDataSubset(0, n_trials_acc-1, false);
			my_nr_images = YSIZE(exp_metadata);
			node->relion_MPI_Send(&my_nr_images, 1, MPI_INT, first_slave, MPITAG_JOB_REQUEST, MPI_COMM_WORLD);
			node->relion_MPI_Send(MULTIDIM_ARRAY(exp_metadata), MULTIDIM_SIZE(exp_metadata), MY_MPI_DOUBLE, first_slave, MPITAG_METADATA, MPI_COMM_WORLD);
			// Also send exp_fn_ctfs if necessary
			length_fn_ctf = exp_fn_img.length() + 1; // +1 to include \0 at the end of the string
			node->relion_MPI_Send(&length_fn_ctf, 1, MPI_INT, first_slave, MPITAG_JOB_REQUEST, MPI_COMM_WORLD);
			if (length_fn_ctf > 1)
				node->relion_MPI_Send((void*)exp_fn_ctf.c_str(), length_fn_ctf, MPI_CHAR, first_slave, MPITAG_METADATA, MPI_COMM_WORLD);
		}
		else if (node->rank == first_slave)
		{
			// Slave has to receive all metadata from the master!
			node->relion_MPI_Recv(&my_nr_images, 1, MPI_INT, 0, MPITAG_JOB_REQUEST, MPI_COMM_WORLD, status);
			exp_metadata.resize(my_nr_images, METADATA_LINE_LENGTH);
			node->relion_MPI_Recv(MULTIDIM_ARRAY(exp_metadata), MULTIDIM_SIZE(exp_metadata), MY_MPI_DOUBLE, 0, MPITAG_METADATA, MPI_COMM_WORLD, status);
			node->relion_MPI_Recv(&length_fn_ctf, 1, MPI_INT, 0, MPITAG_JOB_REQUEST, MPI_COMM_WORLD, status);
			if (length_fn_ctf > 1)
			{
				char* rec_buf2;
				rec_buf2 = (char *) malloc(length_fn_ctf);
				node->relion_MPI_Recv(rec_buf2, length_fn_ctf, MPI_CHAR, 0, MPITAG_METADATA, MPI_COMM_WORLD, status);
				exp_fn_ctf = rec_buf2;
				free(rec_buf2);
			}
			calculateExpectedAngularErrors(0, n_trials_acc-1);
		}

		// The reconstructing slave Bcast acc_rottilt, acc_psi, acc_trans to all other nodes!
		node->relion_MPI_Bcast(&acc_rot, 1, MY_MPI_DOUBLE, first_slave, MPI_COMM_WORLD);
		node->relion_MPI_Bcast(&acc_trans, 1, MY_MPI_DOUBLE, first_slave, MPI_COMM_WORLD);
	}

	// D. Update the angular sampling (all nodes except master)
	if (!node->isMaster() && do_auto_refine && iter > 1 )
	{
		updateAngularSampling(node->rank == 1);
	}
	node->relion_MPI_Bcast(&has_fine_enough_angular_sampling, 1, MPI_INT, first_slave, MPI_COMM_WORLD);

	// E. All nodes, except the master, check memory and precalculate AB-matrices for on-the-fly shifts
	if (!node->isMaster())
	{
		// Check whether everything fits into memory
		MlOptimiser::expectationSetupCheckMemory(node->rank == first_slave);

		// F. Precalculate AB-matrices for on-the-fly shifts
		if (do_shifts_onthefly)
			precalculateABMatrices();
	}
	// Slave 1 sends has_converged to everyone else (in particular the master needs it!)
	node->relion_MPI_Bcast(&has_converged, 1, MPI_INT, first_slave, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&do_join_random_halves, 1, MPI_INT, first_slave, MPI_COMM_WORLD);


	// Wait until expected angular errors have been calculated
	MPI_Barrier(MPI_COMM_WORLD);
	sleep(1);

	// Now perform real expectation step in parallel, use an on-demand master-slave system
#define JOB_FIRST (first_last_nr_images(0))
#define JOB_LAST  (first_last_nr_images(1))
#define JOB_NIMG  (first_last_nr_images(2))
#define JOB_LEN_FN_IMG  (first_last_nr_images(3))
#define JOB_LEN_FN_CTF  (first_last_nr_images(4))
#define JOB_LEN_FN_RECIMG  (first_last_nr_images(5))
#define JOB_NPAR  (JOB_LAST - JOB_FIRST + 1)

    if (node->isMaster())
    {
        try
        {
    		std::cout << " Expectation iteration " << iter;
    		if (!do_auto_refine)
    			std::cout << " of " << nr_iter;
    		std::cout << std::endl;
        	init_progress_bar(mydata.numberOfOriginalParticles());

			// Master distributes all packages of SomeParticles
			int nr_slaves_done = 0;
			int random_subset = 0;
			long int nr_ori_particles_done = 0;
			long int prev_step_done = nr_ori_particles_done;
			long int progress_bar_step_size = ROUND(mydata.numberOfOriginalParticles() / 80);
			long int nr_ori_particles_done_subset1 = 0;
			long int nr_ori_particles_done_subset2 = 0;
			long int my_nr_ori_particles_done = 0;

			while (nr_slaves_done < node->size - 1)
			{
				// Receive a job request from a slave
				node->relion_MPI_Recv(MULTIDIM_ARRAY(first_last_nr_images), MULTIDIM_SIZE(first_last_nr_images), MPI_LONG, MPI_ANY_SOURCE, MPITAG_JOB_REQUEST, MPI_COMM_WORLD, status);
				// Which slave sent this request?
				int this_slave = status.MPI_SOURCE;

				//#define DEBUG_MPIEXP2
#ifdef DEBUG_MPIEXP2
				std::cerr << " MASTER RECEIVING from slave= " << this_slave<< " JOB_FIRST= " << JOB_FIRST << " JOB_LAST= " << JOB_LAST
						<< " JOB_NIMG= "<<JOB_NIMG<< " JOB_NPAR= "<<JOB_NPAR<< std::endl;
#endif
				// The first time a slave reports it only asks for input, but does not send output of a previous processing task. In that case JOB_NIMG==0
				// Otherwise, the master needs to receive and handle the updated metadata from the slaves
				if (JOB_NIMG > 0)
				{
					exp_metadata.resize(JOB_NIMG, METADATA_LINE_LENGTH);
					node->relion_MPI_Recv(MULTIDIM_ARRAY(exp_metadata), MULTIDIM_SIZE(exp_metadata), MY_MPI_DOUBLE, this_slave, MPITAG_METADATA, MPI_COMM_WORLD, status);

					// The master monitors the changes in the optimal orientations and classes
					monitorHiddenVariableChanges(JOB_FIRST, JOB_LAST);

					// The master then updates the mydata.MDimg table
					MlOptimiser::setMetaDataSubset(JOB_FIRST, JOB_LAST);
					if (nr_ori_particles_done - prev_step_done > progress_bar_step_size)
					{
						prev_step_done = nr_ori_particles_done;
						progress_bar(nr_ori_particles_done + JOB_NPAR);
					}
				}

				// See which random_subset this slave belongs to, and keep track of the number of ori_particles that have been processed already
				if (do_split_random_halves)
				{
					random_subset = (this_slave % 2 == 1) ? 1 : 2;
					if (random_subset == 1)
					{
						my_nr_ori_particles_done = nr_ori_particles_done_subset1;
						// random_subset1 is stored in second half of OriginalParticles
						JOB_FIRST = nr_ori_particles_done_subset1;
						JOB_LAST  = XMIPP_MIN(mydata.numberOfOriginalParticles(1) - 1, JOB_FIRST + nr_pool - 1);
					}
					else
					{
						my_nr_ori_particles_done = nr_ori_particles_done_subset2;
						// random_subset2 is stored in second half of OriginalParticles
						JOB_FIRST = mydata.numberOfOriginalParticles(1) + nr_ori_particles_done_subset2;
						JOB_LAST  = XMIPP_MIN(mydata.numberOfOriginalParticles() - 1, JOB_FIRST + nr_pool - 1);
					}
				}
				else
				{
					random_subset = 0;
					my_nr_ori_particles_done = nr_ori_particles_done;
					JOB_FIRST = nr_ori_particles_done;
					JOB_LAST  = XMIPP_MIN(mydata.numberOfOriginalParticles() - 1, JOB_FIRST + nr_pool - 1);
				}

				// Now send out a new job
				if (my_nr_ori_particles_done < mydata.numberOfOriginalParticles(random_subset))
				{

					MlOptimiser::getMetaAndImageDataSubset(JOB_FIRST, JOB_LAST, !do_parallel_disc_io);
					JOB_NIMG = YSIZE(exp_metadata);
					JOB_LEN_FN_IMG = exp_fn_img.length() + 1; // +1 to include \0 at the end of the string
					JOB_LEN_FN_CTF = exp_fn_ctf.length() + 1;
					JOB_LEN_FN_RECIMG = exp_fn_recimg.length() + 1;
				}
				else
				{
					// There are no more particles in the list
					JOB_FIRST = -1;
					JOB_LAST = -1;
					JOB_NIMG = 0;
					JOB_LEN_FN_IMG = 0;
					JOB_LEN_FN_CTF = 0;
					JOB_LEN_FN_RECIMG = 0;
					exp_metadata.clear();
					exp_imagedata.clear();

					// No more particles, this slave is done now
					nr_slaves_done++;
				}
#ifdef DEBUG_MPIEXP2
				std::cerr << " MASTER SENDING to slave= " << this_slave<< " JOB_FIRST= " << JOB_FIRST << " JOB_LAST= " << JOB_LAST
								<< " JOB_NIMG= "<<JOB_NIMG<< " JOB_NPAR= "<<JOB_NPAR<< std::endl;
#endif
				node->relion_MPI_Send(MULTIDIM_ARRAY(first_last_nr_images), MULTIDIM_SIZE(first_last_nr_images), MPI_LONG, this_slave, MPITAG_JOB_REPLY, MPI_COMM_WORLD);

				// Master also sends the required metadata and imagedata for this job
				if (JOB_NIMG > 0)
				{
					node->relion_MPI_Send(MULTIDIM_ARRAY(exp_metadata), MULTIDIM_SIZE(exp_metadata), MY_MPI_DOUBLE, this_slave, MPITAG_METADATA, MPI_COMM_WORLD);
					if (do_parallel_disc_io)
					{
						node->relion_MPI_Send((void*)exp_fn_img.c_str(), JOB_LEN_FN_IMG, MPI_CHAR, this_slave, MPITAG_METADATA, MPI_COMM_WORLD);
						// Send filenames of images to the slaves
						if (JOB_LEN_FN_CTF > 1)
							node->relion_MPI_Send((void*)exp_fn_ctf.c_str(), JOB_LEN_FN_CTF, MPI_CHAR, this_slave, MPITAG_METADATA, MPI_COMM_WORLD);
						if (JOB_LEN_FN_RECIMG > 1)
							node->relion_MPI_Send((void*)exp_fn_recimg.c_str(), JOB_LEN_FN_RECIMG, MPI_CHAR, this_slave, MPITAG_METADATA, MPI_COMM_WORLD);
					}
					else
					{
						// Send imagedata to the slaves
						node->relion_MPI_Send(MULTIDIM_ARRAY(exp_imagedata), MULTIDIM_SIZE(exp_imagedata), MY_MPI_DOUBLE, this_slave, MPITAG_IMAGE, MPI_COMM_WORLD);
					}
				}

				// Update the total number of particles that has been done already
				nr_ori_particles_done += JOB_NPAR;
				if (do_split_random_halves)
				{
					// Also update the number of particles that has been done for each subset
					if (random_subset == 1)
						nr_ori_particles_done_subset1 += JOB_NPAR;
					else
						nr_ori_particles_done_subset2 += JOB_NPAR;
				}
			}
        }
        catch (RelionError XE)
        {
            std::cerr << "master encountered error: " << XE;
            MlOptimiser::usage();
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        }

    }
    else
    {

    	try
    	{
			// Slaves do the real work (The slave does not need to know to which random_subset he belongs)

			// Start off with an empty job request
			JOB_FIRST = 0;
			JOB_LAST = -1; // So that initial nr_particles (=JOB_LAST-JOB_FIRST+1) is zero!
			JOB_NIMG = 0;
			JOB_LEN_FN_IMG = 0;
			JOB_LEN_FN_CTF = 0;
			JOB_LEN_FN_RECIMG = 0;
			node->relion_MPI_Send(MULTIDIM_ARRAY(first_last_nr_images), MULTIDIM_SIZE(first_last_nr_images), MPI_LONG, 0, MPITAG_JOB_REQUEST, MPI_COMM_WORLD);

			while (true)
			{
#ifdef TIMING
				timer.tic(TIMING_MPISLAVEWAIT1);
#endif

				//Receive a new bunch of particles
				node->relion_MPI_Recv(MULTIDIM_ARRAY(first_last_nr_images), MULTIDIM_SIZE(first_last_nr_images), MPI_LONG, 0, MPITAG_JOB_REPLY, MPI_COMM_WORLD, status);
#ifdef TIMING
				timer.toc(TIMING_MPISLAVEWAIT1);
#endif

				//Check whether I am done
				if (JOB_NIMG <= 0)
				{
#ifdef DEBUG
					std::cerr <<" slave "<< node->rank << " has finished expectation.."<<std::endl;
#endif
					exp_imagedata.clear();
					exp_metadata.clear();
					break;
				}
				else
				{
#ifdef TIMING
					timer.tic(TIMING_MPISLAVEWAIT2);
#endif
					// Also receive the imagedata and the metadata for these images from the master
					exp_metadata.resize(JOB_NIMG, METADATA_LINE_LENGTH);
					node->relion_MPI_Recv(MULTIDIM_ARRAY(exp_metadata), MULTIDIM_SIZE(exp_metadata), MY_MPI_DOUBLE, 0, MPITAG_METADATA, MPI_COMM_WORLD, status);

					// Receive the image filenames or the exp_imagedata
					if (do_parallel_disc_io)
					{
						// Resize the exp_fn_img strings
				        char* rec_buf;
				        rec_buf = (char *) malloc(JOB_LEN_FN_IMG);
				        node->relion_MPI_Recv(rec_buf, JOB_LEN_FN_IMG, MPI_CHAR, 0, MPITAG_METADATA, MPI_COMM_WORLD, status);
				        exp_fn_img = rec_buf;
				        free(rec_buf);
						if (JOB_LEN_FN_CTF > 1)
						{
					        char* rec_buf2;
					        rec_buf2 = (char *) malloc(JOB_LEN_FN_CTF);
					        node->relion_MPI_Recv(rec_buf2, JOB_LEN_FN_CTF, MPI_CHAR, 0, MPITAG_METADATA, MPI_COMM_WORLD, status);
					        exp_fn_ctf = rec_buf2;
					        free(rec_buf2);

						}
						if (JOB_LEN_FN_RECIMG > 1)
						{
					        char* rec_buf3;
					        rec_buf3 = (char *) malloc(JOB_LEN_FN_RECIMG);
					        node->relion_MPI_Recv(rec_buf3, JOB_LEN_FN_RECIMG, MPI_CHAR, 0, MPITAG_METADATA, MPI_COMM_WORLD, status);
					        exp_fn_recimg = rec_buf3;
					        free(rec_buf3);
						}
					}
					else
					{
						// resize the exp_imagedata array
						if (mymodel.data_dim == 3)
						{

							if (do_ctf_correction)
							{
								if (has_converged && do_use_reconstruct_images)
									exp_imagedata.resize(3*mymodel.ori_size, mymodel.ori_size, mymodel.ori_size);
								else
									exp_imagedata.resize(2*mymodel.ori_size, mymodel.ori_size, mymodel.ori_size);
							}
							else
							{
								if (has_converged && do_use_reconstruct_images)
									exp_imagedata.resize(2*mymodel.ori_size, mymodel.ori_size, mymodel.ori_size);
								else
									exp_imagedata.resize(mymodel.ori_size, mymodel.ori_size, mymodel.ori_size);
							}
						}
						else
						{
							if (has_converged && do_use_reconstruct_images)
								exp_imagedata.resize(2*JOB_NIMG, mymodel.ori_size, mymodel.ori_size);
							else
								exp_imagedata.resize(JOB_NIMG, mymodel.ori_size, mymodel.ori_size);
						}
						node->relion_MPI_Recv(MULTIDIM_ARRAY(exp_imagedata), MULTIDIM_SIZE(exp_imagedata), MY_MPI_DOUBLE, 0, MPITAG_IMAGE, MPI_COMM_WORLD, status);
					}

					// Now process these images
#ifdef DEBUG_MPIEXP
					std::cerr << " SLAVE EXECUTING node->rank= " << node->rank << " JOB_FIRST= " << JOB_FIRST << " JOB_LAST= " << JOB_LAST << std::endl;
#endif
#ifdef TIMING
					timer.toc(TIMING_MPISLAVEWAIT2);
					timer.tic(TIMING_MPISLAVEWORK);
#endif
					expectationSomeParticles(JOB_FIRST, JOB_LAST);
#ifdef TIMING
					timer.toc(TIMING_MPISLAVEWORK);
					timer.tic(TIMING_MPISLAVEWAIT3);
#endif

					// Report to the master how many particles I have processed
					node->relion_MPI_Send(MULTIDIM_ARRAY(first_last_nr_images), MULTIDIM_SIZE(first_last_nr_images), MPI_LONG, 0, MPITAG_JOB_REQUEST, MPI_COMM_WORLD);
					// Also send the metadata belonging to those
					node->relion_MPI_Send(MULTIDIM_ARRAY(exp_metadata), MULTIDIM_SIZE(exp_metadata), MY_MPI_DOUBLE, 0, MPITAG_METADATA, MPI_COMM_WORLD);

#ifdef TIMING
					timer.toc(TIMING_MPISLAVEWAIT3);
#endif

				}

			}
    	}
        catch (RelionError XE)
        {
            std::cerr << "slave "<< node->rank << " encountered error: " << XE;
            MlOptimiser::usage();
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        }

    }

    // Just make sure the temporary arrays are empty...
	exp_imagedata.clear();
	exp_metadata.clear();

    progress_bar(mydata.numberOfOriginalParticles());

#ifdef TIMING
    // Measure how long I have to wait for the rest
    timer.tic(TIMING_MPIWAIT);
    node->barrierWait();
    timer.toc(TIMING_MPIWAIT);
#endif

	// Wait until expected angular errors have been calculated
	MPI_Barrier(MPI_COMM_WORLD);

	// All slaves reset the size of their projector to zero tosave memory
	if (!node->isMaster())
	{
		for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
			mymodel.PPref[iclass].initialiseData(0);
	}

#define DEBUG_EXPECTATION_MPI_TMP
#ifdef DEBUG_EXPECTATION_MPI_TMP
	std::cerr << "MlOptimiserMpi::expectation: done, node: " << node->rank << std::endl;
#endif

}


void MlOptimiserMpi::combineAllWeightedSumsViaFile()
{

#ifdef TIMING
    timer.tic(TIMING_MPICOMBINEDISC);
#endif

#define COMBINE_WEIGHTS_FILE
#ifdef COMBINE_WEIGHTS_FILE
    std::cerr << "in combineAllWeightedSumsViaFile function, node: " << node->rank << std::endl;
#endif

	MultidimArray<DOUBLE> Mpack;
	FileName fn_pack;

	int nr_subsets = (do_split_random_halves) ? 2 : 1;

	// Only need to combine if there are more than one slaves per subset!
	if ((node->size - 1)/nr_subsets > 1)
	{
		// A. First all slaves pack up their wsum_model (this is done simultaneously)
		if (!node->isMaster())
		{
			wsum_model.pack(Mpack); // use negative piece and nr_pieces to only make a single Mpack, i.e. do not split into multiple pieces
		}

		// B. All slaves write their Mpack to disc. Do this SEQUENTIALLY to prevent heavy load on disc I/O
		for (int this_slave = 1; this_slave < node->size; this_slave++ )
		{
			if (this_slave == node->rank)
			{
				fn_pack.compose(fn_out+"_rank", node->rank, "tmp");
				if (fn_scratch != "")
					fn_pack = fn_scratch + "/" + fn_pack;

				Mpack.writeBinary(fn_pack);
				//std::cerr << "Rank "<< node->rank <<" has written: "<<fn_pack << " sum= "<<Mpack.sum()<< std::endl;
			}
			if (!do_parallel_disc_io)
				MPI_Barrier(MPI_COMM_WORLD);
		}
#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", before mpi barrier, B" << std::endl;
#endif
		MPI_Barrier(MPI_COMM_WORLD);

#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", AFTER mpi barrier, B" << std::endl;
#endif

		// C. First slave of each subset reads all other slaves' Mpack; sum; and write sum to disc
		// Again, do this SEQUENTIALLY to prevent heavy load on disc I/O
		for (int first_slave = 1; first_slave <= nr_subsets; first_slave++)
		{
			if (node->rank == first_slave)
			{
				for (int other_slave = first_slave + nr_subsets; other_slave < node->size; other_slave+= nr_subsets )
				{
					fn_pack.compose(fn_out+"_rank", other_slave, "tmp");
					if (fn_scratch != "")
						fn_pack = fn_scratch + "/" + fn_pack;

					Mpack.readBinaryAndSum(fn_pack);
					//std::cerr << "Slave "<<node->rank<<" has read "<<fn_pack<< " sum= "<<Mpack.sum() << std::endl;
				}
				// After adding all Mpacks together: write the sum to disc
				fn_pack.compose(fn_out+"_rank", node->rank, "tmp");
				if (fn_scratch != "")
					fn_pack = fn_scratch + "/" + fn_pack;

				Mpack.writeBinary(fn_pack);
				//std::cerr << "Slave "<<node->rank<<" is writing total SUM in "<<fn_pack << std::endl;
			}
			if (!do_parallel_disc_io)
				MPI_Barrier(MPI_COMM_WORLD);
		}

#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", BEFORE mpi barrier, C" << std::endl;
#endif

		MPI_Barrier(MPI_COMM_WORLD);

#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", after mpi barrier, C" << std::endl;
#endif

		// D. All other slaves read the summed Mpack from their first_slave
		// Again, do this SEQUENTIALLY to prevent heavy load on disc I/O
		for (int this_slave = nr_subsets + 1; this_slave < node->size; this_slave++ )
		{
			if (this_slave == node->rank)
			{
				// Find out who is the first slave in this subset
				int first_slave;
				if (!do_split_random_halves)
					first_slave = 1;
				else
					first_slave = (this_slave % 2 == 1) ? 1 : 2;

				// Read the corresponding Mpack (which now contains the sum of all Mpacks)
				fn_pack.compose(fn_out+"_rank", first_slave, "tmp");
				if (fn_scratch != "")
					fn_pack = fn_scratch + "/" + fn_pack;

				Mpack.readBinary(fn_pack);
				//std::cerr << "Rank "<< node->rank <<" has read: "<<fn_pack << " sum= "<<Mpack.sum()<< std::endl;
			}
			if (!do_parallel_disc_io)
				MPI_Barrier(MPI_COMM_WORLD);
		}

#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", before mpi barrier, D" << std::endl;
#endif

		MPI_Barrier(MPI_COMM_WORLD);

#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", AFTER mpi barrier, D" << std::endl;
#endif

		// E. All slaves delete their own temporary file
		// Again, do this SEQUENTIALLY to prevent heavy load on disc I/O
		for (int this_slave = 1; this_slave < node->size; this_slave++ )
		{
			if (this_slave == node->rank)
			{
				fn_pack.compose(fn_out+"_rank", node->rank, "tmp");
				if (fn_scratch != "")
					fn_pack = fn_scratch + "/" + fn_pack;

				remove((fn_pack).c_str());
				//std::cerr << "Rank "<< node->rank <<" has deleted: "<<fn_pack << std::endl;
			}
			if (!do_parallel_disc_io)
				MPI_Barrier(MPI_COMM_WORLD);
		}

		// F. Finally all slaves unpack Msum into their wsum_model (do this simultaneously)
		if (!node->isMaster())
			wsum_model.unpack(Mpack);

	} // end if ((node->size - 1)/nr_subsets > 1)

#ifdef COMBINE_WEIGHTS_FILE
		std::cerr << "node: " << node->rank << ", end combineAllWeightedSumsViaFile function" << std::endl;
#endif

#ifdef TIMING
    timer.toc(TIMING_MPICOMBINEDISC);
#endif

}

void MlOptimiserMpi::combineAllWeightedSums()
{

#ifdef TIMING
    timer.tic(TIMING_MPICOMBINENETW);
#endif

    // Pack all weighted sums in Mpack
	MultidimArray<DOUBLE> Mpack, Msum;
	MPI_Status status;

	// First slave manually sums over all other slaves of it's subset
	// And then sends results back to all those slaves
	// When splitting the data into two random halves, perform two passes: one for each subset
	int nr_subsets = (do_split_random_halves) ? 2 : 1;

	// Only combine weighted sums if there are more than one slaves per subset!
	if ((node->size - 1)/nr_subsets > 1)
	{
		// Loop over possibly multiple instances of Mpack of maximum size
		int piece = 0;
		int nr_pieces = 1;
		long int pack_size;
		while (piece < nr_pieces)
		{
			// All nodes except those who will reset nr_pieces piece will pass while loop in next pass
			nr_pieces = 0;

			// First all slaves pack up their wsum_model
			if (!node->isMaster())
			{
				wsum_model.pack(Mpack, piece, nr_pieces);
				// The first slave(s) set Msum equal to Mpack, the others initialise to zero
				if (node->rank <= nr_subsets)
					Msum = Mpack;
				else
					Msum.initZeros(Mpack);
			}


			// Loop through all slaves: each slave sends its Msum to the next slave for its subset.
			// Each next slave sums its own Mpack to the received Msum and sends it on to the next slave

			for (int this_slave = 1; this_slave < node->size; this_slave++ )
			{
				// Find out who is the first slave in this subset
				int first_slave;
				if (!do_split_random_halves)
					first_slave = 1;
				else
					first_slave = (this_slave % 2 == 1) ? 1 : 2;

				// Find out who is the next slave in this subset
				int other_slave = this_slave + nr_subsets;

				if (other_slave < node->size)
				{
					if (node->rank == this_slave)
					{
#ifdef DEBUG
						std::cerr << " AA SEND node->rank= " << node->rank << " MULTIDIM_SIZE(Msum)= "<< MULTIDIM_SIZE(Msum)
								<< " this_slave= " << this_slave << " other_slave= "<<other_slave << std::endl;
#endif
						node->relion_MPI_Send(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, other_slave, MPITAG_PACK, MPI_COMM_WORLD);
					}
					else if (node->rank == other_slave)
					{
						MPI_Recv(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, this_slave, MPITAG_PACK, MPI_COMM_WORLD, &status);
#ifdef DEBUG
						std::cerr << " AA RECV node->rank= " << node->rank  << " MULTIDIM_SIZE(Msum)= "<< MULTIDIM_SIZE(Msum)
								<< " this_slave= " << this_slave << " other_slave= "<<other_slave << std::endl;
#endif
						// Add my own Mpack to send onwards in the next step
						Msum += Mpack;
					}
				}
				else
				{
					// Now this_slave has reached the last slave, which passes the final Msum to the first one (i.e. first_slave)
					if (node->rank == this_slave)
					{
#ifdef DEBUG
						std::cerr << " BB SEND node->rank= " << node->rank  << " MULTIDIM_SIZE(Msum)= "<< MULTIDIM_SIZE(Msum)
								<< " this_slave= " << this_slave << " first_slave= "<<first_slave << std::endl;
#endif
						node->relion_MPI_Send(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, first_slave, MPITAG_PACK, MPI_COMM_WORLD);
					}
					else if (node->rank == first_slave)
					{
						node->relion_MPI_Recv(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, this_slave, MPITAG_PACK, MPI_COMM_WORLD, status);
#ifdef DEBUG
						std::cerr << " BB RECV node->rank= " << node->rank  << " MULTIDIM_SIZE(Msum)= "<< MULTIDIM_SIZE(Msum)
								<< " this_slave= " << this_slave << " first_slave= "<<first_slave << std::endl;
#endif
					}
				}
			} // end for this_slave

			// Now loop through all slaves again to pass around the Msum
			for (int this_slave = 1; this_slave < node->size; this_slave++ )
			{
				// Find out who is the next slave in this subset
				int other_slave = this_slave + nr_subsets;

				// Do not send to the last slave, because it already had its Msum from the cycle above, therefore subtract nr_subsets from node->size
				if (other_slave < node->size - nr_subsets)
				{
					if (node->rank == this_slave)
					{
#ifdef DEBUG
						std::cerr << " CC SEND node->rank= " << node->rank << " MULTIDIM_SIZE(Msum)= "<< MULTIDIM_SIZE(Msum)
								<< " this_slave= " << this_slave << " other_slave= "<<other_slave << std::endl;
#endif
						node->relion_MPI_Send(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, other_slave, MPITAG_PACK, MPI_COMM_WORLD);
					}
					else if (node->rank == other_slave)
					{
						node->relion_MPI_Recv(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, this_slave, MPITAG_PACK, MPI_COMM_WORLD, status);
#ifdef DEBUG
						std::cerr << " CC RECV node->rank= " << node->rank << " MULTIDIM_SIZE(Msum)= "<< MULTIDIM_SIZE(Msum)
								<< " this_slave= " << this_slave << " other_slave= "<<other_slave << std::endl;
#endif
					}
				}
			} // end for this_slave


			// Finally all slaves unpack Msum into their wsum_model
			if (!node->isMaster())
			{
				// Subtract 1 from piece because it was incremented already...
				wsum_model.unpack(Msum, piece - 1);
			}


		} // end for piece

		MPI_Barrier(MPI_COMM_WORLD);
	}

#ifdef TIMING
    timer.toc(TIMING_MPICOMBINENETW);
#endif

}

void MlOptimiserMpi::combineWeightedSumsTwoRandomHalvesViaFile()
{
	// Just sum the weighted halves from slave 1 and slave 2 and Bcast to everyone else
	if (!do_split_random_halves)
		REPORT_ERROR("MlOptimiserMpi::combineWeightedSumsTwoRandomHalvesViaFile BUG: you cannot combineWeightedSumsTwoRandomHalves if you have not split random halves");

	MultidimArray<DOUBLE> Mpack;
	FileName fn_pack = fn_out + ".tmp";
        if (fn_scratch != "")
            fn_pack = fn_scratch + "/" + fn_pack;

	// Everyone packs up his wsum_model (simultaneously)
	// The slaves from 3 and onwards also need this in order to have the correct Mpack size to be able to read in the summed Mpack
	if (!node->isMaster())
		wsum_model.pack(Mpack);

	// Rank 2 writes it Mpack to file
	if (node->rank == 2)
	{
		Mpack.writeBinary(fn_pack);
		//std::cerr << "Rank "<< node->rank <<" has written: "<<fn_pack << " sum= "<<Mpack.sum()<< std::endl;
	}

	// Wait until rank2 is ready
	MPI_Barrier(MPI_COMM_WORLD);

	// Now rank1 reads the file of rank2 and adds it to its own Mpack and (over)write the sum to disc
	if (node->rank == 1)
	{
		Mpack.readBinaryAndSum(fn_pack);
		Mpack.writeBinary(fn_pack);
	}

	// Now all slaves read the sum and unpack
	// Do this sequentially in order not to have very heavy disc I/O
	for (int this_slave = 2; this_slave < node->size; this_slave++)
	{
		if (node->rank == this_slave)
			Mpack.readBinary(fn_pack);
		if (!do_parallel_disc_io)
			MPI_Barrier(MPI_COMM_WORLD);
	}

	// in case we're doing do_parallel_disc_io
	MPI_Barrier(MPI_COMM_WORLD);

	// Remove temporary file
	if (node->rank == 1)
		remove(fn_pack.c_str());

	// Then everyone except the master unpacks
	if (!node->isMaster())
		wsum_model.unpack(Mpack);

}

void MlOptimiserMpi::combineWeightedSumsTwoRandomHalves()
{
	// Just sum the weighted halves from slave 1 and slave 2 and Bcast to everyone else
	if (!do_split_random_halves)
		REPORT_ERROR("MlOptimiserMpi::combineWeightedSumsTwoRandomHalves BUG: you cannot combineWeightedSumsTwoRandomHalves if you have not split random halves");

	MultidimArray<DOUBLE> Mpack, Msum;
	MPI_Status status;

	int piece = 0;
	int nr_pieces = 1;
	long int pack_size;
	while (piece < nr_pieces)
	{
		// All nodes except those who will reset nr_pieces will pass while next time
		nr_pieces = 0;

		if (node->rank == 2)
		{
			wsum_model.pack(Mpack, piece, nr_pieces, false); // do not clear the model!
			node->relion_MPI_Send(MULTIDIM_ARRAY(Mpack), MULTIDIM_SIZE(Mpack), MY_MPI_DOUBLE, 1, MPITAG_PACK, MPI_COMM_WORLD);
			Mpack.clear();
		}
		else if (node->rank == 1)
		{
			std::cout << " Combining two random halves ..."<< std::endl;
			wsum_model.pack(Mpack, piece, nr_pieces);
			Msum.initZeros(Mpack);
			node->relion_MPI_Recv(MULTIDIM_ARRAY(Msum), MULTIDIM_SIZE(Msum), MY_MPI_DOUBLE, 2, MPITAG_PACK, MPI_COMM_WORLD, status);
			Msum += Mpack;
			// Unpack the sum (subtract 1 from piece because it was incremented already...)
			wsum_model.unpack(Msum, piece - 1);
			Msum.clear();
			Mpack.clear();
		}
	}

	// Now slave 1 has the sum of the two halves
	MPI_Barrier(MPI_COMM_WORLD);

	// Bcast to everyone
	piece = 0;
	nr_pieces = 1;
	while (piece < nr_pieces)
	{

		// All nodes except those who will reset nr_pieces will pass while next time
		nr_pieces = 0;

		// The master does not have a wsum_model!
		if (!node->isMaster())
		{
			// Let's have everyone repack their Mpack, so we know the size etc
			wsum_model.pack(Mpack, piece, nr_pieces);

			// rank one sends Mpack to everyone else
			if (node->rank == 1)
			{
				for (int other_slave = 2; other_slave < node->size; other_slave++)
					node->relion_MPI_Send(MULTIDIM_ARRAY(Mpack), MULTIDIM_SIZE(Mpack), MY_MPI_DOUBLE, other_slave, MPITAG_PACK, MPI_COMM_WORLD);
			}
			else
			{
				node->relion_MPI_Recv(MULTIDIM_ARRAY(Mpack), MULTIDIM_SIZE(Mpack), MY_MPI_DOUBLE, 1, MPITAG_PACK, MPI_COMM_WORLD, status);
			}

			// Everyone unpacks the new Mpack
			wsum_model.unpack(Mpack, piece - 1);
			Mpack.clear();

		}

	}

}

void MlOptimiserMpi::maximization()
{
#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::maximization: Entering " << std::endl;
#endif

#ifdef TIMING
		timer.tic(TIMING_RECONS);
#endif

	if (verb > 0)
	{
		std::cout << " Maximization ..."<< std::endl;
		init_progress_bar(mymodel.nr_classes);
	}


	// First reconstruct all classes in parallel
	for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
    {
		if (mymodel.pdf_class[iclass] > 0.)
		{
			// Parallelise: each MPI-node has a different reference

			// The code below will NOT work if nr_classes > 1 AND do_split_random_halves, but that is disabled anyway...
			// Master does not participate in reconstruction tasks
			int reconstruct_rank1 = (iclass % (node->size - 1) ) + 1;
			//std::cerr << " size00 " <<  MULTIDIM_SIZE(mymodel.Iref[0]) << " rank= "<<node->rank << " iclass= "<<iclass << std::endl;
			if (node->rank == reconstruct_rank1)
			{
				wsum_model.BPref[iclass].reconstruct(mymodel.Iref[iclass], gridding_nr_iter, do_map,
						mymodel.tau2_fudge_factor, mymodel.tau2_class[iclass], mymodel.sigma2_class[iclass],
						mymodel.data_vs_prior_class[iclass], mymodel.fsc_halves_class[iclass], wsum_model.pdf_class[iclass],
						do_split_random_halves, (do_join_random_halves || do_always_join_random_halves), nr_threads, minres_map);

				// Also perform the unregularized reconstruction
				if (do_auto_refine && has_converged)
					readTemporaryDataAndWeightArraysAndReconstruct(iclass, 1);

			}
			//std::cerr << " size01 " <<  MULTIDIM_SIZE(mymodel.Iref[0]) << " rank= "<<node->rank << " iclass= "<<iclass << std::endl;
			// In some cases there is not enough memory to reconstruct two random halves in parallel
			// Therefore the following option exists to perform them sequentially
			if (do_sequential_halves_recons)
				MPI_Barrier(MPI_COMM_WORLD);

			// When splitting the data into two random halves, perform two reconstructions in parallel: one for each subset
			if (do_split_random_halves)
			{
				int reconstruct_rank2 =(iclass % (node->size - 1) ) + 2;

				if (node->rank == reconstruct_rank2)
				{
					// Rank 2 does not need to do the joined reconstruction
					if (!do_join_random_halves)
						wsum_model.BPref[iclass].reconstruct(mymodel.Iref[iclass], gridding_nr_iter, do_map,
								mymodel.tau2_fudge_factor, mymodel.tau2_class[iclass], mymodel.sigma2_class[iclass],
								mymodel.data_vs_prior_class[iclass], mymodel.fsc_halves_class[iclass], wsum_model.pdf_class[iclass],
								do_split_random_halves, do_join_random_halves, nr_threads, minres_map);

					// But rank 2 always does the unfiltered reconstruction
					if (do_auto_refine && has_converged)
						readTemporaryDataAndWeightArraysAndReconstruct(iclass, 2);

				}
			}

		} // endif pdf_class[iclass] > 0.
		else
		{
			mymodel.Iref[iclass].initZeros();
		}

//#define DEBUG_RECONSTRUCT
#ifdef DEBUG_RECONSTRUCT
		MPI_Barrier( MPI_COMM_WORLD);
#endif
    }

#ifdef DEBUG
	std::cerr << "rank= "<<node->rank<<" has reached barrier of reconstruction" << std::endl;
#endif
	MPI_Barrier(MPI_COMM_WORLD);

#ifdef DEBUG
	std::cerr << "All classes have been reconstructed" << std::endl;
#endif

	// Once reconstructed, broadcast new models to all other nodes
	// This cannot be done in the reconstruction loop itself because then it will be executed sequentially
	for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
	{

		if (do_split_random_halves && !do_join_random_halves)
		{
			MPI_Status status;
			// Make sure I am sending from the rank where the reconstruction was done (see above) to all other slaves of this subset
			// Loop twice through this, as each class was reconstructed by two different slaves!!
			int nr_subsets = 2;
			for (int isubset = 1; isubset <= nr_subsets; isubset++)
			{
				if (node->myRandomSubset() == isubset)
				{
					int reconstruct_rank = (iclass % (node->size - 1) ) + isubset; // first pass subset1, second pass subset2
					int my_first_recv = node->myRandomSubset();

					for (int recv_node = my_first_recv; recv_node < node->size; recv_node += nr_subsets)
					{
						if (node->rank == reconstruct_rank && recv_node != node->rank)
						{
#ifdef DEBUG
							std::cerr << "isubset= "<<isubset<<" Sending iclass="<<iclass<<" from node "<<reconstruct_rank<<" to node "<<recv_node << std::endl;
#endif
							node->relion_MPI_Send(MULTIDIM_ARRAY(mymodel.Iref[iclass]), MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, recv_node, MPITAG_IMAGE, MPI_COMM_WORLD);
							node->relion_MPI_Send(MULTIDIM_ARRAY(mymodel.data_vs_prior_class[iclass]), MULTIDIM_SIZE(mymodel.data_vs_prior_class[iclass]), MY_MPI_DOUBLE, recv_node, MPITAG_METADATA, MPI_COMM_WORLD);
							node->relion_MPI_Send(MULTIDIM_ARRAY(mymodel.sigma2_class[iclass]), MULTIDIM_SIZE(mymodel.sigma2_class[iclass]), MY_MPI_DOUBLE, recv_node, MPITAG_DOUBLE, MPI_COMM_WORLD);
							node->relion_MPI_Send(MULTIDIM_ARRAY(mymodel.fsc_halves_class[iclass]), MULTIDIM_SIZE(mymodel.fsc_halves_class[iclass]), MY_MPI_DOUBLE, recv_node, MPITAG_RANDOMSEED, MPI_COMM_WORLD);
						}
						else if (node->rank != reconstruct_rank && node->rank == recv_node)
						{
							//std::cerr << "isubset= "<<isubset<< " Receiving iclass="<<iclass<<" from node "<<reconstruct_rank<<" at node "<<node->rank<< std::endl;
							node->relion_MPI_Recv(MULTIDIM_ARRAY(mymodel.Iref[iclass]), MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPITAG_IMAGE, MPI_COMM_WORLD, status);
							node->relion_MPI_Recv(MULTIDIM_ARRAY(mymodel.data_vs_prior_class[iclass]), MULTIDIM_SIZE(mymodel.data_vs_prior_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPITAG_METADATA, MPI_COMM_WORLD, status);
							node->relion_MPI_Recv(MULTIDIM_ARRAY(mymodel.sigma2_class[iclass]), MULTIDIM_SIZE(mymodel.sigma2_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPITAG_DOUBLE, MPI_COMM_WORLD, status);
							node->relion_MPI_Recv(MULTIDIM_ARRAY(mymodel.fsc_halves_class[iclass]), MULTIDIM_SIZE(mymodel.fsc_halves_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPITAG_RANDOMSEED, MPI_COMM_WORLD, status);
#ifdef DEBUG
							std::cerr << "isubset= "<<isubset<< " Received!!!="<<iclass<<" from node "<<reconstruct_rank<<" at node "<<node->rank<< std::endl;
#endif
						}
					}

				}
			}
			// No one should continue until we're all here
			MPI_Barrier(MPI_COMM_WORLD);

			// Now all slaves have all relevant reconstructions
			// TODO: someone should also send reconstructions to the master (for comparison with other subset?)

		}
		else
		{
			int reconstruct_rank = (iclass % (node->size - 1) ) + 1;
			// Broadcast the reconstructed references to all other MPI nodes
			node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.Iref[iclass]),
					MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);
			// Broadcast the data_vs_prior spectra to all other MPI nodes
			node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.data_vs_prior_class[iclass]),
					MULTIDIM_SIZE(mymodel.data_vs_prior_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);
			// Broadcast the sigma2_class spectra to all other MPI nodes
			node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.sigma2_class[iclass]),
					MULTIDIM_SIZE(mymodel.sigma2_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);

		}

		// Re-set the origin (this may be lost in some cases??)
		mymodel.Iref[iclass].setXmippOrigin();
	}
#ifdef TIMING
		timer.toc(TIMING_RECONS);
#endif

	if (node->isMaster())
	{
		// If we're realinging movies then the master needs to update the priors
		// These will then be re-distributed to the slaves in the expectation step (through get/setMetadataSubset)
		// Switch this off, as we're only doing a single round of movie processing anyway!
		//if (do_realign_movies)
		//	updatePriorsForMovieFrames();

		// The master also updates the changes in hidden variables
		updateOverallChangesInHiddenVariables();
	}
	else
	{
		// Now do the maximisation of all other parameters (and calculate the tau2_class-spectra of the reconstructions
		// The lazy master never does this: it only handles metadata and does not have the weighted sums
		maximizationOtherParameters();
	}

	// The master broadcasts the changes in hidden variables to all other nodes
	node->relion_MPI_Bcast(&current_changes_optimal_classes, 1, MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&current_changes_optimal_orientations, 1, MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&current_changes_optimal_offsets, 1, MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&nr_iter_wo_large_hidden_variable_changes, 1, MPI_INT, 0, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&smallest_changes_optimal_classes, 1, MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&smallest_changes_optimal_offsets, 1, MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);
	node->relion_MPI_Bcast(&smallest_changes_optimal_orientations, 1, MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);

	if (verb > 0)
		progress_bar(mymodel.nr_classes);

#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::maximization: done" << std::endl;
#endif

}

void MlOptimiserMpi::joinTwoHalvesAtLowResolution()
{
#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::joinTwoHalvesAtLowResolution: Entering " << std::endl;
#endif

	if (!do_split_random_halves)
		REPORT_ERROR("BUG: you should not be in MlOptimiserMpi::joinTwoHalvesAtLowResolution!");

	// Loop over all classes (this will be just one class for now...)
	DOUBLE myres = XMIPP_MAX(low_resol_join_halves, 1./mymodel.current_resolution);
	int lowres_r_max = CEIL(mymodel.ori_size * mymodel.pixel_size / myres);

	for (int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
	{
		if (node->rank == 1 || node->rank == 2)
		{
			MultidimArray<Complex > lowres_data;
			MultidimArray<DOUBLE > lowres_weight;
			wsum_model.BPref[iclass].getLowResDataAndWeight(lowres_data, lowres_weight, lowres_r_max);

			if (node->rank == 2)
			{
				MPI_Status status;

				// The second slave sends its lowres_data and lowres_weight to the first slave
				node->relion_MPI_Send(MULTIDIM_ARRAY(lowres_data), 2*MULTIDIM_SIZE(lowres_data), MY_MPI_DOUBLE, 1, MPITAG_IMAGE, MPI_COMM_WORLD);
				node->relion_MPI_Send(MULTIDIM_ARRAY(lowres_weight), MULTIDIM_SIZE(lowres_weight), MY_MPI_DOUBLE, 1, MPITAG_DOUBLE, MPI_COMM_WORLD);

				// Now the first slave is calculating the average....

				// Then the second slave receives the average back from the first slave
				node->relion_MPI_Recv(MULTIDIM_ARRAY(lowres_data), 2*MULTIDIM_SIZE(lowres_data), MY_MPI_DOUBLE, 1, MPITAG_IMAGE, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(lowres_weight), MULTIDIM_SIZE(lowres_weight), MY_MPI_DOUBLE, 1, MPITAG_DOUBLE, MPI_COMM_WORLD, status);


			}
			else if (node->rank == 1)
			{

				std::cout << " Averaging half-reconstructions up to " << myres << " Angstrom resolution to prevent diverging orientations ..." << std::endl;
				std::cout << " Note that only for higher resolutions the FSC-values are according to the gold-standard!" << std::endl;
				MPI_Status status;
				MultidimArray<Complex > lowres_data_half2;
				MultidimArray<DOUBLE > lowres_weight_half2;
				lowres_data_half2.resize(lowres_data);
				lowres_weight_half2.resize(lowres_weight);
#ifdef DEBUG
				std::cerr << "AAArank=1 lowresdata: "; lowres_data.printShape();
				std::cerr << "AAArank=1 lowresdata_half2: "; lowres_data_half2.printShape();
#endif
				// The first slave receives the average from the second slave
				node->relion_MPI_Recv(MULTIDIM_ARRAY(lowres_data_half2), 2*MULTIDIM_SIZE(lowres_data_half2), MY_MPI_DOUBLE, 2, MPITAG_IMAGE, MPI_COMM_WORLD, status);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(lowres_weight_half2), MULTIDIM_SIZE(lowres_weight_half2), MY_MPI_DOUBLE, 2, MPITAG_DOUBLE, MPI_COMM_WORLD, status);

				// The first slave calculates the average of the two lowres_data and lowres_weight arrays
#ifdef DEBUG
				std::cerr << "BBBrank=1 lowresdata: "; lowres_data.printShape();
				std::cerr << "BBBrank=1 lowresdata_half2: "; lowres_data_half2.printShape();
#endif
				FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(lowres_data)
				{
					DIRECT_MULTIDIM_ELEM(lowres_data, n)   += DIRECT_MULTIDIM_ELEM(lowres_data_half2, n) ;
					DIRECT_MULTIDIM_ELEM(lowres_data, n)   /= 2.;
					DIRECT_MULTIDIM_ELEM(lowres_weight, n) += DIRECT_MULTIDIM_ELEM(lowres_weight_half2, n) ;
					DIRECT_MULTIDIM_ELEM(lowres_weight, n) /= 2.;
				}

				// The first slave sends the average lowres_data and lowres_weight also back to the second slave
				node->relion_MPI_Send(MULTIDIM_ARRAY(lowres_data), 2*MULTIDIM_SIZE(lowres_data), MY_MPI_DOUBLE, 2, MPITAG_IMAGE, MPI_COMM_WORLD);
				node->relion_MPI_Send(MULTIDIM_ARRAY(lowres_weight), MULTIDIM_SIZE(lowres_weight), MY_MPI_DOUBLE, 2, MPITAG_DOUBLE, MPI_COMM_WORLD);

			}

			// Now that both slaves have the average lowres arrays, set them back into the backprojector
			wsum_model.BPref[iclass].setLowResDataAndWeight(lowres_data, lowres_weight, lowres_r_max);
		}
	}


#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::joinTwoHalvesAtLowResolution: done" << std::endl;
#endif

}

void MlOptimiserMpi::writeTemporaryDataAndWeightArrays()
{

	if (mymodel.ref_dim == 3 && (node->rank == 1 || (do_split_random_halves && node->rank == 2) ) )
	{
		Image<DOUBLE> It;
		FileName fn_root = fn_out + "_half" + integerToString(node->rank);;

		// Write out temporary arrays for all classes
		for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
		{
			FileName fn_tmp;
			fn_tmp.compose(fn_root+"_class", iclass+1, "", 3);
#ifdef DEBUG_WRITE_TEMPORARY
			std::cerr << "node: " << node << ", iclass: " << iclass << ", fn_tmp: " << fn_tmp << std::endl;
#endif
			if (mymodel.pdf_class[iclass] > 0.)
			{
				It().resize(wsum_model.BPref[iclass].data);
				FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(It())
				{
					DIRECT_MULTIDIM_ELEM(It(), n) = (DIRECT_MULTIDIM_ELEM(wsum_model.BPref[iclass].data, n)).real;
				}
				It.write(fn_tmp+"_data_real.mrc");
				FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(It())
				{
					DIRECT_MULTIDIM_ELEM(It(), n) = (DIRECT_MULTIDIM_ELEM(wsum_model.BPref[iclass].data, n)).imag;
				}
				It.write(fn_tmp+"_data_imag.mrc");
				It()=wsum_model.BPref[iclass].weight;
				It.write(fn_tmp+"_weight.mrc");
			}
		}
    }

}

void MlOptimiserMpi::readTemporaryDataAndWeightArraysAndReconstruct(int iclass, int ihalf)
{
	if (mymodel.ref_dim == 3)
	{
		MultidimArray<DOUBLE> dummy;
		Image<DOUBLE> Iunreg, Itmp;
		FileName fn_root = fn_out + "_half" + integerToString(ihalf);;
		fn_root.compose(fn_root+"_class", iclass+1, "", 3);

		// Read temporary arrays back in
		Itmp.read(fn_root+"_data_real.mrc");
		Itmp().setXmippOrigin();
		Itmp().xinit=0;
		if (!Itmp().sameShape(wsum_model.BPref[iclass].data))
		{
			wsum_model.BPref[iclass].data.printShape(std::cerr);
			Itmp().printShape(std::cerr);
			REPORT_ERROR("Incompatible size of "+fn_root+"_data_real.mrc");
		}
		FOR_ALL_ELEMENTS_IN_ARRAY3D(Itmp())
		{
			A3D_ELEM(wsum_model.BPref[iclass].data, k, i, j).real = A3D_ELEM(Itmp(), k, i, j);
		}

		Itmp.read(fn_root+"_data_imag.mrc");
		Itmp().setXmippOrigin();
		Itmp().xinit=0;
		if (!Itmp().sameShape(wsum_model.BPref[iclass].data))
		{
			wsum_model.BPref[iclass].data.printShape(std::cerr);
			Itmp().printShape(std::cerr);
			REPORT_ERROR("Incompatible size of "+fn_root+"_data_imag.mrc");
		}
		FOR_ALL_ELEMENTS_IN_ARRAY3D(Itmp())
		{
			A3D_ELEM(wsum_model.BPref[iclass].data, k, i, j).imag = A3D_ELEM(Itmp(), k, i, j);
		}

		Itmp.read(fn_root+"_weight.mrc");
		Itmp().setXmippOrigin();
		Itmp().xinit=0;
		if (!Itmp().sameShape(wsum_model.BPref[iclass].weight))
		{
			wsum_model.BPref[iclass].weight.printShape(std::cerr);
			Itmp().printShape(std::cerr);
			REPORT_ERROR("Incompatible size of "+fn_root+"_weight.mrc");
		}
		FOR_ALL_ELEMENTS_IN_ARRAY3D(Itmp())
		{
			A3D_ELEM(wsum_model.BPref[iclass].weight, k, i, j) = A3D_ELEM(Itmp(), k, i, j);
		}

		// Now perform the unregularized reconstruction
		wsum_model.BPref[iclass].reconstruct(Iunreg(), gridding_nr_iter, false, 1., dummy, dummy, dummy, dummy, 1., false, true, nr_threads, -1);

		// Update header information
		DOUBLE avg, stddev, minval, maxval;
	    Iunreg().computeStats(avg, stddev, minval, maxval);
	    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_MIN, minval);
	    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_MAX, maxval);
	    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_AVG, avg);
	    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_STDDEV, stddev);
		Iunreg.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size);
		Iunreg.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size);
		Iunreg.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size);
		// And write the resulting model to disc
		Iunreg.write(fn_root+"_unfil.mrc");


		// remove temporary arrays from the disc
		// under the condition of not using ITK solvent flatten
		// or if using ITK solvent flatten mask, but not do phase random fsc correction
		// the program will also remove these files
		if(!use_itk_flatten || (use_itk_flatten && !do_phase_random_fsc ))
		{
			remove((fn_root+"_data_real.mrc").c_str());
			remove((fn_root+"_data_imag.mrc").c_str());
			remove((fn_root+"_weight.mrc").c_str());
		}
	}
}

void MlOptimiserMpi::compareTwoHalves()
{
#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::compareTwoHalves: Entering " << std::endl;
#endif

	if (!do_split_random_halves)
		REPORT_ERROR("ERROR: you should not be in MlOptimiserMpi::compareTwoHalves!");

	// Loop over all classes
	for (int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
	{
		if (node->rank == 1 || node->rank == 2)
		{

			// The first two slaves calculate the downsampled average
			MultidimArray<Complex > avg1;
			wsum_model.BPref[iclass].getDownsampledAverage(avg1);
//#define DEBUG_FSC
#ifdef DEBUG_FSC
			MultidimArray<Complex > avg;
			MultidimArray<DOUBLE> Mavg;
			Mavg.resize(mymodel.ori_size, mymodel.ori_size, mymodel.ori_size);
			FourierTransformer transformer_debug;
			transformer_debug.setReal(Mavg);
			transformer_debug.getFourierAlias(avg);
			wsum_model.BPref[0].decenter(avg1,avg, wsum_model.BPref[0].r_max * wsum_model.BPref[0].r_max);
			transformer_debug.inverseFourierTransform();
			FileName fnt;
			fnt.compose("downsampled_avg_half",node->rank,"spi");
			Image<DOUBLE> It;
			CenterFFT(Mavg, true);
			It()=Mavg;
			It.write(fnt);
#endif
			if (node->rank == 2)
			{
				// The second slave sends its average to the first slave
				node->relion_MPI_Send(MULTIDIM_ARRAY(avg1), 2*MULTIDIM_SIZE(avg1), MY_MPI_DOUBLE, 1, MPITAG_IMAGE, MPI_COMM_WORLD);
			}
			else if (node->rank == 1)
			{

				std::cout << " Calculating gold-standard FSC ..."<< std::endl;
				// The first slave receives the average from the second slave and calculates the FSC between them
				MPI_Status status;
				MultidimArray<Complex > avg2;
				avg2.resize(avg1);
				node->relion_MPI_Recv(MULTIDIM_ARRAY(avg2), 2*MULTIDIM_SIZE(avg2), MY_MPI_DOUBLE, 2, MPITAG_IMAGE, MPI_COMM_WORLD, status);
				wsum_model.BPref[iclass].calculateDownSampledFourierShellCorrelation(avg1, avg2, mymodel.fsc_halves_class[iclass]);
			}

		}

		// Now slave 1 sends the fsc curve to everyone else
		node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.fsc_halves_class[iclass]), MULTIDIM_SIZE(mymodel.fsc_halves_class[iclass]), MY_MPI_DOUBLE, 1, MPI_COMM_WORLD);
	}

#ifdef DEBUG
	std::cerr << "MlOptimiserMpi::compareTwoHalves: done" << std::endl;
#endif
}

// testing section for the solventFlatten function using ITK library:

void MlOptimiserMpi::solventFlatten()
{

#ifdef DEBUG
	std::cerr << "Entering MlOptimiser::solventFlatten" << std::endl;
#endif

	// If not use ITK library in this function, then perform normal solvent flatten function process
	if(!use_itk_flatten && !has_converged)
	{
		// First read solvent mask from disc, or pre-calculate it
		Image<DOUBLE> Isolvent, Isolvent2;
	    Isolvent().resize(mymodel.Iref[0]);
		Isolvent().setXmippOrigin();
		Isolvent().initZeros();
		if (fn_mask.contains("None"))
		{
			DOUBLE radius = particle_diameter / (2. * mymodel.pixel_size);
			DOUBLE radius_p = radius + width_mask_edge;
			FOR_ALL_ELEMENTS_IN_ARRAY3D(Isolvent())
			{
				DOUBLE r = sqrt((DOUBLE)(k*k + i*i + j*j));
				if (r < radius)
					A3D_ELEM(Isolvent(), k, i, j) = 1.;
				else if (r > radius_p)
					A3D_ELEM(Isolvent(), k, i, j) = 0.;
				else
				{
					A3D_ELEM(Isolvent(), k, i, j) = 0.5 - 0.5 * cos(PI * (radius_p - r) / width_mask_edge );
				}
			}
		}
		else
		{
			Isolvent.read(fn_mask);
			Isolvent().setXmippOrigin();

			if (Isolvent().computeMin() < 0. || Isolvent().computeMax() > 1.)
				REPORT_ERROR("MlOptimiser::solventFlatten: ERROR solvent mask should contain values between 0 and 1 only...");
		}

		// Also read a second solvent mask if necessary
		if (!fn_mask2.contains("None"))
		{
			Isolvent2.read(fn_mask2);
			Isolvent2().setXmippOrigin();
			if (!Isolvent2().sameShape(Isolvent()))
				REPORT_ERROR("MlOptimiser::solventFlatten ERROR: second solvent mask is of incorrect size.");
		}

		for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
		{

			// Then apply the expanded solvent mask to the map
			mymodel.Iref[iclass] *= Isolvent();

			// Apply a second solvent mask if necessary
			// This may for example be useful to set the interior of icosahedral viruses to a constant density value that is higher than the solvent
			// Invert the solvent mask, so that an input mask can be given where 1 is the masked area and 0 is protein....
			if (!fn_mask2.contains("None"))
				softMaskOutsideMap(mymodel.Iref[iclass], Isolvent2(), true);

// #define FLTN_SOLVENT
#ifdef FLTN_SOLVENT
			std::cerr << "==============Oidinary solvent flatten function.=========" << std::endl;
			std::cerr << "iclass: " << iclass << ", yinit: " << mymodel.Iref[iclass].yinit
				<< ", xinit: " << mymodel.Iref[iclass].xinit << ", zinit: " << mymodel.Iref[iclass].zinit << std::endl;
#endif

		} // end for iclass
	} // end if not use itk in solvent flatten
	else if(use_itk_flatten ) // && (node->rank == 2 ) ) // || node->rank == 2) )
	{
// #define ITK_FLATTEN_DEBUG
#ifdef ITK_FLATTEN_DEBUG
		std::cerr << "Enter into using ITK flatten solvnet flatten part, iteration: " << iter << std::endl;
#endif
		/*
		    This section using the ITK library to perform solvent flatten for reconstructed model
		  NEEDS further REVISION!!!
		    Because in the not last iteration reconstruction process, models are independently reconstructed in either two halves, so 
		  the function only needs to flatten TWO models from each random subset NODE (rank 1 and rank 2), then send flattened models to other
		  nodes.
		    This will use the relion_MPI_Send and relion_MPI_Recv functions
		    In the last iteration, the final reconstructed model from combined particles will be finally flattened at the master node (??????)
		      ---> At the last iteration, we cannot perform solventFlatten to the reconstructed model!!!

		  So, there will be a big change in the following ITK flatten section and the judging condition!!!

		*/

		/*
		  and after the otsu thresholding to gradient magnitude map, the function should perform properly dilation
		  to the output binary map
		*/

		// Temporary, the solventflatten function using ITK only used in autorefine process
		// so the number of classes to be refined only to be 1

		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			int reconstruct_rank1 = (iclass % (node->size - 1)) + 1;
			// at first, the variable of reconstruct_rank2 will be equal to reconstruct_rank1,
			// if do auto refine, that is do split random halves, do change the reconstruct_rank2
			int reconstruct_rank2 = reconstruct_rank1;

			if( do_split_random_halves )
			{reconstruct_rank2 = (iclass % (node->size - 1)) + 2;}

			// for 3D classification using MPI approach, we need to do some modifications for writting part:
			int nr_subsets = (do_split_random_halves)? 2:1; 

			// only in node->rank ==1(3d classification), or node-> rank ==2(auto-refine condition) to write out unmasked map:
			if( node->rank==1 || ( do_split_random_halves && node->rank==2 ) )
			{
				std::cerr << "ITK temporary write out mymodel.Iref[" << iclass << "], node: " << node->rank << std::endl;
				FileName fn_itk_iref_root, fn_iref_tmp;
				fn_itk_iref_root.compose(fn_out+"_it", iter, "", 3);

				Image< DOUBLE > img_itk_tmp_model_Iref;

				img_itk_tmp_model_Iref.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
				img_itk_tmp_model_Iref.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
				img_itk_tmp_model_Iref.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

				if(do_split_random_halves)
				{
					fn_iref_tmp.compose( fn_itk_iref_root+"_half_"+integerToString(node->rank)+"_model_unmasked_map_class", iclass+1, "mrc", 3 );
				}
				else
				{
					fn_iref_tmp.compose( fn_itk_iref_root+"_model_tmp_map_class", iclass+1, "mrc", 3 );
				}
				// re-center the mymoel.Iref[iclass] map:
				mymodel.Iref[iclass].setXmippOrigin();
				// mymodel.Iref[iclass].zinit = mymodel.Iref[iclass].yinit = mymodel.Iref[iclass].xinit = 0;
#define DEBUG_ITK_TMP
#ifdef DEBUG_ITK_TMP
				std::cerr << "zinit: " << mymodel.Iref[iclass].zinit << ", yinit: " << mymodel.Iref[iclass].yinit
					<< ", xinit: " << mymodel.Iref[iclass].xinit << std::endl;
#endif

				img_itk_tmp_model_Iref() = mymodel.Iref[iclass];
				img_itk_tmp_model_Iref.write(fn_iref_tmp);

			}

			if(node->rank == reconstruct_rank1 || ( do_split_random_halves && node->rank == reconstruct_rank2 ))
			{
				std::cerr << "iteration: " << iter << std::endl;
				std::cerr << "ITK in solventFlatten function, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
				

				// If use the ITK library in this solventflatten function, then we need to create and find a mask
				// for multiplicating it to the reconstructed model

				mymodel.Iref[iclass].zinit = mymodel.Iref[iclass].yinit = mymodel.Iref[iclass].xinit = 0;

				Image<DOUBLE> ITK_Isolvent; // this variable will create the final binary mask for reconstructed model
				ITK_Isolvent().resize(mymodel.Iref[iclass]);
				// ITK_Isolvent().setXmippOrigin();
				ITK_Isolvent().initZeros();

				// Next, use the ITK library's functions and methods:
				//==========

				//===== First, create some normal types for input density map assign to ITK image type,
				// and then assign the value of input map to input_image type
// #define DEBUG_ITK
#ifdef DEBUG_ITK
		std::cerr << "In the ITK assignment section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif
				typedef 	DOUBLE InputPixelType;
				typedef 	DOUBLE OutputPixeltype;

				const unsigned int Dimension = 3;  // the density map is 3 dimensional
				typedef 	itk::Image< InputPixelType, Dimension > 	InputImageType;
				typedef 	itk::Image< OutputPixeltype, Dimension > 	OutputImageType;
				typedef		itk::Image< InputPixelType, 1 >				NewImageType;

				InputImageType::Pointer input_image = InputImageType::New();
				const InputImageType::SizeType input_size = {{ XSIZE(mymodel.Iref[iclass]), YSIZE(mymodel.Iref[iclass]), ZSIZE(mymodel.Iref[iclass]) }};
				const InputImageType::IndexType input_start = {{ 0, 0, 0 }};

				// region, spacing, and other information of the input image:
				InputImageType::RegionType 	input_region;
				input_region.SetSize( input_size );
				input_region.SetIndex( input_start );

				input_image->SetRegions( input_region );
				input_image->Allocate( true );
				InputImageType::SpacingType 	input_spacing;
				input_spacing[0] = input_spacing[1] = input_spacing[2] = mymodel.pixel_size;

				input_image->SetSpacing( input_spacing );

				// assign the input density map to the input image, using iterator
				typedef 	itk::ImageRegionIteratorWithIndex< InputImageType > 	InputIteratorType;
				typedef		itk::ImageRegionIteratorWithIndex< OutputImageType >	OutputIteratorType;
				InputIteratorType input_image_It( input_image, input_region );

				// mymodel.Iref[0].zinit = mymodel.Iref[0].yinit = mymodel.Iref[0].xinit = 0;

#ifdef DEBUG_ITK
// to print out the starting index of mymodel.Iref[0]
std::cerr << "node: " << node->rank << " mymodel.Iref[" << iclass << "] starting x, y, z: " 
     << STARTINGX(mymodel.Iref[iclass]) << " " << STARTINGY(mymodel.Iref[iclass]) << " " << STARTINGZ(mymodel.Iref[iclass]) <<std::endl;

#endif
				for( input_image_It.GoToBegin(); !input_image_It.IsAtEnd(); ++input_image_It )
				{
					InputImageType::IndexType input_idx = input_image_It.GetIndex();
					int input_idx_x = (int)(input_idx[0]); // x index
					int input_idx_y = (int)(input_idx[1]); // y index
					int input_idx_z = (int)(input_idx[2]); // z index

					input_image_It.Set( A3D_ELEM(mymodel.Iref[iclass], input_idx_z, input_idx_y, input_idx_x) );
				}
				//==== end assign the value of input map to input image

				//==== Second, perform smoothing to the input image:
				// Second, do Gaussian smoothing to the input_image
				DOUBLE gaussian_sigma = 3.8/mymodel.pixel_size;
				DOUBLE gaussian_kernel_width = 3.5 * gaussian_sigma;

				OutputImageType::Pointer smooth_output_image = OutputImageType::New();

				GaussianFilterMapProcess( input_image, smooth_output_image, gaussian_sigma, gaussian_kernel_width );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK smoothing section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

				// void GaussianFilterMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap_new,
	    		// DOUBLE GaussianKernelSigma, DOUBLE GaussianMaxKernelWidth )

	    		// Third, compute voxels inide the user defined sphere, for gaussian smoothing result map

				NewImageType::Pointer new_1d_map_gauss_in_sphere_for_threshold = NewImageType::New();
/*				DOUBLE sphere_radius = particle_diameter/2;

				// convert the angstrom sphere_radius to it unit in pixels:
				sphere_radius /= mymodel.pixel_size;

*/
				// change the definition of sphere_radius value, 
				// to be a specific percentage of half of side length of box
				DOUBLE half_box_side_length = XSIZE(mymodel.Iref[iclass])/2;

				// the variable to be multiplied to half of side length of the box size
				// for the generation of histograms and calculation of thresholding value
				// for the better calculation!!!
				DOUBLE fraction_factor = 0.9; // this value we set it to 90%
				DOUBLE sphere_radius = fraction_factor * half_box_side_length;
#define TEMPORARY_ITK_DEBUGGING
#ifdef TEMPORARY_ITK_DEBUGGING
				std::cerr << "node: " << node->rank << ", xsize of mymodel: " << XSIZE(mymodel.Iref[iclass]) 
					<< ", sphere_radius: " << sphere_radius << std::endl;
#endif

				// input map for this function is result of gaussian smoothing
				DensityMapinDefinedSphereRadiusProcess( smooth_output_image, new_1d_map_gauss_in_sphere_for_threshold,
									sphere_radius );

				// void DensityMapinDefinedSphereRadiusProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 1 > *outputimage,
	    		// DOUBLE user_defined_sphere_radius )

	    		// Fourth, compute the 1D image for 99.74% information focused region( ONLY under the Gaussian smoothing
	    		// condition)

				DOUBLE info_left_height_sum, info_right_height_sum;
				DOUBLE info_left_bound_val, info_right_bound_val;

				NewImageType::Pointer new_1d_map_gauss_in_sphere_and_info_focused = NewImageType::New();

				// for get information focused region based on histogram, the threshold value will be set according to 
				// command line parameter, default value is 0.9974
				InformationFocusedRegionDensityProcess( new_1d_map_gauss_in_sphere_for_threshold, new_1d_map_gauss_in_sphere_and_info_focused,
						information_focused_zone_threshold_value, info_left_height_sum, info_right_height_sum,
						info_left_bound_val, info_right_bound_val, false );

	    		// void InformationFocusedRegionDensityProcess( itk::Image<DOUBLE,1> *input_image, itk::Image<DOUBLE,1> *output_image,
				// DOUBLE man_centre_fraction_threshold, DOUBLE & left_height_sum, DOUBLE & right_height_sum, 
				// DOUBLE & computed_left_bound, DOUBLE & computed_right_bound, bool is_gradient_mag = false )


	    		// Fifth, compute the Otsu's and Kittler-Illingworth's method threshold value
	    		// for Gaussian smoothing result

	    			// A. compute the otsu thresholding value for gaussian smoothing result map
				DOUBLE otsu_thre_val_gauss_map = 0.;

				// use the function to compute otsu's threshold value:
				// firstly set the number of histogram bins of otsu filter to be 8000
				// the input image for this filter should be information focused and voxels' value 
				// confined in user defined sphere
				OtsuFilterThresholdProcess( new_1d_map_gauss_in_sphere_and_info_focused, 8000, otsu_thre_val_gauss_map,
											0., 1. );
#define DEBUG_ITK_TMP1
#ifdef DEBUG_ITK_TMP1
				std::cerr << "otsu_thre_val_gauss_map value: " << otsu_thre_val_gauss_map << std::endl;
#endif

				// After we obtain the otsu threshold value of Gaussian smoothed dentisy map,
				// we should multiply it with the fraction value, got from command line
				// whose default value is 0.35:
				otsu_thre_val_gauss_map = otsu_thre_val_gauss_map * fraction_otsu_thres_val_in_digi;

#ifdef DEBUG_ITK_TMP1
				std::cerr << "otsu_thre_val_gauss_map value after shrinking: " << otsu_thre_val_gauss_map << std::endl;
#endif

				// void OtsuFilterThresholdProcess( itk::Image< DOUBLE, 1 > *inputimage,
				// unsigned long int numberofHistogramBins, DOUBLE insideValue = 0., DOUBLE outsideValue = 1.,
				// DOUBLE & thres_val=0. )

					// B. compute the Kittler-Illingworth thresholding vlaue for gaussian smoothing result map
				DOUBLE kittler_thre_val_gauss_map = 0.;

				// use the function to compute Kittler-Illingworth threshold value
				// and firstly, set the number of histogram bins of kittler filter to be 8000

				// whether to perform this process will depend on user
				if(compute_kittler_threshold_gaussian)
				{
					KittlerIllingWorthFilterThresholdProcess( new_1d_map_gauss_in_sphere_and_info_focused,
											 8000, kittler_thre_val_gauss_map );
#define ITK_SLVNT_KITTLER_THRESHOLD
#ifdef ITK_SLVNT_KITTLER_THRESHOLD
				std::cerr << "After kittler-Illingworth thresholding method function, " << std::endl
					<< "and kittler threshold value of gaussian smoothed map: " << kittler_thre_val_gauss_map
					<< std::endl;
#endif

				}

				// void KittlerIllingWorthFilterThresholdProcess( itk::Image< DOUBLE, 1 > *inputimage,
				// unsigned long int numberofHistogramBins, DOUBLE & thres_val = 0. )

	    		// Sixth, this will be the binarization processing section for Gaussian smoothing result map
				// for both otsu and kittler-Illingworth computed threshold value

	    		OutputImageType::Pointer gaussian_otsu_threshold_binary_map = OutputImageType::New();

	    		// if do compute kittler threshold
	    		// if(compute_kittler_threshold_gaussian)
	    		// OutputImageType::Pointer gaussian_kittler_threshold_binary_map = OutputImageType::New();

	    		// the input for binarization function should be result of gaussian smoothing filter
	    		// binarization for Otsu thresholding result:
	    		ThresholdBinarizeDensityMapProcess( smooth_output_image, gaussian_otsu_threshold_binary_map,
	    												otsu_thre_val_gauss_map );
				
				// void ThresholdBinarizeDensityMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap
				//     DOUBLE binarize_threshold_value )
	    		
	    		// binarization for Kittler-Illingworth thresholding result:
	    		// if do compute kittler threshold getted from command line:
    			// binarization for Kittler-Illingworth thresholding result:
	    		OutputImageType::Pointer gaussian_kittler_threshold_binary_map = OutputImageType::New();
	    		if(compute_kittler_threshold_gaussian)
		    	{
		    		// OutputImageType::Pointer gaussian_kittler_threshold_binary_map = OutputImageType::New();
		    		ThresholdBinarizeDensityMapProcess( smooth_output_image, gaussian_kittler_threshold_binary_map,
		    											kittler_thre_val_gauss_map );
		    	}

	    		// ==============
	    			// After the binarization of the result map of gaussian smoothing,
	    			// we need to do some properly binary dilation to those binary map

	    			// This is in the binary map dilating section, firstly, set the radius of structuring element
	    			// this is an initial variable, should be further modified in the future
	    		int gaussian_smoothed_thresholded_binary_dilate_element_radius = 1;

	    			// set the input and output binary map to be the same -----> should not be the same one!

	    		OutputImageType::Pointer gaussian_otsu_threshold_dilated_binary_map = OutputImageType::New();

	    		// if get from the command line, to do kittler thresholding process:
	    		// if(compute_kittler_threshold_gaussian)
	    			OutputImageType::Pointer gaussian_kittler_threshold_dilated_binary_map = OutputImageType::New();

	    		BinaryDilationDensityProcess( gaussian_otsu_threshold_binary_map, gaussian_otsu_threshold_dilated_binary_map,
	    										structuring_elelment_radius ); // using from command line parameter
	    										// gaussian_smoothed_thresholded_binary_dilate_element_radius );

	    		// if get from the command line to do kittler thresholding process:
	    		if(compute_kittler_threshold_gaussian)
		    		{BinaryDilationDensityProcess( gaussian_kittler_threshold_binary_map, gaussian_kittler_threshold_dilated_binary_map,
		    										structuring_elelment_radius );} // using from command line parameter
		    										// gaussian_smoothed_thresholded_binary_dilate_element_radius );}

	    		// void BinaryDilationDensityProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
	 			// int binary_dilating_radius = 1)


	    		// ==============
	    		// Seventh, compute the gradient magnitude density map, the input map is
	    		// result of Gaussian smoothing

	    		OutputImageType::Pointer grad_mag_output_image = OutputImageType::New();
	    		GradientMagnitudeFilterMapProcess( smooth_output_image, grad_mag_output_image );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK edge detection section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// void GradientMagnitudeFilterMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap )

	    		// Eighth, computing voxels confined inside user defined sphere, for gradient magnitude density map
	    		// after we obtain the 1d image with pixels inside the confined sphere, respective to original 3D
	    		// density map, we should then do thresholding computation process

	    		NewImageType::Pointer new_1d_map_grad_mag_in_sphere_for_threshold = NewImageType::New();
	    		DensityMapinDefinedSphereRadiusProcess( grad_mag_output_image, new_1d_map_grad_mag_in_sphere_for_threshold,
	    													sphere_radius );

	    		// Ninth, compute the first Otsu's method thresholding value for gradient magnitude map,
	    		// with voxels confined in the user defined sphere
	    		DOUBLE otsu_first_thres_val = 0.;
	    		OtsuFilterThresholdProcess( new_1d_map_grad_mag_in_sphere_for_threshold, 8000, otsu_first_thres_val,
	    									0.,	1. );

	    		// Tenth, compute the second Otsu's method thresholding value( local mask???) for gradient magnitude map,
	    		// with voxels confined in user defined sphere, and also located in the VALUE ZONE: [0, first otsu threshold value]

	    		// ONLY get the specific parameters from the command line, this process will perform:

	    		DOUBLE otsu_second_thres_val = 0.;

	    		if(compute_second_otsu_threshold_grad_mag_map)
	    		{	// we need to get a 1D image with pixel values confined in specific value zone:
	    			NewImageType::Pointer new_1d_map_grad_mag_in_sphere_range_for_threshold = 
		    			NewImageType::New();
		    		VoxelValuesinDefinedValueRangeImageProcess( new_1d_map_grad_mag_in_sphere_for_threshold,
	    				new_1d_map_grad_mag_in_sphere_range_for_threshold, 0., otsu_first_thres_val );

	    			DOUBLE otsu_second_thres_val = 0.;
	    			// do the otsu thresholding process for the second time( local mask?)
		    		OtsuFilterThresholdProcess( new_1d_map_grad_mag_in_sphere_range_for_threshold, 8000, otsu_second_thres_val,
		    									 0., 1. );
	    		}

#ifdef DEBUG_ITK
		std::cerr << "In the ITK Otsu thresholding section in gradient magnitude processing part, node: " 
					<< node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// Eleventh, do binarization to gradient magnitude map, according to computed threshold value
	    		// ===========
	    		// Notice!!!! This section includes many process steps, do not make any mistake!!!

	    			// first otsu thresholding binary map:
	    		OutputImageType::Pointer grad_mag_first_otsu_binary_map = OutputImageType::New();
	    		ThresholdBinarizeDensityMapProcess( grad_mag_output_image, grad_mag_first_otsu_binary_map,
	    					otsu_first_thres_val );

	    			// second otsu thresholding binary map: (the local mask)
	    			// only perform this process when got specific parameters from command line:
	    		OutputImageType::Pointer grad_mag_second_otsu_binary_map = OutputImageType::New();
	    		if(compute_second_otsu_threshold_grad_mag_map)
	    		{
	    			
		    		BinarizeDensityMapinRangeProcess( grad_mag_output_image, grad_mag_second_otsu_binary_map,
	    						otsu_first_thres_val, otsu_second_thres_val );
	    		}

	    			// After finishing binarization of first and second local otsu for gradient magnitude map, respectively
	    			// we need to perform addition and thresholding of these two binary map, to generate a binary map
	    			// then, we should perform properly dilation to the resulting binary map of the previous step
	    		OutputImageType::Pointer grad_mag_final_otsu_binary_map = OutputImageType::New();

	    		if(compute_second_otsu_threshold_grad_mag_map)
		    		{BinaryMapsAdditionandThresholdingProcess( grad_mag_first_otsu_binary_map, grad_mag_second_otsu_binary_map,
		    										grad_mag_final_otsu_binary_map );}
		    	else
		    		{grad_mag_final_otsu_binary_map = grad_mag_first_otsu_binary_map;}
	    			// after the binary addition, we will perform properly dilation of added binary map

	    			// firstly, set the binary dilating ball element radius, (temporary to set 1 )
	    		int grad_mag_output_otsu_binary_dilate_radius = 1;

	    		// the output of binary dilation process for the previous step
	    		OutputImageType::Pointer grad_mag_final_otsu_dilated_binary_map = OutputImageType::New();

	    		BinaryDilationDensityProcess( grad_mag_final_otsu_binary_map, grad_mag_final_otsu_dilated_binary_map,
	    										structuring_elelment_radius ); // using from the command line
	    										// grad_mag_output_otsu_binary_dilate_radius );

	    		// Then, because of the thresholded gradient magnitude density map, there might be some holes inside
	    		// the binary map, so we need to perform watersheds segmentation method or
	    		// connected components filter segmentation method to fill out holes

    			// before if condition, first define the final otsu thresholded binary map, which will be used for
    			// output of final binary mask map
	    		OutputImageType::Pointer final_otsu_method_binary_map = OutputImageType::New();

	    		if( do_use_itk_watershed_method )
	    		{
		    		OutputImageType::Pointer watershed_grad_mag_map = OutputImageType::New();
		    		// output of the watershed segment function should be a binary map
	    			WatershedSegmentDensityProcess( grad_mag_final_otsu_dilated_binary_map, watershed_grad_mag_map,
	    				watershed_threshold, watershed_level );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK watersheds segmenting section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

			    		// void WatershedSegmentDensityProcess( itk::Image< DOUBLE, 3 > * inputmap, itk::Image< DOUBLE, 3 > * outputmap,
			    		// DOUBLE watershed_threshold = 0., DOUBLE watershed_level = 1. )

	    				// Then, dilate the output of watershed filtering function, set
	    				// the structuring element radius just be 1
		    		OutputImageType::Pointer watershed_grad_mag_dilated_map = OutputImageType::New();
		    		BinaryDilationDensityProcess( watershed_grad_mag_map, watershed_grad_mag_dilated_map,
		    										structuring_elelment_radius ); // using from command line
	    			 // 1 );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK binary dilating section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

		    			// after the dilation of watershed output map, we should add this result with
		    			// the dilated map of addition of two times of otsu thresholding result

		    		// define the final result of otsu thresholding method image pointer
		    		// OutputImageType::Pointer final_otsu_method_binary_map = OutputImageType::New();
	    			BinaryMapsAdditionandThresholdingProcess( grad_mag_final_otsu_dilated_binary_map,watershed_grad_mag_dilated_map,
	    								final_otsu_method_binary_map );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK adding section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif
				}
	    		else if( do_use_itk_connected_components_method )
	    		{
	    			// first, invert binary voxel values of dilated gradient magnitude binary map
	    			OutputImageType::Pointer binay_inverted_grad_mag_map = OutputImageType::New();

	    			InvertVoxelValuesBinaryImage( grad_mag_final_otsu_dilated_binary_map, binay_inverted_grad_mag_map );
	    			// extern void InvertVoxelValuesBinaryImage( itk::Image< DOUBLE, 3 > *inputimage, itk::Image< DOUBLE, 3 > *outputimage )

	    			// second, perform itkConnectedComponentImageFilter
	    			OutputImageType::Pointer connected_component_output_binay_inverted_grad_mag_map =
	    									OutputImageType::New();

	    			ConnectedComponentsBinaryImageProcess( binay_inverted_grad_mag_map, connected_component_output_binay_inverted_grad_mag_map );
	    			// extern void ConnectedComponentsBinaryImageProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
					// bool input_inverted_binary )

	    			// third, perform binary dilation of output of connected image filter process function
	    			OutputImageType::Pointer connected_component_grad_mag_dilated_map = OutputImageType::New();

#ifdef DEBUG_ITK
		std::cerr << "In the ITK binary dilating section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    			BinaryDilationDensityProcess( connected_component_output_binay_inverted_grad_mag_map,
	    					connected_component_grad_mag_dilated_map, structuring_elelment_radius );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK adding section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    			// fourth, after binary dilation of result of connected components, we should add it
	    			// with final otsu dilated output map
	    			BinaryMapsAdditionandThresholdingProcess( grad_mag_final_otsu_dilated_binary_map,
	    					connected_component_grad_mag_dilated_map, final_otsu_method_binary_map );

	    		}


	    		// =====
	    		// Twelfth, addition the gradient magnitude final binary map with
	    		// gaussian final binary map, generate the final mask binary map, WITHOUT SOFT EDGE!!!

	    		OutputImageType::Pointer final_ITK_mask_binary_map_tmp = OutputImageType::New();

	    		// there will add a judgement section, depends on if get do kittler thresholding process from the command line
	    		if(compute_kittler_threshold_gaussian)
	    		{
	    			if( otsu_thre_val_gauss_map < kittler_thre_val_gauss_map )
	    				// BinaryMapsAdditionandThresholdingProcess( gaussian_otsu_threshold_binary_map, final_otsu_method_binary_map,
		    			BinaryMapsAdditionandThresholdingProcess( gaussian_otsu_threshold_dilated_binary_map, final_otsu_method_binary_map,
		    								 final_ITK_mask_binary_map_tmp );
		    		else if( kittler_thre_val_gauss_map <= otsu_thre_val_gauss_map )
	    				// BinaryMapsAdditionandThresholdingProcess( gaussian_kittler_threshold_binary_map, final_otsu_method_binary_map,
	    				BinaryMapsAdditionandThresholdingProcess( gaussian_kittler_threshold_dilated_binary_map, final_otsu_method_binary_map,
	    									 final_ITK_mask_binary_map_tmp );
	    		}
	    		else
	    		{ // if not do kittler from the command line
	    				// BinaryMapsAdditionandThresholdingProcess( gaussian_otsu_threshold_binary_map, final_otsu_method_binary_map,
		    			BinaryMapsAdditionandThresholdingProcess( gaussian_otsu_threshold_dilated_binary_map, final_otsu_method_binary_map,
		    								 final_ITK_mask_binary_map_tmp );

	    		}

#define DEBUG_ITK_MSK_CONNECTED
#ifdef DEBUG_ITK_MSK_CONNECTED
	    		std::cerr << "in debugging connected componects part, node: " << node->rank << std::endl;
	    		Image<DOUBLE> ITK_msk_write;
	    		ITK_msk_write().resize(mymodel.Iref[iclass]);
	    		ITK_msk_write().initZeros();
	    		// ITK_msk_write().setXmippOrigin();

	    		OutputIteratorType final_ITK_mask_binary_map_tmp_It(final_ITK_mask_binary_map_tmp,
	    			final_ITK_mask_binary_map_tmp->GetRequestedRegion());

	    std::cerr << "before assigning value, node: " << node->rank << std::endl;

	    		for(final_ITK_mask_binary_map_tmp_It.GoToBegin(); !final_ITK_mask_binary_map_tmp_It.IsAtEnd(); ++final_ITK_mask_binary_map_tmp_It)
	    		{
	    			OutputImageType::IndexType idx = final_ITK_mask_binary_map_tmp_It.GetIndex();
	    		    			// x, y, z coordinate:
					int idx_x = (int)(idx[0]); // x index
					int idx_y = (int)(idx[1]); // y index
					int idx_z = (int)(idx[2]); // z index
					A3D_ELEM(ITK_msk_write(), idx_z, idx_y, idx_x) = final_ITK_mask_binary_map_tmp->GetPixel(idx);

	    		}

	    std::cerr << "after assigning value, node: " << node->rank << std::endl;

	    		FileName fn_write_tmp;
	    		fn_write_tmp.compose( fn_out+"_it", iter, "", 3 );

	    		ITK_msk_write.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
				ITK_msk_write.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
				ITK_msk_write.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

		// fn_tmp_msk.compose( fn_root+"_half_"+integerToString(node->rank)+"_flatten_soft_mask_map_class", iclass+1, "mrc",3 );
		fn_write_tmp.compose(fn_write_tmp+"_half"+integerToString(node->rank)+"_connted_no_soft_msk_debug_class", iclass+1,"mrc",3);
				ITK_msk_write().setXmippOrigin();
				ITK_msk_write.write(fn_write_tmp);

#endif

	    		// ===========
	    		// because there still exists some noise in the obtained final_ITK_mask_binary_map, there is a need to
	    		// get the connected components to generate the final binary mask
	    		// and this will use the connected components filtering function:
	    		OutputImageType::Pointer final_ITK_mask_binary_map = OutputImageType::New();

	    		ConnectedComponentsLargestBinaryMaskGeneration( final_ITK_mask_binary_map_tmp,
	    								final_ITK_mask_binary_map );

// the debugging part of get largest connected components
#ifdef DEBUG_ITK_MSK_CONNECTED
	    std::cerr << "in debugging connected components largest writing part, node: " << node->rank << std::endl;
	    Image<DOUBLE> itk_connected_msk;
	    itk_connected_msk().resize(mymodel.Iref[iclass]);
	    itk_connected_msk().initZeros();
	    // itk_connected_msk().setXmippOrigin();

	    OutputIteratorType final_ITK_mask_binary_map_it( final_ITK_mask_binary_map,
	    					final_ITK_mask_binary_map->GetRequestedRegion());

std::cerr << "before assigning value, node: " << node->rank << std::endl;

	    // assigning values:
	    for(final_ITK_mask_binary_map_it.GoToBegin();!final_ITK_mask_binary_map_it.IsAtEnd();++final_ITK_mask_binary_map_it)
	    {
	    	OutputImageType::IndexType idx = final_ITK_mask_binary_map_it.GetIndex();

   			// x, y, z coordinate:
			int idx_x = (int)(idx[0]); // x index
			int idx_y = (int)(idx[1]); // y index
			int idx_z = (int)(idx[2]); // z index
			A3D_ELEM(itk_connected_msk(), idx_z, idx_y, idx_x) = final_ITK_mask_binary_map->GetPixel(idx);
	    }

std::cerr << "after assigning value, node: " << node->rank << std::endl;

	    FileName fn_write_tmp2;
		fn_write_tmp2.compose( fn_out+"_it", iter, "", 3 );

   		itk_connected_msk.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
		itk_connected_msk.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
		itk_connected_msk.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

		fn_write_tmp2.compose(fn_write_tmp2+"_half"+integerToString(node->rank)
			+"_connted_largest_no_soft_msk_debug_class", iclass+1,"mrc",3);
		itk_connected_msk().setXmippOrigin();
		itk_connected_msk.write(fn_write_tmp2);

#endif
	    		// =====
	    		// Thirteenth, after getting the final ITK mask map, we need to assign its voxels value to 
	    		// MultidimArray map, and make multiplication to mymodel.Iref[iclass]!!!

	    		OutputIteratorType final_ITK_mask_binary_mapIt( final_ITK_mask_binary_map,
	    							 final_ITK_mask_binary_map->GetRequestedRegion() );

#ifdef DEBUG_ITK
		std::cerr << "In the ITK assignment value to density map section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		for( final_ITK_mask_binary_mapIt.GoToBegin(); !final_ITK_mask_binary_mapIt.IsAtEnd(); ++final_ITK_mask_binary_mapIt )
	    		{
	    			OutputImageType::IndexType mask_map_idx = final_ITK_mask_binary_mapIt.GetIndex();
	    			// x, y, z coordinate:
					int idx_x = (int)(mask_map_idx[0]); // x index
					int idx_y = (int)(mask_map_idx[1]); // y index
					int idx_z = (int)(mask_map_idx[2]); // z index

					A3D_ELEM(ITK_Isolvent(), idx_z, idx_y, idx_x) = final_ITK_mask_binary_map->GetPixel( mask_map_idx );
	    		}

#ifdef DEBUG_ITK
		std::cerr << "End ITK assigning value to density map section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

				// for(addition_It.GoToBegin(); !addition_It.IsAtEnd(); ++addition_It)
				// {
				// 	// get the index of the iterator, for precisely locate final assignment of output map
				// 	InputImageType::IndexType addition_image_idx = addition_It.GetIndex();
				// 	// x, y, z coordinate
				// 	int idx_x = (int)(addition_image_idx[0]); // x index
				// 	int idx_y = (int)(addition_image_idx[1]); // y index
				// 	int idx_z = (int)(addition_image_idx[2]); // z index

				// 	A3D_ELEM(ITK_Isolvent(), idx_z, idx_y, idx_x) = addition_image->GetPixel(addition_image_idx);
				// }

	    		// Before final apply ITK_Isolvent() to multiply with mymodel.Iref[iclass], we have to make 
	    		// generate a soft mask map of ITK_Isolvent() map

	    		// ========================

#ifdef DEBUG_ITK
		std::cerr << "In the ITK soft edge making section, before multiplication, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif

	    		// define the image of the output result of soft mask function
	    		Image< DOUBLE > ITK_Isolvent_soft;
	    		ITK_Isolvent_soft().setXmippOrigin();

	    		// Because we need to make a soft mask, we need to know the soft edge of final soft mask
	    		// temporarily, we set it to 4 pixels, this needs further modified in the future
	    		// IMPORTANT!!!
	    		// and I think this should be parsed from the command line of the parser object!!!
	    		// DOUBLE soft_edge_width = 4.;

	    		// temporarily set the binary extend radius to 2.0 pixel, this parameter also
	    		// needs to be parsed from the command line in the future!!!! IMPORTANT!
	    		// DOUBLE itk_solvent_mask_extend = 2.;

	    		ITK_Isolvent().setXmippOrigin();

	    		// actually implement the automask function
	    		autoMask( ITK_Isolvent(), ITK_Isolvent_soft(),
	    			0.3, itk_solvent_msk_extend_pixel, itk_solvent_msk_soft_width, true ); // set the extend width to 0 because the 




#ifdef DEBUG_ITK
		std::cerr << "In the ITK multiplicating section, node: " << node->rank << " random subset: " << node->myRandomSubset() << std::endl;
#endif
				// do set xmipporigin to these two mask arrays and mymodel.Iref!!!
				mymodel.Iref[iclass].setXmippOrigin();

// #define MULTIDIM_DEBUG
#ifdef MULTIDIM_DEBUG
std::cerr << "===================== MultidimArray Debugging Part =====================" << std::endl;
std::cerr << "node: " << node-> rank << " random subset: " << node->myRandomSubset() << std::endl;
std::cerr << "Information of mymodel.Iref[" << iclass << "]:" << std::endl;
std::cerr << "STARTINGX of mymodel.Iref[" << iclass << "]: " << STARTINGX(mymodel.Iref[iclass]) << std::endl;
std::cerr << "STARTINGY of mymodel.Iref[" << iclass << "]: " << STARTINGY(mymodel.Iref[iclass]) << std::endl;
std::cerr << "STARTINGZ of mymodel.Iref[" << iclass << "]: " << STARTINGZ(mymodel.Iref[iclass]) << std::endl;
std::cerr << "XSIZE of mymodel.Iref[" << iclass << "]: " << XSIZE(mymodel.Iref[iclass]) << std::endl;
std::cerr << "YSIZE of mymodel.Iref[" << iclass << "]: " << YSIZE(mymodel.Iref[iclass]) << std::endl;
std::cerr << "ZSIZE of mymodel.Iref[" << iclass << "]: " << ZSIZE(mymodel.Iref[iclass]) << std::endl;
std::cerr << "----------" << std::endl;
std::cerr << "Information of ITK_Isolvent_soft() 3D array:" << std::endl;
std::cerr << "STARTINGX of ITK_Isolvent_soft(): " << STARTINGX(ITK_Isolvent_soft()) << std::endl;
std::cerr << "STARTINGY of ITK_Isolvent_soft(): " << STARTINGY(ITK_Isolvent_soft()) << std::endl;
std::cerr << "STARTINGZ of ITK_Isolvent_soft(): " << STARTINGZ(ITK_Isolvent_soft()) << std::endl;
std::cerr << "XSIZE of ITK_Isolvent_soft(): " << XSIZE(ITK_Isolvent_soft()) << std::endl;
std::cerr << "YSIZE of ITK_Isolvent_soft(): " << YSIZE(ITK_Isolvent_soft()) << std::endl;
std::cerr << "ZSIZE of ITK_Isolvent_soft(): " << ZSIZE(ITK_Isolvent_soft()) << std::endl;

std::cerr << "========================================================================" << std::endl;

#endif
				if(!has_converged)
	    			mymodel.Iref[iclass] *= ITK_Isolvent_soft();
	    		else
	    			mymodel.Iref[iclass] = mymodel.Iref[iclass]; 
	    			// if has converged, not multiply the mask to mymodel.Iref[iclass]

	    		// mymodel.Iref[iclass] *= ITK_Isolvent();
	    		// finishing applying mask to reconstructed model
	    		// finishing one iteration of the loop



// at this location, to write out the ITK_Isolvent_soft map to disk

				FileName fn_root, fn_tmp, fn_tmp_msk;
				fn_root.compose( fn_out+"_it", iter, "", 3 );

				Image< DOUBLE > img_itk_mask;
				img_itk_mask.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
				img_itk_mask.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
				img_itk_mask.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );
				
				if( do_split_random_halves )
				{
					fn_tmp_msk.compose( fn_root+"_half_"+integerToString(node->rank)+"_flatten_soft_mask_map_class", iclass+1, "mrc",3 );
				}
				else
				{
					fn_tmp_msk.compose( fn_root+"_flatten_soft_mask_map_class", iclass+1, "mrc", 3 );
				}

				// re-center:
				ITK_Isolvent_soft().setXmippOrigin();

				img_itk_mask() = ITK_Isolvent_soft();
				img_itk_mask.write(fn_tmp_msk);

#define DEBUG_ITK_SOLVENT_WRITE
#ifdef DEBUG_ITK_SOLVENT_WRITE
				std::cerr << "node: " << node->rank << ", fn_tmp_msk: " << fn_tmp_msk << std::endl;
#endif

/*				// set back to the Xmipporigin:
				mymodel.Iref[iclass].setXmippOrigin(); // because after maximization function, mymodel.Iref is already set to xmipporigin
				ITK_Isolvent().setXmippOrigin();
				ITK_Isolvent_soft().setXmippOrigin();
				// mymodel.Iref[0] = mymodel.Iref[0]* ITK_Isolvent();
				mymodel.Iref[iclass] = mymodel.Iref[iclass]* ITK_Isolvent_soft();
*/

				//==== end multiplication

				// Then, after the multiplication section, the program should also write out the mask map to disk
				// to as a reference; and should be two mask maps in total every iteration in the autorefine process

					// ========
					// IMPORTANT!!!
					// I think if we want to write out mask map, we need to wait the program finish
					// the final broadcasting process!!!

					// FileName fn_root, fn_tmp;

					// fn_root.compose(fn_out+"_it", iter, "", 3);

					// Image<DOUBLE> img;
					// // Set the correct voxel size in the mask map mrc file header:
					// img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size);
					// img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size);
					// img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size);

					// for(int iiclass = 0; iiclass < mymodel.nr_classes; iiclass++)
					// {
					// 	fn_tmp.compose(fn_root + "_half_flatten_mask" + integerToString(node->rank) + "_class", iiclass+1, "mrc", 3);
					// 	img() = mymodel.Iref[iiclass];
					// 	img.write(fn_tmp);
					// }


				/*
				End of using ITK library in the SolventFlatten function, this is a temporary testing code.
				The final output mask map is ITK_Isolvent MultidimArray, and the final multiplicated output
				model density map is mymodel.Iref[0].
				*/
				//==========

			} // end if node in which rank

		} // end the loop for, iclass



		// before do process mpisend and mpirecv, we have to wait for all nodes sychronized!
		MPI_Barrier(MPI_COMM_WORLD);

		// finally, after the end of multiplication, send flattened model to all other slave nodes
		// have to make sure salve nodes at the same subset!!!

// #define DEBUG_ITK_MPI
		
		// Strategy of the subsequent sending is referred from line 1282 -...., in maximization function
		// because there might be some classes of reference model, we need to use the loop iteration
	if(!has_converged)
	{
		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			// re-center:
			mymodel.Iref[iclass].setXmippOrigin();

			if(do_split_random_halves)
			{
				MPI_Status status;
				int nr_subsets = 2;

				for(int isubset = 1; isubset <= nr_subsets; isubset++)
				{
					if(node->myRandomSubset() == isubset)
					{
						int reconstruct_rank = (iclass % (node->size - 1)) + isubset; // the REAL reconstruct rank ,first is subset1, second will be subset2
						int my_first_recv = node->myRandomSubset();

						for(int recv_node = my_first_recv; recv_node < node->size; recv_node += nr_subsets)
						{
							// the sending node and receiving node rank judgement:
							if(node->rank == reconstruct_rank && recv_node != node->rank)
							{
#ifdef DEBUG_ITK_MPI
			std::cerr << "isubset= " << isubset << " iclass: " << iclass << ", Sending node: " << reconstruct_rank << ", receiving node: " << recv_node << std::endl;
#endif						
								// send flattened model from reconstruct rank to same subset slave nodes
								node->relion_MPI_Send(MULTIDIM_ARRAY(mymodel.Iref[iclass]), MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, recv_node, MPITAG_IMAGE, MPI_COMM_WORLD);
							}
							else if(node->rank != reconstruct_rank && recv_node == node->rank)
							{
								// the other slave nodes at the same subset reveive flattened model from reconstruct node
								node->relion_MPI_Recv(MULTIDIM_ARRAY(mymodel.Iref[iclass]), MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPITAG_IMAGE, MPI_COMM_WORLD, status);
#ifdef DEBUG_ITK_MPI
			std::cerr << "isubset= " << isubset << " iclass: " << iclass << ", receiving from: " << reconstruct_rank << " at node: " << node->rank << std::endl;
#endif					
							}
						}
					}
				}

				// wait until all nodes have done their work!
				// no one will continue until we are all at here
				MPI_Barrier(MPI_COMM_WORLD);
			}
			else
			{
				int reconstruct_rank = (iclass % (node->size - 1) ) + 1;
				// Broadcast the reconstructed references to all other MPI nodes
				node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.Iref[iclass]),
						MULTIDIM_SIZE(mymodel.Iref[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);
				// Broadcast the data_vs_prior spectra to all other MPI nodes
				// node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.data_vs_prior_class[iclass]),
						// MULTIDIM_SIZE(mymodel.data_vs_prior_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);
				// Broadcast the sigma2_class spectra to all other MPI nodes
				// node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.sigma2_class[iclass]),
						// MULTIDIM_SIZE(mymodel.sigma2_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);

#ifdef DEBUG_ITK_MPI
				std::cerr << "=== In broadcasting to other slave nodes section ===" << std::endl;
				std::cerr << "do_split_random_halves: " << do_split_random_halves << std::endl;
				std::cerr << "node: " << node->rank 
					 << ", reconstruct_rank: " << reconstruct_rank << std::endl;
#endif
				// waiting:
				// MPI_Barrier(MPI_COMM_WORLD);

#ifdef DEBUG_ITK_MPI
				std::cerr << "=== End broadcasting section ===" << std::endl;
#endif
			}


		} // end loop for iclass, MPI broadcasting reference map to other nodes

				// waiting:
				MPI_Barrier(MPI_COMM_WORLD);



		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			int reconstruct_rank1 = (iclass % (node->size - 1)) + 1;
			int reconstruct_rank2 = (iclass % (node->size - 1)) + 2;

			// if( node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2 )
			if( node->rank == 1 || ( do_split_random_halves && node->rank == 2 ) )
			{
#ifdef DEBUG_ITK_MPI
		std::cerr << "In the ITK MPI writting mask map section, node: " << node->rank << " random_subset: " 
					<< node-> myRandomSubset() << std::endl;
#endif

					// will user the for loop to write out half mask maps, which will under certain condition
					// node at specific rank, will write out the mask, which would be used in the solventFlatten function
					// the root and tmp file name:
					FileName fn_root, fn_tmp, fn_tmp_msk;
					fn_root.compose( fn_out+"_it", iter, "", 3 );

					Image< DOUBLE > img;
					// Image< DOUBLE > img_itk_mask;

					// set the voxel size of the mask map in the mrc header:
					img.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
					img.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
					img.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

					// img_itk_mask.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size );
					// img_itk_mask.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size );
					// img_itk_mask.MDMainHeader.setValue( EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size );

				// in the random subset, half set rank, should write out the mask map
				// and because this is inside the loop, so we will obtain the iclass number according to the iteration

				if(do_split_random_halves)
				{
					// under the condition of auto-refine:
					fn_tmp.compose( fn_root+"_half_"+integerToString(node->rank)+"_flatten_soft_masked_class",iclass+1, "mrc", 3 );
				}
				else
				{
					fn_tmp.compose( fn_root+"_flatten_soft_masked_class", iclass+1, "mrc", 3 );
				}
				
				// re-center:
				mymodel.Iref[iclass].setXmippOrigin();

				img() = mymodel.Iref[iclass];
				img.write(fn_tmp);

				// fn_tmp_msk.compose( fn_root+"_half_"+integerToString(node->rank)+"_flatten_soft_mask_map", iclass+1, "mrc",3 );
				// img_itk_mask() = ITK_Isolvent_soft();
				// img_itk_mask.write(fn_tmp_msk);

#ifdef DEBUG_ITK_MPI
		std::cerr << "End of ITK MPI writting mask map section, node: " << node-> rank << " random_subset: "
					<< node->myRandomSubset() << std::endl;
#endif
			}
		} // end for loop write out soft mask in specific node rank
	} // end if !has_converged
/*
	
		for( int iclass = 0; iclass < mymodel.nr_classes; iclass++ )
		{
			int reconstruct_rank1 = (iclass % (node->size - 1)) + 1;
			int reconstruct_rank2 = (iclass % (node->size - 1)) + 2;


			if(node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2)
			{


*/

	} // end else if use_itk_flatten
// #define DEBUG_TMP
#ifdef DEBUG_TMP
	std::cerr << "Leaving MlOptimiser::solventFlatten, node: " << node->rank 
				<< " random_subset: " << node->myRandomSubset() << std::endl;
#endif

}

//=====================

void MlOptimiserMpi::iterate()
{
#ifdef TIMING
	// MPI-specific timing stuff goes here...
	TIMING_MPIWAIT= timer.setNew("mpiWaitEndOfExpectation");
	TIMING_MPICOMBINEDISC= timer.setNew("mpiCombineThroughDisc");
	TIMING_MPICOMBINENETW= timer.setNew("mpiCombineThroughNetwork");
	TIMING_MPISLAVEWORK= timer.setNew("mpiSlaveWorking");
	TIMING_MPISLAVEWAIT1= timer.setNew("mpiSlaveWaiting1");
	TIMING_MPISLAVEWAIT2= timer.setNew("mpiSlaveWaiting2");
	TIMING_MPISLAVEWAIT3= timer.setNew("mpiSlaveWaiting3");
#endif


	// Launch threads etc.
	MlOptimiser::iterateSetup();

	// Initialize the current resolution
	updateCurrentResolution();

	for (iter = iter + 1; iter <= nr_iter; iter++)
    {
#ifdef TIMING
		timer.tic(TIMING_EXP);
#endif

		// Nobody can start the next iteration until everyone has finished
		MPI_Barrier(MPI_COMM_WORLD);

		if (do_auto_refine && node->rank == 1)
			printConvergenceStats();

		expectation();

#define ITER_EXP_TMP
#ifdef ITER_EXP_TMP
		std::cerr << "After expectation, before mpi barrier, node: " << node->rank << std::endl;
#endif

		MPI_Barrier(MPI_COMM_WORLD);

#ifdef ITER_EXP_TMP
		std::cerr << "After expectation, after mpi barrier, node: " << node->rank << std::endl;
#endif
		if (do_skip_maximization)
		{
			// Only write data.star file and break from the iteration loop
			if (node->isMaster())
			{
				// The master only writes the data file (he's the only one who has and manages these data!)
				iter = -1; // write output file without iteration number
				MlOptimiser::write(DONT_WRITE_SAMPLING, DO_WRITE_DATA, DONT_WRITE_OPTIMISER, DONT_WRITE_MODEL, node->rank);
				std::cout << " Auto-refine: Skipping maximization step, so stopping now... " << std::endl;
			}
			break;
		}

#ifdef ITER_EXP_TMP
		std::cerr << "iter: " << iter << ", before combine weights, node: " << node->rank 
			<< ", combine_weights_thru_disc: " << combine_weights_thru_disc << std::endl;
#endif

		// Now combine all weighted sums
		// Leave the option ot both for a while. Then, if there are no problems with the system via files keep that one and remove the MPI version from the code
		if (combine_weights_thru_disc)
			combineAllWeightedSumsViaFile();
		else
			combineAllWeightedSums();

#ifdef ITER_EXP_TMP
		std::cerr << "After combine weights, before mpi barrier, node: " << node->rank << std::endl;
#endif

		MPI_Barrier(MPI_COMM_WORLD);

#ifdef ITER_EXP_TMP
		std::cerr << "After combine weights, after mpi barrier, node: " << node->rank << std::endl;
#endif

		// Write out data and weight arrays to disc in order to also do an unregularized reconstruction
		// if (do_auto_refine && has_converged)
		// for the correction of FSC of ITK solvent mask, we have to change the condition
		#define DEBUG_WRITE_TEMPORARY
		if (do_auto_refine && ( ( use_itk_flatten && do_phase_random_fsc ) || has_converged ))
		{

			writeTemporaryDataAndWeightArrays();
		}

		// Inside iterative refinement: do FSC-calculation BEFORE the solvent flattening, otherwise over-estimation of resolution
		// anyway, now that this is done inside BPref, there would be no other way...
		if (do_split_random_halves)
		{

			// For asymmetric molecules, join 2 half-reconstructions at the lowest resolutions to prevent them from diverging orientations
			if (low_resol_join_halves > 0.)
				joinTwoHalvesAtLowResolution();

			// Calculate gold-standard FSC curve
			compareTwoHalves();

			// For automated sampling procedure
			if (!node->isMaster()) // the master does not have the correct mymodel.current_size, it only handles metadata!
			{
				// Check that incr_size is at least the number of shells as between FSC=0.5 and FSC=0.143
				int fsc05   = -1;
				int fsc0143 = -1;
				FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(mymodel.fsc_halves_class[0])
				{
					if (DIRECT_A1D_ELEM(mymodel.fsc_halves_class[0], i) < 0.5 && fsc05 < 0)
						fsc05 = i;
					if (DIRECT_A1D_ELEM(mymodel.fsc_halves_class[0], i) < 0.143 && fsc0143 < 0)
						fsc0143 = i;
				}
				// At least fsc05 - fsc0143 + 5 shells as incr_size
				incr_size = XMIPP_MAX(incr_size, fsc0143 - fsc05 + 5);
				has_high_fsc_at_limit = (DIRECT_A1D_ELEM(mymodel.fsc_halves_class[0], mymodel.current_size/2 - 1) > 0.2);
			}

			// Upon convergence join the two random halves
			if (do_join_random_halves || do_always_join_random_halves)
			{
				if (combine_weights_thru_disc)
					combineWeightedSumsTwoRandomHalvesViaFile();
				else
					combineWeightedSumsTwoRandomHalves();

			}
		}

#ifdef TIMING
		timer.toc(TIMING_EXP);
		timer.tic(TIMING_MAX);
#endif

		maximization();

// #define DEBUG_ITK_TMP2
#ifdef DEBUG_ITK_TMP2
// to print out the starting index of mymodel.Iref[0]
std::cerr << " after maximization function, node: " << node->rank << std::endl; // << " mymodel.Iref[0] starting x, y, z: " 
     // << STARTINGX(mymodel.Iref[0]) << " " << STARTINGY(mymodel.Iref[0]) << " " << STARTINGZ(mymodel.Iref[0]) <<std::endl;

#endif

		// Make sure all nodes have the same resolution, set the data_vs_prior array from half1 also for half2
		if (do_split_random_halves)
			for (int iclass = 0; iclass < mymodel.nr_classes; iclass++)
				node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.data_vs_prior_class[iclass]), MULTIDIM_SIZE(mymodel.data_vs_prior_class[iclass]), MY_MPI_DOUBLE, 1, MPI_COMM_WORLD);

#ifdef TIMING
		timer.toc(TIMING_MAX);
#endif

		MPI_Barrier(MPI_COMM_WORLD);

		// Mask the reconstructions to get rid of noisy solvent areas
		// Skip masking upon convergence (for validation purposes)
		if (do_solvent )// && !has_converged)
			{
				solventFlatten();
#ifdef DEBUG_ITK_TMP2
		std::cerr << "after solventFlatten function, node: " << node->rank << "random subset: " << node->myRandomSubset() << std::endl;
#endif
				MPI_Barrier(MPI_COMM_WORLD);
			}

#define PHASE_FSC

		if(do_phase_random_fsc && use_itk_flatten && do_auto_refine)
		{

#ifdef PHASE_FSC
			std::cerr << "before perform correct fsc, node: " << node->rank << std::endl;
#endif
			reconstructUnregularisedMapAndCalculateSolventCorrectedFSC();
#ifdef PHASE_FSC
			std::cerr << "after perform correct fsc, before mpi barrier, node: " << node->rank << std::endl;
#endif
			// wait:
			MPI_Barrier(MPI_COMM_WORLD);
#ifdef PHASE_FSC
			std::cerr << "after mpi barrier, node: " << node->rank << std::endl;
#endif
		}

		// Re-calculate the current resolution, do this before writing to get the correct values in the output files
		updateCurrentResolution();

		// If we are joining random halves, then do not write an optimiser file so that it cannot be restarted!
		bool do_write_optimiser = !do_join_random_halves;
		// Write out final map without iteration number in the filename
		if (do_join_random_halves)
			iter = -1;

// debugging part:
#define DEBUG_ITER_TMP1
#ifdef DEBUG_ITER_TMP1
	if(node->rank==1)
	{
		std::cerr << "node: " << node->rank << ", debugging part in iteration, before write" << std::endl;
		std::cerr << "to see the origin index for writing out. " << std::endl;
		for( int iclass=0; iclass < mymodel.nr_classes; iclass++)
		{
			std::cerr << "iclass: " << iclass << ", zinit: " << mymodel.Iref[iclass].zinit << ", yinit: " << mymodel.Iref[iclass].yinit
				<< ", xinit: " << mymodel.Iref[iclass].xinit << std::endl;
		}
	}
#endif

		if (node->rank == 1 || (do_split_random_halves && !do_join_random_halves && node->rank == 2) )
			//Only the first_slave of each subset writes model to disc (do not write the data.star file, only master will do this)
			MlOptimiser::write(DO_WRITE_SAMPLING, DONT_WRITE_DATA, do_write_optimiser, DO_WRITE_MODEL, node->rank);
		else if (node->isMaster())
			// The master only writes the data file (he's the only one who has and manages these data!)
			MlOptimiser::write(DONT_WRITE_SAMPLING, DO_WRITE_DATA, DONT_WRITE_OPTIMISER, DONT_WRITE_MODEL, node->rank);

		if (do_auto_refine && has_converged)
		{
			if (verb > 0)
			{
				std::cout << " Auto-refine: Refinement has converged, stopping now... " << std::endl;
				std::cout << " Auto-refine: + Final reconstruction from all particles is saved as: " <<  fn_out << "_class001.mrc" << std::endl;
				std::cout << " Auto-refine: + Final model parameters are stored in: " << fn_out << "_model.star" << std::endl;
				std::cout << " Auto-refine: + Final data parameters are stored in: " << fn_out << "_data.star" << std::endl;
				std::cout << " Auto-refine: + Final resolution (without masking) is: " << 1./mymodel.current_resolution << std::endl;
				if (acc_rot < 10.)
					std::cout << " Auto-refine: + But you may want to run relion_postprocess to mask the unfil.mrc maps and calculate a higher resolution FSC" << std::endl;
				else
				{
					std::cout << " Auto-refine: + WARNING: The angular accuracy is worse than 10 degrees, so basically you cannot align your particles!" << std::endl;
					std::cout << " Auto-refine: + WARNING: This has been observed to lead to spurious FSC curves, so be VERY wary of inflated resolution estimates..." << std::endl;
					std::cout << " Auto-refine: + WARNING: You most probably do NOT want to publish these results!" << std::endl;
					std::cout << " Auto-refine: + WARNING: Sometimes it is better to tune resolution yourself by adjusting T in a 3D-classification with a single class." << std::endl;
				}
				if (do_use_reconstruct_images)
					std::cout << " Auto-refine: + Used rlnReconstructImageName images for final reconstruction. Ignore filtered map, and only assess the unfiltered half-reconstructions!" << std::endl;
			}
			break;
		}

		// Check whether we have converged by now
		// Master does not have all info, only slaves do this and then broadcast has_converged so also master has it
		// If we have, set do_join_random_halves and do_use_all_data for the next iteration
		if (!node->isMaster() && do_auto_refine)
			checkConvergence();
		node->relion_MPI_Bcast(&has_converged, 1, MPI_INT, 1, MPI_COMM_WORLD);



#ifdef TIMING
		// Only first slave prints it timing information
		if (node->rank == 1)
			timer.printTimes(false);
#endif


    }

	// delete threads etc.
	MlOptimiser::iterateWrapUp();

}

// ====
// the added function
void MlOptimiserMpi::reconstructUnregularisedMapAndCalculateSolventCorrectedFSC()
{

// debugging part
#define DEBUG_CORRECT_FSC

	for( int iclass= 0; iclass < mymodel.nr_classes; iclass++ )
	{
		int reconstruct_rank1 = (iclass % ( (node->size - 1)/2 )) + 1;
		int reconstruct_rank2 = (iclass % ( (node->size - 1)/2 )) + 2;
		if( mymodel.ref_dim == 3 && (node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2) )
		{
#ifdef DEBUG_CORRECT_FSC
			std::cerr << "in the correcting fsc function, iclass: " << iclass <<std::endl;
			std::cerr << "node: " << node->rank << std::endl;
			std::cerr << "do_join_random_halves: " << do_join_random_halves << std::endl;
			std::cerr << "iter: " << iter << std::endl;
#endif

			// A. read temporary data and weight arrays from disc to perform unregularised reconstruction
			// notice the random subset problem!
			MultidimArray<DOUBLE> dummy;
			Image<DOUBLE> Iunreg, Itmp;

			int random_halfset = (node->rank %2 == 1)? 1 : 2;
			// FileName fn_root = fn_out + "_half" + integerToString(ihalf);;
			// the variable to read in temporary file from disc:
			FileName fn_root = fn_out + "_half" + integerToString(random_halfset);; 

			// the variable to write out unregularised reconstruction to disc (for correction of FSC)
			FileName fn_root_out;
			if(iter > -1)
				fn_root_out.compose(fn_out+"_it", iter, "", 3);
			else
				fn_root_out = fn_out;
			fn_root_out += "_half" + integerToString(random_halfset);
			fn_root_out.compose(fn_root_out+"_class", iclass+1, "", 3);

			fn_root.compose(fn_root+"_class", iclass+1, "", 3);

			// Read temporary arrays back in
			Itmp.read(fn_root+"_data_real.mrc");
			Itmp().setXmippOrigin();
			Itmp().xinit=0;
			if (!Itmp().sameShape(wsum_model.BPref[iclass].data))
			{
				wsum_model.BPref[iclass].data.printShape(std::cerr);
				Itmp().printShape(std::cerr);
				REPORT_ERROR("Incompatible size of "+fn_root+"_data_real.mrc");
			}
			FOR_ALL_ELEMENTS_IN_ARRAY3D(Itmp())
			{
				A3D_ELEM(wsum_model.BPref[iclass].data, k, i, j).real = A3D_ELEM(Itmp(), k, i, j);
			}

			Itmp.read(fn_root+"_data_imag.mrc");
			Itmp().setXmippOrigin();
			Itmp().xinit=0;
			if (!Itmp().sameShape(wsum_model.BPref[iclass].data))
			{
				wsum_model.BPref[iclass].data.printShape(std::cerr);
				Itmp().printShape(std::cerr);
				REPORT_ERROR("Incompatible size of "+fn_root+"_data_imag.mrc");
			}
			FOR_ALL_ELEMENTS_IN_ARRAY3D(Itmp())
			{
				A3D_ELEM(wsum_model.BPref[iclass].data, k, i, j).imag = A3D_ELEM(Itmp(), k, i, j);
			}

			Itmp.read(fn_root+"_weight.mrc");
			Itmp().setXmippOrigin();
			Itmp().xinit=0;
			if (!Itmp().sameShape(wsum_model.BPref[iclass].weight))
			{
				wsum_model.BPref[iclass].weight.printShape(std::cerr);
				Itmp().printShape(std::cerr);
				REPORT_ERROR("Incompatible size of "+fn_root+"_weight.mrc");
			}
			FOR_ALL_ELEMENTS_IN_ARRAY3D(Itmp())
			{
				A3D_ELEM(wsum_model.BPref[iclass].weight, k, i, j) = A3D_ELEM(Itmp(), k, i, j);
			}

			// Now perform the unregularized reconstruction
			// using a new backprojector object!
			BackProjector BPextra(wsum_model.BPref[iclass]);

			BPextra.reconstruct(Iunreg(), gridding_nr_iter, false, 1., dummy, dummy, dummy, dummy, 1., false, true, nr_threads, -1);

			// Update header information
			DOUBLE avg, stddev, minval, maxval;
		    Iunreg().computeStats(avg, stddev, minval, maxval);
		    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_MIN, minval);
		    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_MAX, maxval);
		    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_AVG, avg);
		    Iunreg.MDMainHeader.setValue(EMDL_IMAGE_STATS_STDDEV, stddev);
			Iunreg.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, mymodel.pixel_size);
			Iunreg.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, mymodel.pixel_size);
			Iunreg.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, mymodel.pixel_size);

			// write the resulting model to disc
			Iunreg.write(fn_root_out+"_unfil_for_fsc.mrc");
#ifdef DEBUG_CORRECT_FSC
			std::cerr << "node: " << node->rank << ", fn_root_out: " << fn_root_out <<
			std::cerr << ", fn_root: " << fn_root << std::endl;
#endif
		}

		// reconstruct_rank1 also sends the current_size to the master, so that it knows where to cut the FSC to zero
		MPI_Status status;
		if (node->rank == reconstruct_rank1)
			node->relion_MPI_Send(&mymodel.current_size, 1, MPI_INT, 0, MPITAG_INT, MPI_COMM_WORLD);
		if (node->rank == 0)
			node->relion_MPI_Recv(&mymodel.current_size, 1, MPI_INT, reconstruct_rank1, MPITAG_INT, MPI_COMM_WORLD, status);

		MPI_Barrier(MPI_COMM_WORLD);

		// B. read solvent mask from disc and multiply it to reconstructed map
		// and calculate solvent-corrected FSC curve
		// perform this process on the master node
		if( node->rank == 0 )
		{
			std::cout << "Calculating solvent-corrected gold-standard FSC ..." << std::endl;

			// read in half solvent mask from disc:
			// read in the half-reconstruction from rand2 and then perform FSC correction

			Image<DOUBLE> Iunreg1, Iunreg2;
			FileName fn_root1, fn_root2;

			if(iter > -1)
				fn_root1.compose(fn_out+"_it", iter, "", 3);
			else
				fn_root1 = fn_out;

			fn_root2.compose(fn_root1+"_half2_class", 1, "", 3);
			fn_root1.compose(fn_root1+"_half1_class", 1, "", 3);
			// fn_root2.compose(fn_root1+"_half2_class", 1, "", 3);

			fn_root1 += "_unfil_for_fsc.mrc";
			fn_root2 += "_unfil_for_fsc.mrc";

#ifdef DEBUG_CORRECT_FSC
			std::cerr << "node: " << node->rank << std::endl 
			<< "fn_root1: " << fn_root1 << ", fn_root2: " << fn_root2 << std::endl;
#endif

			Iunreg1.read(fn_root1);
			Iunreg2.read(fn_root2);
			Iunreg1().setXmippOrigin();
			Iunreg2().setXmippOrigin();

			// read in solvent mask obtained from ITK library from disc:
			FileName fn_root_msk1, fn_root_msk2;

			if(iter > -1)
				fn_root_msk1.compose(fn_out+"_it", iter, "", 3);
			else
				fn_root_msk1 = fn_out;

			fn_root_msk2.compose(fn_root_msk1+"_half_2_flatten_soft_mask_map_class", 1, "", 3);
			fn_root_msk1.compose(fn_root_msk1+"_half_1_flatten_soft_mask_map_class", 1, "", 3);
			

			fn_root_msk1 += ".mrc";
			fn_root_msk2 += ".mrc";

#ifdef DEBUG_CORRECT_FSC
		std::cerr << "node: " << node->rank << ", fn_root1: " << fn_root1 << ", fn_root_msk1: " << fn_root_msk1 << std::endl;
		std::cerr << "node: " << node->rank << ", fn_root2: " << fn_root2 << ", fn_root_msk2: " << fn_root_msk2 << std::endl;
#endif

			// the mask image:
			Image<DOUBLE> solvent_msk1, solvent_msk2;

			solvent_msk1.read(fn_root_msk1);
			solvent_msk2.read(fn_root_msk2);
			solvent_msk1().setXmippOrigin();
			solvent_msk2().setXmippOrigin();

			// for do phase-randomisation FSC-correction for the solvent mask
			MultidimArray<DOUBLE> fsc_unmasked, fsc_masked, fsc_random_masked, fsc_true;

			// calculate FSC of the unmasked maps
			getFSC(Iunreg1(), Iunreg2(), fsc_unmasked);

			Iunreg1() *= solvent_msk1();
			Iunreg2() *= solvent_msk2();
			
			// get the maksed FSC curve:
			getFSC(Iunreg1(), Iunreg2(), fsc_masked);

			// re-read the same input maps again and randomize phased before masking
			Iunreg1.read(fn_root1);
			Iunreg2.read(fn_root2);
			Iunreg1().setXmippOrigin();
			Iunreg2().setXmippOrigin();

			// Check at which resolution shell the FSC drops below 0.8
			int randomize_at = -1;
			FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(fsc_unmasked)
			{
				if (i > 0 && DIRECT_A1D_ELEM(fsc_unmasked, i) < 0.8)
				{
					randomize_at = i;
					break;
				}
			}
			if (randomize_at > 0)
			{
				if (verb > 0)
				{
					std::cout.width(35); std::cout << std::left << "  + randomize phases beyond: "; std::cout << XSIZE(Iunreg1())* mymodel.pixel_size / randomize_at << " Angstroms" << std::endl;
				}
				randomizePhasesBeyond(Iunreg1(), randomize_at);
				randomizePhasesBeyond(Iunreg2(), randomize_at);

				// mask randomized phases maps and calculate fsc_random_masked
				Iunreg1() *= solvent_msk1();
				Iunreg2() *= solvent_msk2();
				getFSC(Iunreg1(), Iunreg2(), fsc_random_masked);

				// Now that we have fsc_masked and fsc_random_masked, calculate fsc_true according to Richard's formula
				// FSC_true = FSC_t - FSC_n / ( )
				fsc_true.resize(fsc_masked);
				FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(fsc_true)
				{
					// 29jan2015: let's move this 2 shells upwards, because of small artefacts near the resolution of randomisation!
					if (i < randomize_at + 2)
					{
						DIRECT_A1D_ELEM(fsc_true, i) = DIRECT_A1D_ELEM(fsc_masked, i);
					}
					else
					{
						DOUBLE fsct = DIRECT_A1D_ELEM(fsc_masked, i);
						DOUBLE fscn = DIRECT_A1D_ELEM(fsc_random_masked, i);
						if (fscn > fsct)
							DIRECT_A1D_ELEM(fsc_true, i) = 0.;
						else
							DIRECT_A1D_ELEM(fsc_true, i) = (fsct - fscn) / (1. - fscn);
					}
				}
				mymodel.fsc_halves_class[iclass] = fsc_true;				

			}
			else
			{
				std::cerr << " WARNING: FSC curve between unmasked maps never drops below 0.8. Using unmasked FSC as FSC_true... "<<std::endl;
				std::cerr << " WARNING: This message should go away during the later stages of refinement!" << std::endl;

				mymodel.fsc_halves_class[iclass] = fsc_unmasked;

			}

					// then, set fsc_halves_class explicitly to zero beyond the current_size:

					for(int idx = mymodel.current_size / 2 +1; idx < MULTIDIM_SIZE(mymodel.fsc_halves_class[iclass]); idx++)
						DIRECT_A1D_ELEM(mymodel.fsc_halves_class[iclass], idx) = 0.;

#define DEBUG_FSC_SHAPE
#ifdef DEBUG_FSC_SHAPE
			std::cerr << "in fsc print shape part, node: " << node->rank << std::endl;
		(mymodel.fsc_halves_class[iclass]).printShape(std::cerr);
		(mymodel.data_vs_prior_class[iclass]).printShape(std::cerr);
#endif


						// the last step, because the updatecurrentResolution step is using the mymodel.data_vs_prior_class
						// so, only update the curve on the master node, and then broadcast it to all other nodes
						// the same process as in the reconstruct function in the backprojector.cpp file
						FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(mymodel.fsc_halves_class[iclass])
						{
							DOUBLE myfsc = XMIPP_MAX(0.001, DIRECT_A1D_ELEM(mymodel.fsc_halves_class[iclass], i));
							if(has_converged)
							{
								// Sqrt-term to get 60-degree phase errors....
								myfsc = sqrt(2. * myfsc / (myfsc + 1.));
							}
							myfsc = XMIPP_MIN(0.999, myfsc);
							DOUBLE myssnr = myfsc / (1. - myfsc);

							// now, update the mymodel.data_vs_prior[iclass]:
							DIRECT_A1D_ELEM(mymodel.data_vs_prior_class[iclass], i) = myssnr;
						}


		} // end if the node is at the master rank

		// this time, must update the mymodel.data_vs_prior_class[iclass] curve!!!
// #define DEBUG_FSC_SHAPE
/*#ifdef DEBUG_FSC_SHAPE

		(mymodel.fsc_halves_class[iclass]).printShape(std::cerr);
#endif*/

/*		FOR_ALL_DIRECT_ELEMENTS_IN_ARRAY1D(mymodel.fsc_halves_class[iclass])
		{
			DOUBLE myfsc = XMIPP_MAX(0.001, DIRECT_A1D_ELEM(mymodel.fsc_halves_class[iclass], i));
			if(has_converged)
			{
				// Sqrt-term to get 60-degree phase errors....
				myfsc = sqrt(2. * myfsc / (myfsc + 1.));
			}
			myfsc = XMIPP_MIN(0.999, myfsc);
			DOUBLE myssnr = myfsc / (1. - myfsc);

			// now, update the mymodel.data_vs_prior[iclass]:
			DIRECT_A1D_ELEM(mymodel.data_vs_prior_class[iclass], i) = myssnr;
		}
*/
		// the master node sends the fsc curve to everyone else
		node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.fsc_halves_class[iclass]),
			 MULTIDIM_SIZE(mymodel.fsc_halves_class[iclass]), MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);

		// At last, make sure all nodes have the same resolution, set the data_vs_prior_class array
		// from half1 also for half2
		// the master node sends the fsc curve to all other slave nodes!!
		// for(int iclass = 0; iclass < mymodel.nr_classes; iclass++)
			node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.data_vs_prior_class[iclass]),
				MULTIDIM_SIZE(mymodel.data_vs_prior_class[iclass]), MY_MPI_DOUBLE, 0, MPI_COMM_WORLD);


/*
			node->relion_MPI_Bcast(MULTIDIM_ARRAY(mymodel.sigma2_class[iclass]),
					MULTIDIM_SIZE(mymodel.sigma2_class[iclass]), MY_MPI_DOUBLE, reconstruct_rank, MPI_COMM_WORLD);

*/

		// C.
		if( mymodel.ref_dim == 3 && (node->rank == reconstruct_rank1 || node->rank == reconstruct_rank2) )
		{
			// C. remove temporary arrays from the disc
			int random_halfset = (node->rank %2 == 1)? 1 : 2;
			FileName fn_root = fn_out + "_half" + integerToString(random_halfset);; 
			fn_root.compose(fn_root+"_class", iclass+1, "", 3);

#ifdef DEBUG_CORRECT_FSC
			std::cerr << "C part in correct fsc function, node: " << node->rank << ", fn_root: " << fn_root << std::endl;
#endif
			remove((fn_root+"_data_real.mrc").c_str());
			remove((fn_root+"_data_imag.mrc").c_str());
			remove((fn_root+"_weight.mrc").c_str());
		}

	} // end the loop over all classes


	// A. read temporary data and weight arrays from disc to perform unregularised reconstruction
	// notice the random subset problem!

	// B. read already written soft mask map from disc, to multiply it to
	// unregularised reconstructed map

	// C. remove the temporary written data and weight file from disc
}
