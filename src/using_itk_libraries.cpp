
/*
  This header filer will use the ITK lirary to process the 3D density map
  in refine process
*/

// #ifndef USING_ITK_LIBRARIES_H
// #define USING_ITK_LIBRARIES_H

#include "src/using_itk_libraries.h"


// ==========
// First, is Gaussian smoothing function.
// the function to process input map with Gaussian filter, 
// to smooth the origin map, and suppress noise
extern void GaussianFilterMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap_new,
    DOUBLE GaussianKernelSigma, DOUBLE GaussianMaxKernelWidth )
{
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 3 >    InputImageType;
	typedef itk::Image< PixelType, 3 >    OutputImageType;


	typedef itk::DiscreteGaussianImageFilter<
				InputImageType, OutputImageType>	FilterType;
	FilterType::Pointer filter = FilterType::New();
	DOUBLE gaussianVariance = GaussianKernelSigma* GaussianKernelSigma;

	filter->SetVariance( gaussianVariance );
	filter->SetMaximumKernelWidth( GaussianMaxKernelWidth );

	filter->SetInput( inputmap );

	OutputImageType::Pointer outputmap_old = OutputImageType::New();
	outputmap_old = filter->GetOutput();

	try
	{
	filter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
	std::cerr << "Exception thrown " << excp << std::endl;
	}

	// set some important properties of outputmap_new to be the same as inputmap:
	outputmap_new->SetRegions( inputmap->GetRequestedRegion() );
	outputmap_new->SetSpacing( inputmap->GetSpacing() );
	outputmap_new->CopyInformation( inputmap );
	outputmap_new->Allocate( true );

	typedef itk::ImageRegionIterator< OutputImageType > IteratorType_output;

	IteratorType_output outputmapIt( outputmap_old, outputmap_old->GetRequestedRegion() );
	IteratorType_output outputmap_newIt( outputmap_new, outputmap_new->GetRequestedRegion() );

	for( outputmapIt.GoToBegin(), outputmap_newIt.GoToBegin(); !outputmapIt.IsAtEnd(); ++outputmapIt )
	{
		outputmap_newIt.Set( outputmapIt.Get() );
		++outputmap_newIt;
	}

}

// ==========
// Second section, should be for threshoding section, for input smoothed map
// to compute the thresholding value which is based on histogram method of input image
// the computed thresholding value should be used for binarization of next section


// whatever the Otsu's method or Kittler-Illingworh's method, all of them are based on
// the distribution of Histogram of input map
// in this section, First we should get the 1D image with pixels of those origin 3D density map
// confined in a sphere with specific radius( half of particle diameter)

// ===== A.
// the function only maintain the output image pointer voxel value
// within the user defined sphere radius to be parsed from command line
extern void DensityMapinDefinedSphereRadiusProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 1 > *outputimage,
    DOUBLE user_defined_sphere_radius )
{
	typedef DOUBLE    PixelType;
	typedef itk::Image< PixelType, 3 >    InputImageType;
	typedef itk::Image< PixelType, 1 >    OutputImageType;

	// get the side length of input map
	DOUBLE side_length;
	side_length = (DOUBLE)((inputmap->GetRequestedRegion()).GetSize()[1]);

	// the DOUBLE type of centre coordinate of sphere
	DOUBLE centre_coord = (side_length - 1)/2;

	DOUBLE sqr_user_radius = user_defined_sphere_radius * user_defined_sphere_radius + 1;

	// using the iterator with index, to loop over the input map:
	// then get the 1D IMAGE:
	typedef itk::ImageRegionIteratorWithIndex< InputImageType >  IteratorWithIndexType;
	IteratorWithIndexType input_imageItwithindex( inputmap, inputmap->GetRequestedRegion() );

	// number of voxels of original map within user defined sphere:
	unsigned long int voxel_count_cond = 0;

	// iterator looping over the original 3D map:
	for( input_imageItwithindex.GoToBegin(); !input_imageItwithindex.IsAtEnd(); ++input_imageItwithindex )
	{
		InputImageType::IndexType inputimage_idx = input_imageItwithindex.GetIndex();
		// squared distance of the grid point from centre of the user defined sphere:
		DOUBLE sqr_dist_from_cen = (inputimage_idx[0]-centre_coord)*(inputimage_idx[0]-centre_coord)
					+ (inputimage_idx[1]-centre_coord)*(inputimage_idx[1]-centre_coord)
					+ (inputimage_idx[2]-centre_coord)*(inputimage_idx[2]-centre_coord);

		// if the current voxel is in the user defined sphere, then do some process:
		if( sqr_dist_from_cen - sqr_user_radius <= 1e-6 )
		{
			voxel_count_cond++;
		}
	}

	// then, create the NEW 1D IMAGE, to store voxels within the sphere
	// OutputImageType::Pointer output_image = OutputImageType::NEW();

	// then, setup some properties of the NEW 1D IMAGE:
	// set the start index to ZERO:
	const OutputImageType::IndexType start = {{0}};
	// set the size of new image:
	const OutputImageType::SizeType size = {{voxel_count_cond}};

	// output image region:
	OutputImageType::RegionType output_region;
	output_region.SetSize( size );
	output_region.SetIndex( start );

	// allocate the outputimage pointer, which is from the input parameters of this function:
	outputimage->SetRegions( output_region );
	outputimage->Allocate( true );

	// then, for outputimage pointer, do create the special region iterator(1Dimension!!!)
	// stores original map's voxel value of 3D map to the outputimage:
	typedef itk::ImageRegionIteratorWithIndex< OutputImageType >  NewIteratorType;
	NewIteratorType outputimage_It( outputimage, outputimage->GetRequestedRegion() );

	for( input_imageItwithindex.GoToBegin(), outputimage_It.GoToBegin(); !input_imageItwithindex.IsAtEnd(); ++input_imageItwithindex )
	{
		InputImageType::IndexType inputimage_idx = input_imageItwithindex.GetIndex();
		// squared distance of the grid point from centre of the user defined sphere:
		DOUBLE sqr_dist_from_cen = (inputimage_idx[0]-centre_coord)*(inputimage_idx[0]-centre_coord)
					+ (inputimage_idx[1]-centre_coord)*(inputimage_idx[1]-centre_coord)
					+ (inputimage_idx[2]-centre_coord)*(inputimage_idx[2]-centre_coord);

		if( (sqr_dist_from_cen - sqr_user_radius <= 1e-6) && (!outputimage_It.IsAtEnd()) )
		{
			PixelType ori_image_pix_value = inputmap->GetPixel( inputimage_idx );
			outputimage_It.Set( ori_image_pix_value );
			++outputimage_It;
		}
	}

// #define DEBUG_SPHERE_FUNC
#ifdef DEBUG_SPHERE_FUNC
	std::cerr << std::setprecision( 15 );
	std::cerr << "====================" << std::endl;
	std::cerr << "In function DensityMapinDefinedSphereRadiusProcess debugging part." << std::endl;
	std::cerr << "Size of inputmap: " << ((inputmap->GetRequestedRegion()).GetSize()) << std::endl;
	std::cerr << "Origin of inputmap: " << (inputmap->GetOrigin()) << std::endl;
	std::cerr << "Size of outputimage: " << ((outputimage->GetRequestedRegion()).GetSize()) << std::endl;
	std::cerr << "Origin of outputimage: " << (outputimage->GetOrigin()) << std::endl;
	OutputImageType::IndexType pixel_index = {{201}};
	OutputImageType::PixelType pix_value = outputimage->GetPixel(pixel_index);
	std::cerr << "one pixel value of outputimage: " << pix_value << std::endl;
	std::cerr << "user_defined_sphere_radius value: " << user_defined_sphere_radius << std::endl;
	std::cerr << "side_length value: " << side_length << std::endl;
	std::cerr << "centre_coord value: " << centre_coord << std::endl;
	std::cerr << "voxel_count_cond value: " << voxel_count_cond << std::endl;
	std::cerr << "====================" << std::endl;
#endif

} // end the function DensityMapinDefinedSphereRadiusProcess


// Then, if is for gaussian smoothing thresholding in defined spherical radius
// so the program will do process to compute information focused region based on histogram of input image
// of TWO directions, from minimum and maximum boundary of histogram
// ===== B.
// the function to process input image in specific percentage of histogram information focused region
// IMPORTANT!!! the function ONLY process to compute info focused region of Gaussian smoothed map

extern void InformationFocusedRegionDensityProcess( itk::Image<DOUBLE,1> *input_image, itk::Image<DOUBLE,1> *output_image,
	DOUBLE man_centre_fraction_threshold, DOUBLE & left_height_sum, DOUBLE & right_height_sum, 
	DOUBLE & computed_left_bound, DOUBLE & computed_right_bound, bool is_gradient_mag )//= false )
{
	typedef DOUBLE PixelType;
	typedef itk::Image< DOUBLE, 1 > InputImageType;
	typedef itk::Image< DOUBLE, 1 > OutputImageType;

	typedef itk::Statistics::DenseFrequencyContainer2	FrequencyContainerType;
	// typedef itk::Statistics::SparseFrequencyContainer2	FrequencyContainerType;

	typedef itk::Statistics::ImageToHistogramFilter< InputImageType >
		ImageToHistogramFilterType;

	// typedef DOUBLE MeasurementType;
	typedef double MeasurementType;
	typedef itk::Statistics::Histogram < MeasurementType,
		FrequencyContainerType > HistogramType;
	// then convert the input 1D image to histogram, then search for information focused
	// region, and measurement bounds (sum of those bins' height slightly greater than a threshold)

  ImageToHistogramFilterType::Pointer  image2hist_filter =
   ImageToHistogramFilterType::New();

  const unsigned int Measurementvectorsize = 1; // because the 1D histogram
  // const unsigned int numElems = 10000; // number of histogram bins
  const unsigned int numElems = 8000; // number of histogram bins
  const unsigned int binsPerDimension = numElems;


  ImageToHistogramFilterType::HistogramType::SizeType
    histSize( Measurementvectorsize );

  histSize.Fill( binsPerDimension );

  // convert the input image to histogram
  image2hist_filter -> SetInput( input_image );
  image2hist_filter -> SetHistogramSize( histSize );

  HistogramType::Pointer histogram_1 = HistogramType::New();
  histogram_1 = image2hist_filter -> GetOutput();

  // Then, the program will need to update the ImageToHistogramFilter pointer to invoke the conver process:
  try
  {
    image2hist_filter -> Update();
  }
  catch( itk::ExceptionObject & err )
  {
    std::cerr << "Exception caught!" << std::endl;
    std::cerr << err << std::endl;
    // return EXIT_FAILURE;
  }


  // Here, based on the input parameter of this function( the gradient_mag_or_not parameter)
  // the function will do properly process
  // if the input map is gradient magnitude map, this function will only compute right boundary value
  // TWO FOLD of left value, if input map is gaussian filtered, this function will compute both
  // TWO BOUNDARY VALUE

  // First, the Gaussian smoothed input density map condition:

  // get total voxel number of information focused region: sum of GetSpacing function of itk::Image
  // sustract sum of TWO TAILS in the histogram

  // get the bin width of histogram
  const DOUBLE bin_width = (histogram_1->GetMeasurement(3,0) - histogram_1->GetMeasurement(2,0));
  // const DOUBLE fraction_threshold = 0.999999; // this can be manually set
  // ====> this fraction is set by input parameter of this function

  // const DOUBLE bound_fraction = 1.0 - fraction_threshold; // used for scanning histogram
  const DOUBLE bound_fraction = 1.0 - man_centre_fraction_threshold; // sum of two tails zone, shoule be divided by 2
  const DOUBLE bound_fraction_one_tail = bound_fraction/2;

  // sum of all histogram bins height
  const DOUBLE total_freq = histogram_1->GetTotalFrequency();


  // to record number of voxels cannot meet the condition( in the TAIL FRACTION zone)
  // absolute number of voxels ( summation of histogram bins) in left or right TAIL ZONE
  DOUBLE left_sum = 0.;
  DOUBLE right_sum = 0.; 

  // variables to record bound of left and right tailing ZONE
  // HISTOGRAM bins between the left and right bound value demonstrate focused information of IMAGE SIGNAL
  DOUBLE left_bound, right_bound;

  // the variable to store left bin number and right bin number inside tailing zone ----> in the loop: i value
  unsigned long int left_bin_num = 0;
  unsigned long int right_bin_num = 0;

  // the variable to compute sum of bins height during iterating loop, in the TAIL ZONE OF HISTOGRAM
  DOUBLE frac_freq = 0.; 


  // if the input map is gaussian filtered map:

  if( !is_gradient_mag )
  {
	  // the forward order, from LEFT BOUND to right bound of input image histogram
	  for(unsigned long int i=0; i < histogram_1->GetSize()[0]; i++)
	  {
	    frac_freq += histogram_1->GetFrequency(i);

	    // if( frac_freq/total_freq >= bound_fraction )
	    if( frac_freq/total_freq - bound_fraction_one_tail >= 1e-10 )
	    {
	      // left_bound = (histogram_1->GetMeasurement(i-1,0))- (bin_width/2);
	      left_bound = (histogram_1->GetMeasurement(i,0))+ (bin_width/2);
	      // in order to assign value to left_sum:
	      // frac_freq -= (histogram_1->GetFrequency(i)+ histogram_1->GetFrequency(i-1) );
	      // left_bin_num = i - 2;
	      left_bin_num = i;
	      break;
	    }
	  }

	  // assign value, the left tail related:
	  left_sum = frac_freq;
	  left_height_sum = left_sum;

	  frac_freq = 0.; // refresh the value

	  // the reverse order, from RIGHT BOUND to left bound of input image histogram
	  for( unsigned long int i= (histogram_1->GetSize()[0])-1; i >= 0; i-- )
	  {
	    frac_freq += histogram_1->GetFrequency(i);

	    if( frac_freq/total_freq - bound_fraction_one_tail >= 1e-10 )
	    {
			// right_bound = (histogram_1->GetMeasurement(i+1,0))+ (bin_width/2);
		    right_bound = (histogram_1->GetMeasurement(i,0))- (bin_width/2);
		    // in order to assign value to right_sum:
		    // frac_freq -= (histogram_1->GetFrequency(i)+ histogram_1->GetFrequency(i+1) );
		    // right_bin_num = i + 2;
		    right_bin_num = i;
		    break;
	    }
	  }

	  // assign value, the right tail related:
	  right_sum = frac_freq;
	  right_height_sum = right_sum;
	  frac_freq = 0.;

	  // assign left bound value and right bound value to input
	  // computed left/right bound variables value, because parameters of this function
	  // using reference, so this assignment section will change input variables' value
	  computed_left_bound = left_bound;
	  computed_right_bound = right_bound;

  }
  else if( is_gradient_mag )
  {
	// if the input map is gradient magnitude map, the left boundary value is the left boundary value of 
  	// the first histogram bin
  	// the right boudary value will computed from TWO FOLD of manual set threshold value

  	left_bound = histogram_1->GetMeasurement(0,0) - (bin_width/2);

  	// the next for iteration will use the bound_fraction for judgement condition to compute the right boundary value

  	left_bin_num = 0;
	frac_freq = 0.; // refresh the value

	// the reverse order, from RIGHT BOUND to left bound of input image histogram
	for( unsigned long int i= (histogram_1->GetSize()[0])-1; i >= 0; i-- )
	{
	  frac_freq += histogram_1->GetFrequency(i);

	  if( frac_freq/total_freq - bound_fraction >= 1e-10 )
	  {
		// right_bound = (histogram_1->GetMeasurement(i+1,0))+ (bin_width/2);
	    right_bound = (histogram_1->GetMeasurement(i,0))- (bin_width/2);
	    // in order to assign value to right_sum:
	    // frac_freq -= (histogram_1->GetFrequency(i)+ histogram_1->GetFrequency(i+1) );
	    // right_bin_num = i + 2;
	    right_bin_num = i;
	    break;
	  }
	}

	// assign value, the right tail related:
	// Here only assign the right tail related value,
	// becaue the left value is the smallest histogram bin boundary value
	// so there is nonsense to do process for left boundary value related assignment
	right_sum = frac_freq;
	right_height_sum = right_sum;
	frac_freq = 0.;

	// assign left bound value and right bound value to input
	// computed left/right bound variables value, because parameters of this function
	// using reference, so this assignment section will change input variables' value
	computed_left_bound = left_bound;
	computed_right_bound = right_bound;

  }


  // next section will be the assigning value section, assigning voxel values between left and
  // right boundary value to a NEW 1D IMAGE pointer!!!

  // which will be output of the if judgement part!!!!

  // after the computation of left and right teil zone bound value, the function will 
  // create the new 1D IMAGE, specifically storing voxels with value in the information focused ZONE
  // and the program will also compute number of voxels with value between left and right bound value AGAIN
  // for assign size of new image

  // general work flow:
  // compute pixels in the information focused zone --> set it to size of output image
  // --> looping over input image using iterator and make judgement --> assign value to output image

  // total_voxel - (long int)(left_sum+right_sum);

  // in this section, will use the iterator with index
  typedef itk::ImageRegionIteratorWithIndex< InputImageType > IteratorWithIndexType;
  IteratorWithIndexType input_imageIt( input_image, input_image->GetRequestedRegion() );

  // the variable to record voxels in informatin focused ZONE, and for
  // side length size assignment of new image:
  unsigned long int number_condition_focused_zone = 0;

  for( input_imageIt.GoToBegin(); !input_imageIt.IsAtEnd(); ++input_imageIt )
  {
    InputImageType::IndexType inputimage_idx = input_imageIt.GetIndex();

    PixelType input_image_pix_value = input_image->GetPixel( inputimage_idx );
    // BECAUSE, the input image already stores voxels in the sphere, so here
    // only need to judge whether the current voxel value is in the information focused ZONE
    // if in the zone
    if( (input_image_pix_value - left_bound >= 1e-10) && (input_image_pix_value - right_bound <= 1e-10) )
    {
      number_condition_focused_zone++;
    }
  }


  // assign values to output image:
  // set the start index to ZERO!
  const OutputImageType::IndexType start_output = {{0}};
  // set the size of output image:
  const OutputImageType::SizeType size_output = {{number_condition_focused_zone}};

  // output image region:
  OutputImageType::RegionType region_output;
  region_output.SetSize( size_output );
  region_output.SetIndex( start_output );

  // allcate memory to output image:
  output_image->SetRegions( region_output );
  output_image->Allocate( true );

  // make a new iterator belongs to output_image:
  IteratorWithIndexType output_imageIt( output_image, output_image->GetRequestedRegion() );

  for( input_imageIt.GoToBegin(), output_imageIt.GoToBegin(); !input_imageIt.IsAtEnd(); ++input_imageIt )
  {
    InputImageType::IndexType inputimage_idx = input_imageIt.GetIndex();

    PixelType input_image_pix_value = input_image->GetPixel( inputimage_idx );
    // BECAUSE, the input image already stores voxels in the sphere, so here
    // only need to judge whether the current voxel value is in the information focused ZONE
    // if in the zone
    if( (input_image_pix_value - left_bound >= 1e-10) && (input_image_pix_value - right_bound <= 1e-10) && (!output_imageIt.IsAtEnd()))
    {
      output_imageIt.Set( input_image_pix_value );
      ++output_imageIt;
    }
  }

// #define DEBUG_INFO_FOCUSED_FUNC
#ifdef DEBUG_INFO_FOCUSED_FUNC
	std::cerr << "====================" << std::endl;
	std::cerr << std::setprecision( 15 );
	std::cerr << "In the InformationFocusedRegionDensityProcess function debugging part." << std::endl;
	std::cerr << "Some information of input_image:" << std::endl;
	std::cerr << "Size of input_image: " << ((input_image->GetRequestedRegion()).GetSize()) << std::endl;
	std::cerr << "Origin of input_image: " << (input_image->GetOrigin()) << std::endl;
	InputImageType::IndexType pixel_index = {{201}};
	InputImageType::PixelType pix_value = input_image->GetPixel(pixel_index);
	std::cerr << "one pixel value of input_image: " << pix_value << std::endl;
	// std::cerr << "user_defined_sphere_radius value: " << user_defined_sphere_radius << std::endl; // this
	// variable hasn't been declared in this scope

	std::cerr << "Some information of  output_image:" << std::endl;
	std::cerr << "Size of output_image: " << ((output_image->GetRequestedRegion()).GetSize()) << std::endl;
	std::cerr << "Origin of output_image: " << (output_image->GetOrigin()) << std::endl;
	OutputImageType::IndexType pixel_index_output = {{201}};
	OutputImageType::PixelType pix_value_output = output_image->GetPixel(pixel_index_output);
	std::cerr << "ont pixel value of output_image: " << pix_value_output << std::endl;
	std::cerr << "----------------" << std::endl;
	std::cerr << "Some variables' value of the InformationFocusedRegionDensityProcess function" << std::endl;
	std::cerr << "Total frequency of input_image histogram: " << total_freq << std::endl;
	std::cerr << "Total number of histogram bins of histogram_1: " << (histogram_1->GetSize()) << std::endl;
	std::cerr << "left_bin_num value: " << left_bin_num << std::endl;
	std::cerr << "right_bin_num value: " << right_bin_num << std::endl;
	std::cerr << "left_bound value: " << left_bound << std::endl;
	std::cerr << "right_bound value: " << right_bound << std::endl;
	std::cerr << "left_sum value: " << left_sum << std::endl;
	std::cerr << "right_sum value: " << right_sum << std::endl;
	DOUBLE sum_left_right_tail = left_sum + right_sum;
	std::cerr << "sum of left_sum and right_sum, sum_left_right_tail value: " << sum_left_right_tail << std::endl;
	// then need to print number of voxels with value between the left and right zone.
	std::cerr << "number of voxels in information focused zone, total frequency method: " << ( total_freq - sum_left_right_tail ) << std::endl;
	std::cerr << "number of voxels in information focused zone, image getsize method: " 
    	      << ( (input_image->GetRequestedRegion()).GetSize()[0] - sum_left_right_tail ) << std::endl;
	std::cerr << "----------------" << std::endl;
	std::cerr << "====================" << std::endl;
#endif

} // end the function InformationFocusedRegionDensityProcess


// then, do Otus's method or Kittler-Illingworth' method thresholding of output image from the previous step

// C.
// the function to compute the otsu filter global threshold value
// of input image pointer, and print the computed threshold value to screen
// set the default threshold value of the parameter of the function to be ZERO
extern void OtsuFilterThresholdProcess( itk::Image< DOUBLE, 1 > *inputimage,
	unsigned long int numberofHistogramBins, DOUBLE & thres_val,
	DOUBLE insideValue, DOUBLE outsideValue )
	// DOUBLE insideValue = 0., DOUBLE outsideValue = 1.)
{
	typedef DOUBLE    PixelType;
	typedef itk::Image< PixelType, 1 >    InputImageType;
	typedef itk::OtsuThresholdImageFilter<
			InputImageType, InputImageType> FilterType;

	FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputimage );

	filter->SetInsideValue( insideValue );
	filter->SetOutsideValue( outsideValue );
	filter->SetNumberOfHistogramBins( numberofHistogramBins );

	try
	{
		filter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Exception thrown " << excp << std::endl;
	}

	// then, get the otsu computed global threshold value:
	DOUBLE threshold_value = filter->GetThreshold();

	// assign the threshold_value to thres_val of the parameter of this function
	thres_val = threshold_value;

	// std::cerr << std::setprecision( 15 );
	// std::cerr << "Otsu thresholding value: " << threshold_value << std::endl;

// the debugging part
// #define DEBUG_OTSU_FUNC
#ifdef DEBUG_OTSU_FUNC
	std::cerr << std::setprecision( 15 );
	std::cerr << "====================" << std::endl;
	std::cerr << "In OtsuFilterThresholdProcess function debugging part" << std::endl;
	std::cerr << "Size of input_image: " << ((inputimage->GetRequestedRegion()).GetSize()) << std::endl;
	InputImageType::IndexType pixel_index = {{201}};
	InputImageType::PixelType pix_value = inputimage->GetPixel(pixel_index);
	std::cerr << "one pixel value of inputimage: " << pix_value << std::endl;
	std::cerr << "Number of histogram bins of filter: " << (filter->GetNumberOfHistogramBins()) << std::endl;
	std::cerr << "Otsu thresholding value: " << threshold_value << std::endl;
	std::cerr << "====================" << std::endl;
#endif

} // end the function OtsuFilterThresholdProcess


// D.
// the function to compute the Kittler Illingworth filter global threshold value
// of input image pointer, and print the computed threshold value to screen
// set the threshold value of the parameter of the function to be ZERO
extern void KittlerIllingWorthFilterThresholdProcess( itk::Image< DOUBLE, 1 > *inputimage,
	unsigned long int numberofHistogramBins, DOUBLE & thres_val ) //DOUBLE insideValue, DOUBLE outsideValue )
{
	typedef DOUBLE    PixelType;
	typedef itk::Image< PixelType, 1 >    InputImageType;
	typedef itk::KittlerIllingworthThresholdImageFilter<
			InputImageType, InputImageType> FilterType;

	FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputimage );

	DOUBLE insideValue = 0.;
	DOUBLE outsideValue = 1.;
	filter->SetInsideValue( insideValue );
	filter->SetOutsideValue( outsideValue );
	filter->SetNumberOfHistogramBins( numberofHistogramBins );

	try
	{
		filter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Exception thrown " << excp << std::endl;
	}

	// then, get the kittler Illingworth computed global threshold value:
	DOUBLE threshold_value = filter->GetThreshold();

	// assign the computed threshold_value to thres_val
	thres_val = threshold_value;

	// std::cerr << std::setprecision( 15 );
	// std::cerr << "KittlerIllingworth Filter thresholding value: " << threshold_value << std::endl;

// the debugging part
// #define DEBUG_KITTLER_FUNC
#ifdef DEBUG_KITTLER_FUNC
	std::cerr << std::setprecision( 15 );
	std::cerr << "====================" << std::endl;
	std::cerr << "In KittlerFilterThresholdProcess function debugging part" << std::endl;
	std::cerr << "Size of input_image: " << ((inputimage->GetRequestedRegion()).GetSize()) << std::endl;
	InputImageType::IndexType pixel_index = {{201}};
	InputImageType::PixelType pix_value = inputimage->GetPixel(pixel_index);
	std::cerr << "one pixel value of inputimage: " << pix_value << std::endl;
	std::cerr << "Number of histogram bins of filter: " << (filter->GetNumberOfHistogramBins()) << std::endl;
	std::cerr << "KittlerIllingworth Filter thresholding value: " << threshold_value << std::endl;
	std::cerr << "====================" << std::endl;
#endif

} // end the function KittlerIllingworthFilterThresholdProcess


// ==========
// Third section
// this is the binarization section, the function to be defined in this section will binarize the input map
// with threshold value( the input parameter of function)
// both the input map and output map will be 3 dimenstional
extern void ThresholdBinarizeDensityMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
    DOUBLE binarize_threshold_value )
{
	// set some typedef classes
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 3 >		InputImageType;
	typedef itk::Image< PixelType, 3 >		OutputImageType;


	// assign properties of output map to be the same as input map
	outputmap -> SetRegions( inputmap -> GetRequestedRegion() );
	outputmap -> SetSpacing( inputmap -> GetSpacing() );
	outputmap -> CopyInformation( inputmap );
	outputmap -> Allocate( true );

	// outputmap_new->SetRegions( inputmap->GetRequestedRegion() );
	// outputmap_new->Allocate( true );
	// outputmap_new->SetSpacing( inputmap->GetSpacing() );


	// define TWO iterators of input and output map at the same time
	typedef itk::ImageRegionIterator< InputImageType >	InputIteratorType;
	typedef itk::ImageRegionIterator< OutputImageType >	OutputIteratorType;

	InputIteratorType	inputmapIt( inputmap, inputmap->GetRequestedRegion() );
	OutputIteratorType	outputmapIt( outputmap, outputmap->GetRequestedRegion() );
	// and then looping all voxels of input map and do judgement to those voxel values
	// then assign voxel values to output map

	for( inputmapIt.GoToBegin(), outputmapIt.GoToBegin(); !inputmapIt.IsAtEnd(); ++inputmapIt )
	{
		// do judgement and assignment of outputmapit
		if( inputmapIt.Get() - binarize_threshold_value >= 1e-10 )
		{ outputmapIt.Set(1.0); }
		else
		{ outputmapIt.Set(0.0); }

		++outputmapIt;
	}
}


// ==========
// Fourth section.
// The binary dilation function
// to make dilating process of input binary density map

extern void BinaryDilationDensityProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
	int binary_dilating_radius )
 	// int binary_dilating_radius = 1)
{
	typedef DOUBLE	PixelType;
	typedef itk::Image< PixelType, 3 >		InputImageType;
	typedef itk::Image< PixelType, 3 >		OutputImageType;

	// for the binary dilating process:
	typedef itk::BinaryBallStructuringElement<
							PixelType, 3 >		StructuringElementType;

	StructuringElementType structuringElement;

	structuringElement.SetRadius( binary_dilating_radius );

	structuringElement.CreateStructuringElement();

	typedef itk::BinaryDilateImageFilter<
				InputImageType, OutputImageType, StructuringElementType>	DilateFilterType;

	DilateFilterType::Pointer binaryDilate = DilateFilterType::New();
	binaryDilate->SetKernel( structuringElement );
	binaryDilate->SetInput( inputmap );

	PixelType foreground = 1.0;
	binaryDilate->SetDilateValue( foreground );

	OutputImageType::Pointer outputmap_old = OutputImageType::New();
	outputmap_old = binaryDilate->GetOutput();

	// update the binary dilate filter:
	try
	{
		binaryDilate->Update();
	}
	catch( itk::ExceptionObject & err )
	{
		std::cerr << "Exception thrown " << err << std::endl;
	}


	outputmap->SetRegions( inputmap->GetRequestedRegion() );
	outputmap->SetSpacing( inputmap->GetSpacing() );
	outputmap->CopyInformation( inputmap );
	outputmap->Allocate( true );

	typedef itk::ImageRegionIterator< OutputImageType >	OutputIteratorType;

	OutputIteratorType	outputmap_oldIt( outputmap_old, outputmap_old->GetRequestedRegion() );
	OutputIteratorType	outputmapIt( outputmap, outputmap->GetRequestedRegion() );

	for(outputmap_oldIt.GoToBegin(), outputmapIt.GoToBegin(); !outputmap_oldIt.IsAtEnd(); ++outputmap_oldIt)
	{
		outputmapIt.Set( outputmap_oldIt.Get() );
		++outputmapIt;
	}
}

// ==========
// the binarization function to process the input map, if the voxel value is inside the user defined value zone
// then set it to 1.0, in other cases, set it to 0.0

extern void BinarizeDensityMapinRangeProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
								DOUBLE &lower_boundary_value, DOUBLE &upper_boundary_value )
{
	// if the voxel value is inside the zone: [lower, upper], set it to 1.0
	// otherwise, set to 0.0
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 3 >		InputImageType;
	typedef itk::Image< PixelType, 3 >		OutputImageType;

	// assign properties of output map to be the same as input map
	outputmap -> SetRegions( inputmap -> GetRequestedRegion() );
	outputmap -> SetSpacing( inputmap -> GetSpacing() );
	outputmap -> CopyInformation( inputmap );
	outputmap -> Allocate( true );

	// outputmap_new->SetRegions( inputmap->GetRequestedRegion() );
	// outputmap_new->Allocate( true );
	// outputmap_new->SetSpacing( inputmap->GetSpacing() );


	// define TWO iterators of input and output map at the same time
	typedef itk::ImageRegionIterator< InputImageType >	InputIteratorType;
	typedef itk::ImageRegionIterator< OutputImageType >	OutputIteratorType;

	InputIteratorType	inputmapIt( inputmap, inputmap->GetRequestedRegion() );
	OutputIteratorType	outputmapIt( outputmap, outputmap->GetRequestedRegion() );
	// and then looping all voxels of input map and do judgement to those voxel values
	// then assign voxel values to output map

	for( inputmapIt.GoToBegin(), outputmapIt.GoToBegin(); !inputmapIt.IsAtEnd(); ++inputmapIt )
	{
		// do judgement and assignment of outputmapit
		if( ( inputmapIt.Get() - lower_boundary_value >= 1e-12 ) &&
			( inputmapIt.Get() - upper_boundary_value <= -1e12 ) )
		{ outputmapIt.Set(1.0); }
		else
		{ outputmapIt.Set(0.0); }

		++outputmapIt;
	}

}




// ==========
// Fifth section
// after binarization and binary dilation section, we need to implement watershed method
// to fill out holes inside the binary density map
// after segmentation of these holes, we need to add them to the previous binary density map

// the function to use watershed segmentation filter:
extern void WatershedSegmentDensityProcess( itk::Image< DOUBLE, 3 > * inputmap, itk::Image< DOUBLE, 3 > * outputmap,
	DOUBLE watershed_threshold_func , DOUBLE watershed_level_func )
    // DOUBLE watershed_threshold_func = 0., DOUBLE watershed_level_func = 1. )// need more parameters )
{
	typedef DOUBLE	InputPixelType;
	typedef DOUBLE  OutputPixeltype;
	typedef DOUBLE  PixelType;
	typedef itk::Image< InputPixelType, 3 >		InputImageType;
	typedef itk::Image< OutputPixeltype, 3 >		OutputImageType;

	typedef unsigned long int 	WatershedsOutputPixelType;
	typedef itk::WatershedImageFilter< InputImageType >		WatershedFilterType;
	WatershedFilterType::Pointer watershed_filter = WatershedFilterType::New();
	watershed_filter->SetLevel( watershed_level_func );
	watershed_filter->SetThreshold( watershed_threshold_func );

	typedef itk::Image< WatershedsOutputPixelType, 3 >	WatershedOutputImageType;
	WatershedOutputImageType::Pointer  watershed_orig_output_image = WatershedOutputImageType::New();

	// Here, the input map for WatershedFilter should be a BINARY map!!!
	watershed_filter->SetInput( inputmap );
	watershed_orig_output_image = watershed_filter->GetOutput();

	try
	{
		watershed_filter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "In the ITK Watersheds method segmentation section:" << std::endl;
		std::cerr << "Exception thrown " << excp << std::endl;
	}

	// After getting the Watershed int type labeled output result segmented image, we need to perform
	// type cast to convert it to OutputImageType!!!
	typedef itk::CastImageFilter< WatershedOutputImageType, OutputImageType >	CastingFilterType;

	CastingFilterType::Pointer  watershed_output_caster = CastingFilterType::New();
	watershed_output_caster->SetInput( watershed_orig_output_image );

	// define the final output DOUBLE image pointer and getoutput of the caster:
	OutputImageType::Pointer watershed_caster_output_image = OutputImageType::New();
	watershed_caster_output_image = watershed_output_caster->GetOutput();

	try
	{
		watershed_output_caster->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "In the ITK Watersheds output image casting image type section:" << std::endl;
		std::cerr << "Exception thrown " << excp << std::endl;
	}

	// after getting the final watershed output result, we should perform groundnization, to
	// set all background pixel value to ZERO!
	// and manually binarize thresholding of the output map

	PixelType min_watershed_result_image_value = 1000000;
	PixelType max_watershed_result_image_value = -1000000;

	OutputImageType::Pointer watershed_final_output_image = OutputImageType::New();
	watershed_final_output_image->SetSpacing( watershed_caster_output_image->GetSpacing() );
	watershed_final_output_image->SetOrigin( watershed_caster_output_image->GetOrigin() );
	watershed_final_output_image->SetRegions( watershed_caster_output_image->GetRequestedRegion() );
	watershed_final_output_image->CopyInformation( watershed_caster_output_image );
	watershed_final_output_image->Allocate();

	typedef itk::ImageRegionIteratorWithIndex< InputImageType >		InputIteratorType;
	typedef itk::ImageRegionIteratorWithIndex< OutputImageType >	OutputIteratorType;

	InputIteratorType watershed_caster_output_It( watershed_caster_output_image, watershed_caster_output_image->GetRequestedRegion() );
	InputIteratorType watershed_final_It( watershed_final_output_image, watershed_final_output_image->GetRequestedRegion() );

	// assign original value of the caster output image to the final output image
	// and find the max, min pixel value
	for(watershed_caster_output_It.GoToBegin(), watershed_final_It.GoToBegin(); !watershed_caster_output_It.IsAtEnd(); ++watershed_caster_output_It, ++watershed_final_It)
	{
		InputImageType::IndexType caster_idx = watershed_caster_output_It.GetIndex();
		InputPixelType caster_pix_value = watershed_caster_output_image->GetPixel( caster_idx );
		watershed_final_It.Set( caster_pix_value );

		if(caster_pix_value > max_watershed_result_image_value)
			max_watershed_result_image_value = caster_pix_value;
		if(caster_pix_value < min_watershed_result_image_value)
			min_watershed_result_image_value = caster_pix_value;
	}

	for(watershed_final_It.GoToBegin(); !watershed_final_It.IsAtEnd(); ++watershed_final_It)
	{
		OutputImageType::IndexType final_idx = watershed_final_It.GetIndex();
		OutputPixeltype final_pix_value = watershed_final_output_image->GetPixel( final_idx );

		if(final_pix_value > min_watershed_result_image_value)
			watershed_final_It.Set(1.0);
		else
			watershed_final_It.Set(0.0);
	}

	// so, after this process the final output image is 
	// the variable watershed_final_output_image, which is a binary image

	// using iterator to assign watershed result to output map pointer
	// the actual process: assign watershed_final_output_image voxels value to outputmap
	typedef itk::ImageRegionIterator< InputImageType >	InputRegionIteratorType;
	typedef itk::ImageRegionIterator< OutputImageType >	OutputRegionIteratorType;

	// set properties of outputmap
	outputmap->SetRegions( inputmap->GetRequestedRegion() );
	outputmap->SetSpacing( inputmap->GetSpacing() );
	outputmap->CopyInformation( inputmap );
	outputmap->Allocate( true );

	InputRegionIteratorType  watershed_final_It_region( watershed_final_output_image, watershed_final_output_image->GetRequestedRegion() );
	OutputRegionIteratorType  outputmapIt_region( outputmap, outputmap->GetRequestedRegion() );

	for( watershed_final_It_region.GoToBegin(), outputmapIt_region.GoToBegin(); !watershed_final_It_region.IsAtEnd(); ++watershed_final_It_region )
	{
		outputmapIt_region.Set( watershed_final_It_region.Get() );
		++outputmapIt_region;
	}

} // end function WatershedSegmentDensityProcess


// ==========
// to compute the gradient magnitude density map of input map
extern void GradientMagnitudeFilterMapProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap )
{
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 3 >    InputImageType;
	typedef itk::Image< PixelType, 3 >    OutputImageType;

	typedef itk::GradientMagnitudeImageFilter< InputImageType, OutputImageType > FilterType;
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputmap );

	OutputImageType::Pointer outputmap_old = OutputImageType::New();
	outputmap_old = filter->GetOutput();

	// try update the filter:
	try
	{
		filter->Update();
	}
	catch( itk::ExceptionObject & err )
	{
		std::cerr << "Exception thrown: " << err << std::endl;
	}

	// assign properties of output map to be the same as input map
	outputmap -> SetRegions( inputmap -> GetRequestedRegion() );
	outputmap -> SetSpacing( inputmap -> GetSpacing() );
	outputmap -> CopyInformation( inputmap );
	outputmap -> Allocate( true );

	typedef itk::ImageRegionIterator< InputImageType >	InputIteratorType;
	typedef itk::ImageRegionIterator< OutputImageType >	OutputIteratorType;

	OutputIteratorType outputmap_oldIt( outputmap_old, outputmap_old->GetRequestedRegion() );
	OutputIteratorType outputmapIt( outputmap, outputmap->GetRequestedRegion() );

	for( outputmap_oldIt.GoToBegin(), outputmapIt.GoToBegin(); !outputmap_oldIt.IsAtEnd(); ++outputmap_oldIt )
	{
		outputmapIt.Set( outputmap_oldIt.Get() );
		++outputmapIt;
	}

}

// ==========
// compute out image with pixel value confined in specific value zone
extern void VoxelValuesinDefinedValueRangeImageProcess( itk::Image< DOUBLE, 1 > *inputimage, itk::Image< DOUBLE, 1 > *outputimage,
 DOUBLE lower_range_value,	DOUBLE upper_range_value )
{
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 1 >		InputImageType;
	typedef itk::Image< PixelType, 1 >		OutputImageType;

	// use the iterator with index, loop over all inputimage pixel
	typedef itk::ImageRegionIteratorWithIndex< InputImageType > IteratorWithIndexType;
	IteratorWithIndexType input_imageItwithindex( inputimage, inputimage->GetRequestedRegion() );

	std::cout << std::setprecision(15);

	// a variable to record pixels meeting the condition, confined in input value zone
	unsigned long int pixel_cond_count = 0;

	// the iterator will loop over all inputimage, and then make judgement, whether the pixel value
	// is between the upper and lower boundary value, if true, then print to standard output stream
	for( input_imageItwithindex.GoToBegin(); !input_imageItwithindex.IsAtEnd(); ++input_imageItwithindex )
	{
		InputImageType::IndexType inputimage_idx = input_imageItwithindex.GetIndex();
		PixelType input_image_pix_value = inputimage->GetPixel( inputimage_idx );

		// then the program will make judgement, whether the current pixel value is confined in function's input
		// parameter's boundary value zone, if the current pixel value is within the defined value zone,
		// then do print output current pixel value to standard output stream
		if( (input_image_pix_value - lower_range_value >= 1e-10) && ( input_image_pix_value - upper_range_value <= 1e-10 ) )
		{
			// std::cout << input_image_pix_value << "      ";
			pixel_cond_count++;
			// if( (tmp_count%20) == 0 )
				// std::cout << std::endl;

		}
	}

	// ========= the code of this needs further modification

	// Next, to create a NEW 1D IMAGE, to store pixels with values confined in input parameters zone 
	// (function's input parameters )

	// the starting index of new image to ZERO
	const OutputImageType::IndexType start = {{0}};
	// the size of new image
	const OutputImageType::SizeType size = {{pixel_cond_count}};

	// outputimage region:
	OutputImageType::RegionType output_region;
	output_region.SetSize( size );
	output_region.SetIndex( start );

	// allocate some properties of outputimage pointer, with input paramrters of this function:
	outputimage->SetRegions( output_region );
	outputimage->CopyInformation( inputimage );
	outputimage->Allocate( true );

	// Next, will use the iterator to assign pixel values meeting the condition to new outputimage:
	typedef itk::ImageRegionIteratorWithIndex< OutputImageType >  NewIteratorType;
	NewIteratorType outputimage_It( outputimage, outputimage->GetRequestedRegion() );

	for( input_imageItwithindex.GoToBegin(), outputimage_It.GoToBegin(); !input_imageItwithindex.IsAtEnd(); ++input_imageItwithindex )
	{
		InputImageType::IndexType inputimage_idx = input_imageItwithindex.GetIndex();
		PixelType input_image_pix_value = inputimage->GetPixel( inputimage_idx );

		// then the program will make judgement, whether the current pixel value is confined in function's input
		// parameter's boundary value zone, if the current pixel value is within the defined value zone,
		// then do print output current pixel value to standard output stream
		if( (input_image_pix_value - lower_range_value >= 1e-10) && ( input_image_pix_value - upper_range_value <= 1e-10 )
			&& (!outputimage_It.IsAtEnd()) )
		{
			// std::cout << input_image_pix_value << "      ";
			// pixel_cond_count++;
			outputimage_It.Set( input_image_pix_value );
			++outputimage_It;
			// if( (tmp_count%20) == 0 )
				// std::cout << std::endl;

		}

	}

// debugging part of this function
// #define DEBUG_PRINT_VALUE_FUNCTION
#ifdef DEBUG_PRINT_VALUE_FUNCTION
	std::cerr << "====================" << std::endl;
	std::cerr << std::setprecision(15);
	std::cerr << "In the PrintVoxelValuesinDefinedValueRange function debugging part." << std::endl;
	std::cerr << "Sone information of inputimage:" << std::endl;
	std::cerr << "Size of inputimage: " << ((inputimage->GetRequestedRegion()).GetSize()) << std::endl;
	std::cerr << "Origin of inputimage: " << (inputimage->GetOrigin()) << std::endl;
	InputImageType::IndexType pixel_index = {{201}};
	InputImageType::PixelType pix_value = inputimage->GetPixel(pixel_index);
	std::cerr << "one pixel value of inputimage: " << pix_value << std::endl;
	std::cerr << "--------" << std::endl;
	std::cerr << "Some information of outputimage:" << std::endl;
	std::cerr << "Size of outputimage: " << ((outputimage->GetRequestedRegion()).GetSize()) << std::endl;
	std::cerr << "Origin of outputimage: " << (outputimage->GetOrigin()) << std::endl;
	OutputImageType::IndexType pixel_index_output = {{201}};
	OutputImageType::PixelType pix_value_output = outputimage->GetPixel(pixel_index_output);
	std::cerr << "one pixel value of outputimage: " << pix_value_output << std::endl;
	std::cerr << "--------" << std::endl;
	std::cerr << "some variables' value of this function:" << std::endl;
	std::cerr << "lower_range_value value: " << lower_range_value << std::endl;
	std::cerr << "upper_range_value value: " << upper_range_value << std::endl;
	std::cerr << "pixel_cond_count value: " << pixel_cond_count << std::endl;
	std::cerr << "--------" << std::endl;
	std::cerr << "====================" << std::endl;
#endif

} // end the function PrintVoxelValuesinDefinedValueRange

// ==========
// the function to make addition of two input binary map, and then rescale those voxel values
// of the resulting added map, the output map MUST BE BINARY MAP!!!
extern void BinaryMapsAdditionandThresholdingProcess( itk::Image< DOUBLE,3 > *inputmap_1, itk::Image< DOUBLE,3 > *inputmap_2,
											itk::Image< DOUBLE, 3 > *outputmap )
{
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 3 >		InputImageType;
	typedef itk::Image< PixelType, 3 >		OutputImageType;

	typedef itk::ImageRegionIteratorWithIndex< InputImageType > InputIteratorType;
	typedef itk::ImageRegionIteratorWithIndex< OutputImageType > OutputIteratorType;

	InputIteratorType inputmap_1It( inputmap_1, inputmap_1->GetRequestedRegion() );
	InputIteratorType inputmap_2It( inputmap_2, inputmap_2->GetRequestedRegion() );

	outputmap -> SetRegions( inputmap_1 -> GetRequestedRegion() );
	outputmap -> SetSpacing( inputmap_1 -> GetSpacing() );
	outputmap -> CopyInformation( inputmap_1 );
	outputmap -> Allocate( true );

	OutputIteratorType outputmapIt( outputmap, outputmap->GetRequestedRegion() );

	for( inputmap_1It.GoToBegin(), inputmap_2It.GoToBegin(), outputmapIt.GoToBegin(); !inputmap_1It.IsAtEnd();
			++inputmap_1It, ++inputmap_2It, ++outputmapIt )
	{
		InputImageType::IndexType input_idx_1 = inputmap_1It.GetIndex();
		InputImageType::IndexType input_idx_2 = inputmap_2It.GetIndex();

		PixelType input1_pix_value = inputmap_1->GetPixel( input_idx_1 );
		PixelType input2_pix_value = inputmap_2->GetPixel( input_idx_2 );

		PixelType output_pix_value = input1_pix_value + input2_pix_value;

		// do juegement and assignment value to output map:
		if( output_pix_value - 0.7 >= 1e-10 )
			outputmapIt.Set( 1.0 );
		else
			outputmapIt.Set( 0.0 );
	}

} // end of function BinaryMapsAdditionandThresholdingProcess

// ==========
// the function to invert voxel values of binary map
extern void InvertVoxelValuesBinaryImage( itk::Image< DOUBLE, 3 > *inputimage, itk::Image< DOUBLE, 3 > *outputimage )
{
	typedef DOUBLE PixelType;
	typedef itk::Image< PixelType, 3 >		InputImageType;
	typedef itk::Image< PixelType, 3 >		OutputImageType;

	typedef itk::ImageRegionIteratorWithIndex< InputImageType > InputIteratorType;
	typedef itk::ImageRegionIteratorWithIndex< OutputImageType > OutputIteratorType;

	InputIteratorType inputimageIt( inputimage, inputimage->GetRequestedRegion() );
	// InputIteratorType inputmap_2It( inputmap_2, inputmap_2->GetRequestedRegion() );

	outputimage -> SetRegions( inputimage -> GetRequestedRegion() );
	outputimage -> SetSpacing( inputimage -> GetSpacing() );
	outputimage -> CopyInformation( inputimage );
	outputimage -> Allocate( true );

	OutputIteratorType outputimageIt( outputimage, outputimage->GetRequestedRegion() );

	for( inputimageIt.GoToBegin(), outputimageIt.GoToBegin(); !inputimageIt.IsAtEnd(); ++inputimageIt, ++outputimageIt )
	{
		InputImageType::IndexType input_idx = inputimageIt.GetIndex();
		OutputImageType::IndexType output_idx = outputimageIt.GetIndex();

		// get the value of the corresponding index, and do some process:
		PixelType input_pix_value = inputimage->GetPixel( input_idx );
		PixelType output_pix_value;

		// next is the judgement section:
		if( (input_pix_value - 1.0 >= -1e-6) && (input_pix_value-1.0 <= 1e-6) )
		{
			output_pix_value = 0.0;
		}
		else if( (input_pix_value - 0.0 >= -1e-6) && (input_pix_value - 0.0 <= 1e-6) )
		{
			output_pix_value = 1.0;
		}

		outputimageIt.Set( output_pix_value );
	}

} // end of function InvertVoxelValuesBinaryImage

// ==========
// the function to compute connected regions of input binary density map
// will use the connected component image filter
extern void ConnectedComponentsBinaryImageProcess( itk::Image< DOUBLE, 3 > *inputmap, itk::Image< DOUBLE, 3 > *outputmap,
	bool input_inverted_binary )
{
	// ====
	// the last parameter of this function, the bool variable
	// is useful in the process of rescaling

	typedef DOUBLE 	PixelType;
	typedef itk::Image< DOUBLE, 3 >		InputImageType;
	typedef itk::Image< DOUBLE, 3 >		OutputImageType;

	typedef itk::Image< unsigned long int, 3 >	ConnectedComponentFilterOutputImageType;

	// define the filter:
	typedef itk::ConnectedComponentImageFilter< InputImageType, ConnectedComponentFilterOutputImageType >
					ConnectedComponentImageFilterType;

	ConnectedComponentImageFilterType::Pointer connected_filter = ConnectedComponentImageFilterType::New();

	// set input of the filter:
	connected_filter->SetInput( inputmap );

	// get the output of ConnectedComponentImageFilter:
	ConnectedComponentFilterOutputImageType::Pointer connected_result = ConnectedComponentFilterOutputImageType::New();
	connected_result = connected_filter->GetOutput();

	try
	{
		connected_filter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Exception thrown " << excp << std::endl;
	}

	// After update the connected filter, then we need to define the caster, and then do the casting process
	typedef itk::CastImageFilter< ConnectedComponentFilterOutputImageType, OutputImageType >
				CastingFilterType;
	CastingFilterType::Pointer caster = CastingFilterType::New();
	// =====================NOTICE!!! this section needs further modification and addition!!!

	// get output of the caster:
	OutputImageType::Pointer caster_output = OutputImageType::New();
	caster->SetInput(connected_result);
	caster_output = caster->GetOutput();

	// update the caster:
	try
	{
		caster->Update();
	}
	catch( itk::ExceptionObject & err )
	{
		std::cerr << "Exception thrown " << err << std::endl;
	}

	// in the output image of the caster, voxel values of lebeled connected regions 
	// are 1, 2, 3......, so we need to rescale it to a binary image
	// and VOXELS WITH VALUE 1 shoule also be set to ZERO!!! --> because it is background region before 
	// voxel value inverted of original binary map


	// set some properties of outputmap:
	outputmap -> SetRegions( inputmap -> GetRequestedRegion() );
	outputmap -> SetSpacing( inputmap -> GetSpacing() );
	outputmap -> CopyInformation( inputmap );
	outputmap -> Allocate( true );

	typedef itk::ImageRegionIteratorWithIndex< OutputImageType >  ProcessIteratorType;

	// caster output image and outputmap:
	ProcessIteratorType outputmapIt( outputmap, outputmap->GetRequestedRegion() );
	ProcessIteratorType caster_outputIt( caster_output, caster_output->GetRequestedRegion() );

/*
	outputimage -> SetRegions( inputimage -> GetRequestedRegion() );
	outputimage -> SetSpacing( inputimage -> GetSpacing() );
	outputimage -> CopyInformation( inputimage );
	outputimage -> Allocate( true );
*/

	if(input_inverted_binary)
	{
		// use for loop and also make judgement to set binary value of outputmap
		for( caster_outputIt.GoToBegin(), outputmapIt.GoToBegin(); !caster_outputIt.IsAtEnd(); ++caster_outputIt )
		{
			// get value of caster_output image:
			OutputImageType::IndexType caster_output_idx = caster_outputIt.GetIndex();
			PixelType caster_output_pix_value = caster_output->GetPixel( caster_output_idx );

			if( caster_output_pix_value - 1.0 >= 1e-6 )
			{
				// inside connected region of binary map of original gradient magnitude map
				outputmapIt.Set(1.0);
			}
			else if( (caster_output_pix_value - 1.0 >= -1e-6) && (caster_output_pix_value - 1.0 <= 1e-6) )
			{
				// if the pixel value is EQUAL TO 1.0
				outputmapIt.Set(0.0);
			}
			else
			{
				outputmapIt.Set(0.0);
			}

			// at the end of current iteration, make increment of outputmapIt
			++outputmapIt;
		}
	}


} // end of function ConnectedComponentsBinaryImageProcess


// ==========
// the function to implement connected compnents image filter for input binary image
// to label and make thresholding process, NOT INVERT the binary value
// the aim of this function is to get the largest connected components for generating the final binary mask
extern void ConnectedComponentsLargestBinaryMaskGeneration( itk::Image<DOUBLE, 3> *inputmap,
    itk::Image< DOUBLE, 3 > *outputmap )
{

	typedef DOUBLE 	PixelType;
	typedef itk::Image< DOUBLE, 3 >		InputImageType;
	typedef itk::Image< DOUBLE, 3 >		OutputImageType;

	typedef itk::Image< unsigned long int, 3 >	ConnectedComponentFilterOutputImageType;

	// define the filter:
	typedef itk::ConnectedComponentImageFilter< InputImageType, ConnectedComponentFilterOutputImageType >
					ConnectedComponentImageFilterType;

	ConnectedComponentImageFilterType::Pointer connected_filter = ConnectedComponentImageFilterType::New();

	// set input of the filter:
	connected_filter->SetInput( inputmap );

	// get the output of ConnectedComponentImageFilter:
	ConnectedComponentFilterOutputImageType::Pointer connected_result = ConnectedComponentFilterOutputImageType::New();
	connected_result = connected_filter->GetOutput();

	try
	{
		connected_filter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Exception thrown " << excp << std::endl;
	}

	// After update the connected filter, then we need to use the relabel image filter
	// to find the LARGEST TWO connected components inside the map!!!

	typedef itk::RelabelComponentImageFilter< ConnectedComponentFilterOutputImageType,
					ConnectedComponentFilterOutputImageType > RelabelComponentFilterType;

	RelabelComponentFilterType::Pointer filter_relabel = RelabelComponentFilterType::New();

	filter_relabel->SetInput( connected_result );

	// get the result of relabel filter:
	ConnectedComponentFilterOutputImageType::Pointer relabel_result = ConnectedComponentFilterOutputImageType::New();
	relabel_result = filter_relabel->GetOutput();

	// relabel filter update:
	try
	{
		filter_relabel->Update();
	}
	catch( itk::ExceptionObject & e )
	{
		std::cerr << "Exception thrown " << e << std::endl;
		// return EXIT_FAILURE;
	}




	// After update the connected filter, then we need to define the caster, and then do the casting process
	// WAIT!! this section should be changed its order!!!
	// after the relabeling process!!
	// and the input of the caster image filter should become the output of relabel image filter!!!

	typedef itk::CastImageFilter< ConnectedComponentFilterOutputImageType, OutputImageType >
				CastingFilterType;
	CastingFilterType::Pointer caster = CastingFilterType::New();
	// =====================NOTICE!!! this section needs further modification and addition!!!

	// get output of the caster:
	OutputImageType::Pointer caster_output = OutputImageType::New();
	// caster->SetInput(connected_result);
	caster->SetInput(relabel_result);
	caster_output = caster->GetOutput();

	// update the caster:
	try
	{
		caster->Update();
	}
	catch( itk::ExceptionObject & err )
	{
		std::cerr << "Exception thrown " << err << std::endl;
	}

	// in the output image of the caster, voxel values of lebeled connected regions 
	// are 1, 2, 3......, so we need to rescale it to a binary image
	// and VOXELS OUT OF VALUE ZONE: [1,2] shoule also be set to ZERO!!! --> because it is background region
	// and other noisy region in the input binary map


	// set some properties of outputmap:
	outputmap -> SetRegions( inputmap -> GetRequestedRegion() );
	outputmap -> SetSpacing( inputmap -> GetSpacing() );
	outputmap -> CopyInformation( inputmap );
	outputmap -> Allocate( true );

	typedef itk::ImageRegionIteratorWithIndex< OutputImageType >  ProcessIteratorType;

	// caster output image and outputmap:
	ProcessIteratorType outputmapIt( outputmap, outputmap->GetRequestedRegion() );
	ProcessIteratorType caster_outputIt( caster_output, caster_output->GetRequestedRegion() );

/*
	outputimage -> SetRegions( inputimage -> GetRequestedRegion() );
	outputimage -> SetSpacing( inputimage -> GetSpacing() );
	outputimage -> CopyInformation( inputimage );
	outputimage -> Allocate( true );
*/

	// use for loop and also make judgement to set binary value of outputmap
	for( caster_outputIt.GoToBegin(), outputmapIt.GoToBegin(); !caster_outputIt.IsAtEnd(); ++caster_outputIt )
	{
		// get value of caster_output image:
		OutputImageType::IndexType caster_output_idx = caster_outputIt.GetIndex();
		PixelType caster_output_pix_value = caster_output->GetPixel( caster_output_idx );

		if( (caster_output_pix_value - 1.0 >= -1e-6) && (caster_output_pix_value - 2.0 <= 1e-6) )
		{
			// inside connected region of binary map of original gradient magnitude map
			outputmapIt.Set(1.0);
		}
		else if( (caster_output_pix_value - 0.0 >= -1e-6) && (caster_output_pix_value - 1.0 < -1e-6) )
		{
			// if the pixel value is EQUAL TO 1.0
			outputmapIt.Set(0.0);
		}
		else
		{
			outputmapIt.Set(0.0);
		}

		// at the end of current iteration, make increment of outputmapIt
		++outputmapIt;
	}
	


} // end of function ConnectedComponentsLargestBinaryMaskGeneration


// #endif
