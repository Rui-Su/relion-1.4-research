

#include "/home/pxuser/surui/apps/relion/relion-1.4/src/calcangle_dist.h"

void calcangledistance::read(int argc, char **argv)
{
	parser.setCommandLine(argc, argv);
	// General command line stuff
	int gen_section = parser.addSection("General options");
	fn_in1 = parser.getOption("--in1", "Likelihood result file input rootname, Input images");
	fn_in2 = parser.getOption("--in2", "Always cc result file input rootname");
	fn_out = parser.getOption("--o", "Output rootname", "angulardistances_result");

	// Set the orientations
	int orientations_section = parser.addSection("Orientations");
	// Sampling:
	sampling.fn_sym = parser.getOption("--sym", "Symmetry group", "c1");
}

void calcangledistance::usage()
{
	parser.writeUsage(std::cerr);
}

void calcangledistance::initialise()
{
	// Read in two methods data star files:
	mydata_LL.read(fn_in1, true, false, false);
	mydata_cc.read(fn_in2, true, false, false);
}

void calcangledistance::calculateangulardistances()
{
	long int nr_ori_particles_done = 0;
	long int my_first_ori_particle,  my_last_ori_particle;
	while (nr_ori_particles_done < mydata_cc.numberOfOriginalParticles())
	{
		my_first_ori_particle = nr_ori_particles_done;
		my_last_ori_particle = mydata_cc.numberOfOriginalParticles() - 1;
		//read metadata
		// ?????
		getMetadataSubset(my_first_ori_particle, my_last_ori_particle);
		//calculate distance:
		   // store total number of particle images:
		//exp_nr_images = 0;
		for (long int ori_part_id = my_first_ori_particle; ori_part_id <= my_last_ori_particle; ori_part_id++)
		{
			for (long int ipart = 0; ipart < mydata.ori_particles[ori_part_id].particles_id.size(); ipart++, my_image_no++)
			{
				long int part_id = mydata.ori_particles[ori_part_id].particles_id[ipart];
				DOUBLE rot_LL, tilt_LL, psi_LL;
				DOUBLE rot_cc, tilt_cc, psi_cc;

				rot_LL = DIRECT_A2D_ELEM(exp_metadata_LL, my_image_no, METADATA_ROT);
				tilt_LL = DIRECT_A2D_ELEM(exp_metadata_LL, my_image_no, METADATA_TILT);
				psi_LL = DIRECT_A2D_ELEM(exp_metadata_LL, my_image_no, METADATA_PSI);

				rot_cc = DIRECT_A2D_ELEM(exp_metadata_cc, my_image_no, METADATA_ROT);
				tilt_cc = DIRECT_A2D_ELEM(exp_metadata_cc, my_image_no, METADATA_TILT);
				psi_cc = DIRECT_A2D_ELEM(exp_metadata_cc, my_image_no, METADATA_PSI);

				// image name and micrograph name
				FileName fn_img = "", fn_mic = "";
				mydata_cc.MDimg.getValue(EMDL_IMAGE_NAME, fn_img, part_id);
				mydata_cc.MDimg.getValue(EMDL_MICROGRAPH_NAME, fn_mic, part_id);

				angulardistances = sampling.calculateAngularDistance(rot_LL, tilt_LL, psi_LL, rot_cc, tilt_cc, psi_cc);

				// Next, set the calculated results to the resulting metadata table:
				mydata_results.MDimg.setValue(EMDL_LL_CC_ANGDISTANCE, angulardistances, part_id);
				mydata_results.MDimg.setValue(EMDL_IMAGE_NAME, fn_img, part_id);
				mydata_results.MDimg.setValue(EMDL_MICROGRAPH_NAME, fn_mic, part_id);

				angdist.push_back(angulardistances);
				//exp_nr_images += mydata_cc.ori_particles[ori_part_id].particles_id.size();
			}
		}
		//set calculated results to metadata
		nr_ori_particles_done += my_last_ori_particle - my_first_ori_particle + 1;

	}

}

void calcangledistance::run()
{
	initialise();

	calculateangulardistances();

	writeOutput();
}


void calcangledistance::getMetadataSubset(int first_ori_particle_id, int last_ori_particle_id)
{
	int nr_images = 0;
	for (long int ori_part_id = first_ori_particle_id; ori_part_id <= last_ori_particle_id; ori_part_id++)
	{
		nr_images += mydata_cc.ori_particles[ori_part_id].particles_id.size();
	}

	//angdist.resize(nr_images);
	exp_metadata_LL.initZeros(nr_images, METADATA_LINE_LENGTH);
	exp_metadata_cc.initZeros(nr_images, METADATA_LINE_LENGTH);

	for (long int ori_part_id = first_ori_particle_id, my_image_no = 0; ori_part_id <= last_ori_particle_id; ori_part_id++)
	{
		for (long int ipart = 0; ipart < mydata_cc.ori_particles[ori_part_id].particles_id.size(); ipart++, my_image_no++)
		{
			long int part_id = mydata_cc.ori_particles[ori_part_id].particles_id[ipart];

			// Get the metadata, mainly the 3D orientational angles
			// Always cc:
			mydata_cc.MDimg.getValue(EMDL_ORIENT_ROT, DIRECT_A2D_ELEM(exp_metadata_cc, my_image_no, METADATA_ROT), part_id);
			mydata_cc.MDimg.getValue(EMDL_ORIENT_TILT, DIRECT_A2D_ELEM(exp_metadata_cc, my_image_no, METADATA_TILT), part_id);
			mydata_cc.MDimg.getValue(EMDL_ORIENT_PSI, DIRECT_A2D_ELEM(exp_metadata_cc, my_image_no, METADATA_PSI), part_id);
			// Likelihood:
			mydata_LL.MDimg.getValue(EMDL_ORIENT_ROT, DIRECT_A2D_ELEM(exp_metadata_LL, my_image_no, METADATA_ROT), part_id);
			mydata_LL.MDimg.getValue(EMDL_ORIENT_TILT, DIRECT_A2D_ELEM(exp_metadata_LL, my_image_no, METADATA_TILT), part_id);
			mydata_LL.MDimg.getValue(EMDL_ORIENT_PSI, DIRECT_A2D_ELEM(exp_metadata_LL, my_image_no, METADATA_PSI), part_id);
		}
	}
}

void calcangledistance::writeOutput()
{
	FileName fn_tmp;
	fn_tmp = fn_out + "_compare_results.star";
	mydata_results.write(fn_tmp);
}
