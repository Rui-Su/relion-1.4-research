
#ifndef AUTOCORR_H_
#define AUTOCORR_H_

#include "src/image.h"
#include "src/fftw.h"

class CalculateAutocorrelation
{
public:
	// memeber variables

	IOParser parser;

	// input and output filenames
	FileName fn_in, fn_out;

	Image<DOUBLE> volin, img;

	DOUBLE pixel_size_map;

	// Whether to output intensity of each element of the calculated result map:
	bool do_output_map_intensity;

	// get from the command line option, if is true, then do only maintain norm(Faux) in Fourier Space
	bool do_autocorrelation;

public:
	// member functions

	// read command line options
	void read(int argc, char **argv);

	// print usage instructions:
	void usage();

	void initialise();

	// do compute autocorrelation in Fourier Space and do inverse FFT back to Real Space map
	void ComputeAutoCorrealtion(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &vol_out);

	// write results to outputs
	void WriteOutput();

	// running
	void run();
};

#endif
