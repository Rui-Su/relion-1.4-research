

#include "src/autocorr.h"

void CalculateAutocorrelation::read(int argc, char **argv)
{
	parser.setCommandLine(argc, argv);
	int gen_section = parser.addSection("General options");
	fn_in = parser.getOption("--map_in", "Input map to compute its autocorrelation in the Fourier Space e.g. run001.mrc", "None");
	fn_out = parser.getOption("--output", "Output root name to store resulting real space map", "None");
	pixel_size_map = textToFloat(parser.getOption("--angpix", "Pixel size of the input map"));
	do_output_map_intensity = parser.checkOption("--map_intensity_out", "If this option is given in the command line, then the program will write out the intensity of each element of the result map to a text file");
	do_autocorrelation = parser.checkOption("--autocorrelation", "If this option is given in the command line, then the real autocorrelation operation will be performed in Fourier Space");
}

void CalculateAutocorrelation::usage()
{
	parser.writeUsage(std::cerr);
}

void CalculateAutocorrelation::initialise()
{
	// First read in map for compute autocorrelation:
	volin.read(fn_in);

	int dim_inputmap;
	dim_inputmap = volin().getDim();

	if (dim_inputmap == 3)
		img().resize(ZSIZE(volin()), YSIZE(volin()), XSIZE(volin()));
	else
		img().resize(YSIZE(volin()), XSIZE(volin()));
}


void CalculateAutocorrelation::ComputeAutoCorrealtion(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &vol_out)
{
	vol_in.setXmippOrigin();
	vol_out.setXmippOrigin();

	FourierTransformer transformer;
	MultidimArray<Complex> Faux;

	// translate the map for FFT to put origin of FT in the center: ------------------->>>>  ?????
	//CenterFFT(vol_in, true);

	Faux.resize(ZSIZE(vol_in), YSIZE(vol_in), XSIZE(vol_in)/2 + 1);
	Faux.initZeros();
	Faux.setXmippOrigin();


	transformer.FourierTransform(vol_in, Faux, false);

//#define DEBUG
#ifdef DEBUG
	std::cout << "vol_in: " << STARTINGZ(vol_in) << " " << FINISHINGZ(vol_in) << " " << STARTINGY(vol_in) << " " << FINISHINGY(vol_in) << " " << STARTINGX(vol_in) << " " << FINISHINGX(vol_in) <<"\n";
	std::cout << "Faux: " << STARTINGZ(Faux) << " " << FINISHINGZ(Faux) << " " << STARTINGY(Faux) << " " << FINISHINGY(Faux) << " " << STARTINGX(Faux) << " " << FINISHINGX(Faux) <<"\n";
	Faux.printShape();
	vol_in.printShape();
	vol_out.printShape();
	std::cout << Faux.zinit << " " << Faux.yinit << " " << Faux.xinit << "\n";
#endif
	FOR_ALL_ELEMENTS_IN_FFTW_TRANSFORM(Faux)
	{

		if (do_autocorrelation)
		{
			Complex tmp_FT_intensity;
			tmp_FT_intensity = dAkij(Faux, k, i, j);
			//tmp_FT_intensity = 2 * (dAkij(Faux, k, i, j) * conj(dAkij(Faux, k, i, j)));
			tmp_FT_intensity = (dAkij(Faux, k, i, j) * conj(dAkij(Faux, k, i, j)));
			//tmp_FT_intensity.real = ABS(norm(dAkij(Faux, k, i, j)));
			//tmp_FT_intensity.imag = 0.0;

			//tmp_FT_intensity.real = 1.0;
				//std::cout << tmp_FT_intensity.real << "\n";
			//tmp_FT_intensity.imag = 0.0;
			dAkij(Faux, k, i, j).real = tmp_FT_intensity.real;
			dAkij(Faux, k, i, j).imag = tmp_FT_intensity.imag;
		}


//#define DEBUG_COMPLEX_NUMBER_IN_FOURIER_SPACE
#ifdef DEBUG_COMPLEX_NUMBER_IN_FOURIER_SPACE

		std::cout << dAkij(Faux, k, i, j).real << " " << dAkij(Faux, k, i, j).imag << " " << dAkij(Faux, k, i, j).arg() << "  " << k << " " << i << " " << j << "\n";
		std::cout << Faux(kp, ip, jp).real << " " << Faux(-kp, -ip, -jp).real << std::endl;	
		std::cout << "Faux: " << STARTINGZ(Faux) << " " << FINISHINGZ(Faux) << " " << STARTINGY(Faux) << " " << FINISHINGY(Faux) << " " << STARTINGX(Faux) << " " << FINISHINGX(Faux) <<"\n";
#endif

//#define DEBUG
#ifdef DEBUG
	//std::cout << (A3D_ELEM(Faux,kp, ip, jp).real == A3D_ELEM(Faux,-kp, -ip, -jp).real) << " " << (dAkij(Faux, k, i, j)).arg() << "\n";
		std::cout << kp << " " << ip << " " << jp << "\n";
		std::cout << -kp << " " << -ip << " " << -jp << "\n";
	std::cout << A3D_ELEM(Faux,kp, ip, jp).real << " ";
	std::cout << (dAkij(Faux, k, i, j)).arg() << "\n";	
#endif	
	}

	//Faux.printShape(); // this line of code is just for debugging

	transformer.inverseFourierTransform(Faux, vol_out);
	if (do_autocorrelation)
	{CenterFFT(vol_out, false);}
}

void CalculateAutocorrelation::WriteOutput()
{

	// this time only for the 3d case:
	FileName fn_tmp;
	fn_tmp = fn_out + "_map_autocorrelation_results.mrc";

	img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, pixel_size_map);
	img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, pixel_size_map);
	img.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, pixel_size_map);

	img.write(fn_tmp);

	if(do_output_map_intensity)
	{
		// output each element of the result map to out <<
		std::ofstream fh;

		FileName fn_tmp_out_intensity;
		fn_tmp_out_intensity = fn_out + "_map_elements_intensity.txt";

		fh.open((fn_tmp_out_intensity).c_str(), std::ios::out);

		fh << "#RELION ComputeAutoCorrealtion" <<std::endl;
		fh << "#";

		// Just write command line options as in the header
		parser.writeCommandLine(fh);
		fh << std::endl;

		for (int k = 0; k < ZSIZE(img()); k++)
		{
			for (int i = 0; i < YSIZE(img()); i++)
			{
				for (int j = 0; j < ZSIZE(img()); j++)
				{
					fh << DIRECT_A3D_ELEM(img(), k, i, j) << "\t";
				}//endif x-loop

				fh << std::endl;

			}//endif y-loop

			fh << std::endl;

		}//endif z-loop

		fh << std::endl;


//#define DEBUG_WRITING_INTENSITY_INPUT_MAP
#ifdef DEBUG_WRITING_INTENSITY_INPUT_MAP

		//FOR_ALL_ELEMENTS_IN_ARRAY3D(img())
		//{
		//	DIRECT_A3D_ELEM(img(), k, i, j)
		//}

		// output each element of the result map to out <<
		//CenterFFT(volin(), false);
		//volin().zinit = 0.0;
		//volin().yinit = 0.0;
		//volin().xinit = 0.0;
		//std::cout << "vol_in: " << STARTINGZ(volin()) << " " << FINISHINGZ(volin()) << " " << STARTINGY(volin()) << " " << FINISHINGY(volin()) << " " << STARTINGX(volin()) << " " << FINISHINGX(volin()) <<"\n";
		//volin.write(fn_out + "_origin.mrc");
		std::ofstream fh_vol_out;

		FileName fn_tmp_volout_intensity;
		fn_tmp_volout_intensity = fn_out + "_map_elements_intensity.vol_input.txt";

		fh_vol_out.open((fn_tmp_volout_intensity).c_str(), std::ios::out);

		fh_vol_out << "#RELION ComputeAutoCorrealtion" <<std::endl;
		fh_vol_out << "#";

		// Just write command line options as in the header
		parser.writeCommandLine(fh_vol_out);
		fh_vol_out << std::endl;

		for (int k = 0; k < ZSIZE(volin()); k++)
		{
			for (int i = 0; i < YSIZE(volin()); i++)
			{
				for (int j = 0; j < ZSIZE(volin()); j++)
				{
					fh_vol_out << DIRECT_A3D_ELEM(volin(), k, i, j) << "\t";
				}//endif x-loop

				fh_vol_out << std::endl;

			}//endif y-loop

			fh_vol_out << std::endl;

		}//endif z-loop

		fh_vol_out << std::endl;

	
	
#endif
	}
}



void CalculateAutocorrelation::run()
{
	// only maintain |Amplitude|^2 in Fourier Space:
	ComputeAutoCorrealtion(volin(), img());

	WriteOutput();
}
