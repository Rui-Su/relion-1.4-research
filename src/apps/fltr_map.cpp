#include <src/filtermap.h>
// Aims to use theory of convolution, to convolute the density map with a convolutional kernel,
// and to smooth the map for further processing

int main(int argc, char **argv)
{
	FilterMap_base fltr;
	try
	{
		fltr.read(argc, argv);
		fltr.initialise();
		fltr.run();
	}
	catch (RelionError XE)
	{
		fltr.usage();
		std::cerr << XE;
		exit(1);
	}
	return 0;
}

