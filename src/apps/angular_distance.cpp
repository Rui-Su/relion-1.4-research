#include "/home/pxuser/surui/apps/relion/relion-1.4/src/calcangle_dist.h"
// Aims to calculate the 3D orientational angular distances of the input star files

int main(int argc, char *argv[])
{
	calcangledistance calculate;

	try
	{
		calculate.read(argc, argv);

		calculate.run();
	}

	catch (RelionError XE)
	{
		calculate.usage();
		std::cout << XE;
		exit(1);
	}

	return 0;
}
