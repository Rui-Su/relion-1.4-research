#include <src/autocorr.h>
// Aims to calculate the autocorrelation of the input density map

int main(int argc, char *argv[])
{
	CalculateAutocorrelation autocorr;

	try
	{
		autocorr.read(argc, argv);
		autocorr.initialise();

		autocorr.run();
	}

	catch (RelionError XE)
	{
		autocorr.usage();
		std::cerr << XE;
		exit(1);
	}
	return 0;
}
