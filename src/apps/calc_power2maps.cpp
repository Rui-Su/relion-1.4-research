/*

this file of code aims to calculate the power spectrum of two input density map, and then write them out to corresponding STAR files
and also do rescale power spectrum of input two density maps to the same level
*/

#include <src/calc_powers.h>

int main(int argc, char **argv)
{
	calculate_Power prm;

	try
	{
		prm.read(argc, argv);

		prm.calcinitialise();

		prm.run();
	}

	catch (RelionError XE)
	{
		prm.usage();
		std::cout << XE;
		exit(1);
	}

	return 0;
}

