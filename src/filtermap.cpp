
#include "src/filtermap.h"

void FilterMap_base::read(int argc, char **argv)
{

	parser.setCommandLine(argc, argv);
	int gen_section = parser.addSection("General options");
	fn_in = parser.getOption("--i", "Input density map in mrc file format, e.g. run1.mrc");
	fn_out = parser.getOption("--o", "Output result rootname for mrc dendity map", "filtering_result"); // convolute result
	pixel_size = textToFloat(parser.getOption("--angpix", "Pixel size in Angstrom"));
	conv_kernel_radius = textToInteger(parser.getOption("--radius", "The radius(in pixel) of the convolutional filtering kernel", "3"));  //++++++++++++++
	calc_fft_convkernel_power = parser.checkOption("--compute_fft_kernel","whether to compute power spectrum of the convolution kernel");
	//++++++ to smooth the density map for reduce noise ++++++
	calc_average_smooth = parser.checkOption("--average_filter", "whether to soomth density map using average smoothing filter method");
	calc_gauss_smooth = parser.checkOption("--gauss_filter", "whether to smooth density map using gaussian filter method");
	gauss_kernel_sigma = textToFloat(parser.getOption("--gauss_sigma", "standard deviation of the gaussian smooth filter kernel", "-1."));
	//++++++ next to judge whether to detect the edge surface 
	do_Sobel_computation = parser.checkOption("--sobel_edge", "If this parameter is given, then the 3D sobel operator will implemented to the input density map for edge detection");
	do_Prewitt_computation = parser.checkOption("--prewitt_edge", "If this parameter is given, then Prewitt operator will be implemented to the density map");

	//++++++ Judge whether to use the ITK library to perform smoothing to the density map:
	do_use_itk_smooth = parser.checkOption("--itk_smooth", "If this parameter is given, then the program will use the extra ITK library to smooth the density map");
	do_itk_gauss_smooth = parser.checkOption("--itk_gauss_smooth", "If this parameter is given, then will perform the ITK gaussian smoothing filter");
	itk_gauss_width = textToInteger(parser.getOption("--itk_gauss_width", "Radius of ITK Gaussian smoothing kernel", "3"));
	itk_gauss_sigma = textToFloat(parser.getOption("--itk_gauss_sigma", "standard deviation of the gaussian smooth filter kernel", "1."));

}

void FilterMap_base::usage()
{
	parser.writeUsage(std::cerr);
}

void FilterMap_base::initialise()
{
	// read in the input density map:
	I_in.read(fn_in);

	int dim_inputdensity_map = I_in().getDim();

	if (dim_inputdensity_map == 3)
	{
		I_result().resize(ZSIZE(I_in()), YSIZE(I_in()), XSIZE(I_in()));
		I_result().initZeros(ZSIZE(I_in()), YSIZE(I_in()), XSIZE(I_in()));
	}
	else
		REPORT_ERROR("The input density must be 3 dimension!!!");
}



//+++++++++++++++++++++++++++++++++++ the subsequent function needs further revised!!!!

void FilterMap_base::ComputeFiltermap(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &vol_out, int &kernel_radius)
{
	MultidimArray<DOUBLE> convolute_kernel;	

	// get the x,y,z dim of input density map:
	// int zdim = ZSIZE(vol_in);
	// int ydim = YSIZE(vol_in);
	// int xdim = XSIZE(vol_in);

	// // the coordinate of center of the input density map
	// int center_z = FLOOR(zdim/2);
	// int center_y = FLOOR(ydim/2);
	// int center_x = FLOOR(xdim/2);

	convolute_kernel.initZeros(ZSIZE(vol_in), YSIZE(vol_in), XSIZE(vol_in));

	convolute_kernel.zinit = convolute_kernel.yinit = convolute_kernel.xinit = 0;
	//++++++++
	// this time, we have already initialised zeros of the convolution kernel

	if (calc_average_smooth)
	{
		GetAverageSmoothFilterKernel(convolute_kernel, kernel_radius);
	}
	else if (calc_gauss_smooth)
	{
		GetGaussianFilterKernel(convolute_kernel, kernel_radius, gauss_kernel_sigma);
	}
	else
	{  // if in the exception condition, then report error
		REPORT_ERROR("You must specify which kind of smoothing method to perform to the input dendity map!!!");
	}


	// First, to generate a real space convolute kernel
	// this time,try average smoothing filter, using 1.0 in a user defined circle with a radius
	// DOUBLE r_square;
	// FOR_ALL_ELEMENTS_IN_ARRAY3D(convolute_kernel)
	// {
	// 	r_square = (DOUBLE)((k - center_z)*(k - center_z) + (i - center_y)*(i - center_y) + (j - center_x)*(j - center_x));

	// 	if(r_square <= (DOUBLE)(kernel_radius * kernel_radius))
	// 	{
	// 		A3D_ELEM(convolute_kernel, k, i, j) = 1.0;
	// 	}
	// }

	// // Next, perform the normalize step of the convolute kernel array:
	// DOUBLE conv_kernel_sum = 0;
	// FOR_ALL_ELEMENTS_IN_ARRAY3D(convolute_kernel)
	// {
	// 	conv_kernel_sum = conv_kernel_sum + A3D_ELEM(convolute_kernel, k, i, j);
	// }

	// convolute_kernel = convolute_kernel / conv_kernel_sum;


	// then perform convolution in Fourier Space:
	MultidimArray<Complex> Faux_inmap, Faux_kernel, Faux_filtered;
	FourierTransformer transformer_inmap, transformer_kernel;

//#define DEBUG
#ifdef DEBUG
std::cout << "size of input density map: " << "\n"; vol_in.printShape();
std::cout << "size of output density map: " << "\n"; vol_out.printShape();
std::cout << "size of convolute kernel:" << "\n"; convolute_kernel.printShape();
#endif
	vol_in.setXmippOrigin();
	vol_out.setXmippOrigin();
	convolute_kernel.setXmippOrigin();
	Faux_inmap.setXmippOrigin(), Faux_kernel.setXmippOrigin();

	transformer_inmap.FourierTransform(vol_in, Faux_inmap, false);
	transformer_kernel.FourierTransform(convolute_kernel, Faux_kernel, false);

	// do filter procedure in Fourier Space, this is simply a multiplt operation:
	Faux_filtered.resize(ZSIZE(Faux_kernel), YSIZE(Faux_kernel), XSIZE(Faux_kernel));
	//Faux_filtered.setXmippOrigin();

	Faux_filtered = Faux_kernel * Faux_inmap;

	transformer_kernel.inverseFourierTransform(Faux_filtered, vol_out);
	CenterFFT(vol_out, false);

#ifdef DEBUG
Faux_inmap.printShape();
Faux_filtered.printShape();
Faux_kernel.printShape();
#endif
//vol_out.printShape();
//	vol_out.setXmippOrigin();
//vol_out.printShape();
	// vol_out = vol_out *128.0*128.0*128.0;

	// do compute power spctrum of convolution kernel:
	if(calc_fft_convkernel_power)
	{
		ComputeKernelpower(convolute_kernel, convkernel_power_spectrum);
	}

}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// To convert the initialised convolution kernel to average smoothing kernel
void FilterMap_base::GetAverageSmoothFilterKernel(MultidimArray<DOUBLE> &average_smoothing_filter_kernel, int &kernel_radius)
{
	DOUBLE radi_square;

	bool odd_cube_dim;

	// if ((ZSIZE(average_smoothing_filter_kernel)%2) == 0)
	// 	odd_cube_dim = false;
	// else if ((ZSIZE(average_smoothing_filter_kernel)%2) != 0)
	// 	odd_cube_dim = true;
	DOUBLE center_array_z, center_array_y, center_array_x;

	if(ZSIZE(average_smoothing_filter_kernel)%2 == 0)
	{
		center_array_z = DOUBLE( ((DOUBLE)(ZSIZE(average_smoothing_filter_kernel)-0)) / 2);
		center_array_y = DOUBLE( ((DOUBLE)(YSIZE(average_smoothing_filter_kernel)-0)) / 2);
		center_array_x = DOUBLE( ((DOUBLE)(XSIZE(average_smoothing_filter_kernel)-0)) / 2);
	}
	else
	{
		center_array_z = DOUBLE( ((DOUBLE)(ZSIZE(average_smoothing_filter_kernel)-1)) / 2);
		center_array_y = DOUBLE( ((DOUBLE)(YSIZE(average_smoothing_filter_kernel)-1)) / 2);
		center_array_x = DOUBLE( ((DOUBLE)(XSIZE(average_smoothing_filter_kernel)-1)) / 2);
	}

	FOR_ALL_ELEMENTS_IN_ARRAY3D(average_smoothing_filter_kernel)
	{
		radi_square = (DOUBLE)((k - center_array_z) * (k - center_array_z) + (i - center_array_y) * (i - center_array_y) + (j - center_array_x) * (j - center_array_x));

		if (radi_square <= (DOUBLE)(kernel_radius * kernel_radius))
			{
				A3D_ELEM(average_smoothing_filter_kernel, k, i, j) = 1.0;
			}
	}

	DOUBLE smooth_kernel_sum = 0.;
	FOR_ALL_ELEMENTS_IN_ARRAY3D(average_smoothing_filter_kernel)
	{
		smooth_kernel_sum = smooth_kernel_sum + A3D_ELEM(average_smoothing_filter_kernel, k, i, j);
	}

	average_smoothing_filter_kernel = average_smoothing_filter_kernel / smooth_kernel_sum;

}

// To convert the initialised convolution kernel to Gaussian smoothing kernel
void FilterMap_base::GetGaussianFilterKernel(MultidimArray<DOUBLE> &gauss_smoothing_filter_kernel, int &kernel_radius, DOUBLE sigma)
{
	DOUBLE radi_square;
	DOUBLE _sigma; // function inside variable
	DOUBLE smooth_kernel_sum = 0.;

	DOUBLE center_array_z, center_array_y, center_array_x;

	if((ZSIZE(gauss_smoothing_filter_kernel)-0)%2 == 0)
	{
		center_array_z = DOUBLE( ((DOUBLE) (ZSIZE(gauss_smoothing_filter_kernel)-0)) / 2);
		center_array_y = DOUBLE( ((DOUBLE) (YSIZE(gauss_smoothing_filter_kernel)-0)) / 2);
		center_array_x = DOUBLE( ((DOUBLE) (XSIZE(gauss_smoothing_filter_kernel)-0)) / 2);
	}
	else
	{
		center_array_z = DOUBLE( ((DOUBLE) (ZSIZE(gauss_smoothing_filter_kernel)-1)) / 2);
		center_array_y = DOUBLE( ((DOUBLE) (YSIZE(gauss_smoothing_filter_kernel)-1)) / 2);
		center_array_x = DOUBLE( ((DOUBLE) (XSIZE(gauss_smoothing_filter_kernel)-1)) / 2);
	}

	if (sigma < 0.)
		{ _sigma = 0.3*((2*(DOUBLE)kernel_radius - 1) * 0.5 -1 ) + 0.8; }
	else
		{ _sigma = sigma; }

	FOR_ALL_ELEMENTS_IN_ARRAY3D(gauss_smoothing_filter_kernel)
	{
		radi_square = (DOUBLE)((k - center_array_z) * (k - center_array_z) + (i - center_array_y) * (i - center_array_y) + (j - center_array_x) * (j - center_array_x));

		if (radi_square <= (DOUBLE)(kernel_radius * kernel_radius))
			{
				A3D_ELEM(gauss_smoothing_filter_kernel, k, i, j) = exp(-1.*radi_square/(2.*_sigma*_sigma));
			}
		smooth_kernel_sum = smooth_kernel_sum + A3D_ELEM(gauss_smoothing_filter_kernel, k, i, j);
	}
	gauss_smoothing_filter_kernel = gauss_smoothing_filter_kernel / smooth_kernel_sum;


}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// the subsequent function aims to perform Sobel operation to input density map:
void FilterMap_base::SobelOperationtoDensityMap(MultidimArray<DOUBLE> &input_map, MultidimArray<DOUBLE> &output_map)
{
	// First, set THREE zero kernel array to compute gradient of the input density map in THREE direction:
	MultidimArray<DOUBLE> kernel_dx, kernel_dy, kernel_dz;
	kernel_dx.initZeros(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	kernel_dy.initZeros(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	kernel_dz.initZeros(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));

	// set the starting coordinates to ZEROS:
	kernel_dx.zinit = kernel_dx.yinit = kernel_dx.xinit = 0;
	kernel_dy.zinit = kernel_dy.yinit = kernel_dy.xinit = 0;
	kernel_dz.zinit = kernel_dz.yinit = kernel_dz.xinit = 0;

	// Then, get the central coordinates of the input map:
	int center_array_z, center_array_y, center_array_x;
	if(ZSIZE(input_map)%2 == 0)
	{
		center_array_z = int((ZSIZE(input_map)-0)/ 2);
		center_array_y = int((YSIZE(input_map)-0)/ 2);
		center_array_x = int((XSIZE(input_map)-0)/ 2);
	}
	else
	{
		center_array_z = int((ZSIZE(input_map)-1)/ 2);
		center_array_y = int((YSIZE(input_map)-1)/ 2);
		center_array_x = int((XSIZE(input_map)-1)/ 2);
	}

	// the kernel in the case of xorder=1, yorder=0, zorder=0
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-0, center_array_x-1) = 2.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y+1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-1, center_array_x-0) = 2.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-0, center_array_x-0) = 4.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y+1, center_array_x-0) = 2.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-1, center_array_x+1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-0, center_array_x+1) = 2.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y+1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-0, center_array_x-1) = -2.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-1, center_array_x-0) = -2.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-0, center_array_x-0) = -4.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y+1, center_array_x-0) = -2.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-0, center_array_x+1) = -2.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y+1, center_array_x+1) = -1.0;

	// the kernel in the case of xorder=0, yorder=1, zorder=0
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-0, center_array_x-1) = 2.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y+1, center_array_x-1) = 1.0;

	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-0, center_array_x+1) = -2.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y+1, center_array_x+1) = -1.0;

	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-1, center_array_x-1) = 2.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-0, center_array_x-1) = 4.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y+1, center_array_x-1) = 2.0;

	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-1, center_array_x+1) = -2.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-0, center_array_x+1) = -4.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y+1, center_array_x+1) = -2.0;

	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-0, center_array_x-1) = 2.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y+1, center_array_x-1) = 1.0;

	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-0, center_array_x+1) = -2.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y+1, center_array_x+1) = -1.0;

	// the kernel in the case of xorder=0, yorder=0, zorder=1
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y-1, center_array_x-0) = 2.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y-1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y+1, center_array_x-0) = -2.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y+1, center_array_x+1) = -1.0;

	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y-1, center_array_x-1) = 2.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y-1, center_array_x-0) = 4.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y-1, center_array_x+1) = 2.0;

	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y+1, center_array_x-1) = -2.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y+1, center_array_x-0) = -4.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y+1, center_array_x+1) = -2.0;

	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y-1, center_array_x-0) = 2.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y-1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y+1, center_array_x-0) = -2.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y+1, center_array_x+1) = -1.0;

	// Then compute multiplication operation of the above THREE kernel in Fourier Space:
	MultidimArray<Complex> Faux_inmap;
	FourierTransformer transformer_inmap;
	MultidimArray<Complex> Fourier_kernel_dx, Fourier_kernel_dy, Fourier_kernel_dz;
	FourierTransformer transformer_dx, transformer_dy, transformer_dz;

	input_map.setXmippOrigin(), output_map.setXmippOrigin();
	kernel_dx.setXmippOrigin(), kernel_dy.setXmippOrigin(), kernel_dz.setXmippOrigin();
		// get the Fourier Space density map and gradient kernel:
	transformer_inmap.FourierTransform(input_map, Faux_inmap, false);
	transformer_dx.FourierTransform(kernel_dx, Fourier_kernel_dx, false);
	transformer_dy.FourierTransform(kernel_dy, Fourier_kernel_dy, false);
	transformer_dz.FourierTransform(kernel_dz, Fourier_kernel_dz, false);

	MultidimArray<Complex> Fourier_gradient_x = Fourier_kernel_dx * Faux_inmap;
	MultidimArray<Complex> Fourier_gradient_y = Fourier_kernel_dy * Faux_inmap;
	MultidimArray<Complex> Fourier_gradient_z = Fourier_kernel_dz * Faux_inmap;

	MultidimArray<DOUBLE> Real_gradient_x, Real_gradient_y, Real_gradient_z;
	Real_gradient_x.resize(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	Real_gradient_y.resize(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	Real_gradient_z.resize(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	Real_gradient_x.setXmippOrigin(), Real_gradient_y.setXmippOrigin(), Real_gradient_z.setXmippOrigin();

	transformer_dx.inverseFourierTransform(Fourier_gradient_x, Real_gradient_x);
	transformer_dy.inverseFourierTransform(Fourier_gradient_y, Real_gradient_y);
	transformer_dz.inverseFourierTransform(Fourier_gradient_z, Real_gradient_z);
	CenterFFT(Real_gradient_x, false), CenterFFT(Real_gradient_y, false), CenterFFT(Real_gradient_z, false);

	transformer_dx.clear(), transformer_dy.clear(), transformer_dz.clear();

	//++ Finally, calculate the output= sqrt(dx^2+dy^2+dz^2)
	FOR_ALL_ELEMENTS_IN_ARRAY3D(output_map)
	{
		A3D_ELEM(output_map, k, i, j) = sqrt(A3D_ELEM(Real_gradient_x, k, i, j) * A3D_ELEM(Real_gradient_x, k, i, j)
											+ A3D_ELEM(Real_gradient_y, k, i, j) * A3D_ELEM(Real_gradient_y, k, i, j)
											+ A3D_ELEM(Real_gradient_z, k, i, j) * A3D_ELEM(Real_gradient_z, k, i, j));
	}

}

// the following function aims to perform Prewitt operation to input density map
void FilterMap_base::PrewittOperationtoDensityMap(MultidimArray<DOUBLE> &input_map, MultidimArray<DOUBLE> &output_map)
{
	// First, set THREE zero kernel array to compute gradient of the input density map in THREE direction:
	MultidimArray<DOUBLE> kernel_dx, kernel_dy, kernel_dz;
	kernel_dx.initZeros(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	kernel_dy.initZeros(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	kernel_dz.initZeros(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));

	// set the starting coordinates to ZEROS:
	kernel_dx.zinit = kernel_dx.yinit = kernel_dx.xinit = 0;
	kernel_dy.zinit = kernel_dy.yinit = kernel_dy.xinit = 0;
	kernel_dz.zinit = kernel_dz.yinit = kernel_dz.xinit = 0;

	// Then, get the central coordinates of the input map:
	int center_array_z, center_array_y, center_array_x;
	if(ZSIZE(input_map)%2 == 0)
	{
		center_array_z = int((ZSIZE(input_map)-0)/ 2);
		center_array_y = int((YSIZE(input_map)-0)/ 2);
		center_array_x = int((XSIZE(input_map)-0)/ 2);
	}
	else
	{
		center_array_z = int((ZSIZE(input_map)-1)/ 2);
		center_array_y = int((YSIZE(input_map)-1)/ 2);
		center_array_x = int((XSIZE(input_map)-1)/ 2);
	}

	// the kernel in the case of xorder=1, yorder=0, zorder=0
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-0, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y+1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-1, center_array_x-0) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-0, center_array_x-0) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y+1, center_array_x-0) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-1, center_array_x+1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y-0, center_array_x+1) = 1.0;
	A3D_ELEM(kernel_dx, center_array_z-1, center_array_y+1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-0, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-1, center_array_x-0) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-0, center_array_x-0) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y+1, center_array_x-0) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y-0, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dx, center_array_z+1, center_array_y+1, center_array_x+1) = -1.0;

	// the kernel in the case of xorder=0, yorder=1, zorder=0
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-0, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y+1, center_array_x-1) = 1.0;

	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y-0, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z-1, center_array_y+1, center_array_x+1) = -1.0;

	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-0, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y+1, center_array_x-1) = 1.0;

	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y-0, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z-0, center_array_y+1, center_array_x+1) = -1.0;

	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-0, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y+1, center_array_x-1) = 1.0;

	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-1, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y-0, center_array_x+1) = -1.0;
	A3D_ELEM(kernel_dy, center_array_z+1, center_array_y+1, center_array_x+1) = -1.0;

	// the kernel in the case of xorder=0, yorder=0, zorder=1
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y-1, center_array_x-0) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y-1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y+1, center_array_x-0) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z-1, center_array_y+1, center_array_x+1) = -1.0;

	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y-1, center_array_x-0) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y-1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y+1, center_array_x-0) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z-0, center_array_y+1, center_array_x+1) = -1.0;

	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y-1, center_array_x-1) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y-1, center_array_x-0) = 1.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y-1, center_array_x+1) = 1.0;

	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y+1, center_array_x-1) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y+1, center_array_x-0) = -1.0;
	A3D_ELEM(kernel_dz, center_array_z+1, center_array_y+1, center_array_x+1) = -1.0;

	// Then compute multiplication operation of the above THREE kernel in Fourier Space:
	MultidimArray<Complex> Faux_inmap;
	FourierTransformer transformer_inmap;
	MultidimArray<Complex> Fourier_kernel_dx, Fourier_kernel_dy, Fourier_kernel_dz;
	FourierTransformer transformer_dx, transformer_dy, transformer_dz;

	input_map.setXmippOrigin(), output_map.setXmippOrigin();
	kernel_dx.setXmippOrigin(), kernel_dy.setXmippOrigin(), kernel_dz.setXmippOrigin();
		// get the Fourier Space density map and gradient kernel:
	transformer_inmap.FourierTransform(input_map, Faux_inmap, false);
	transformer_dx.FourierTransform(kernel_dx, Fourier_kernel_dx, false);
	transformer_dy.FourierTransform(kernel_dy, Fourier_kernel_dy, false);
	transformer_dz.FourierTransform(kernel_dz, Fourier_kernel_dz, false);

	MultidimArray<Complex> Fourier_gradient_x = Fourier_kernel_dx * Faux_inmap;
	MultidimArray<Complex> Fourier_gradient_y = Fourier_kernel_dy * Faux_inmap;
	MultidimArray<Complex> Fourier_gradient_z = Fourier_kernel_dz * Faux_inmap;

	MultidimArray<DOUBLE> Real_gradient_x, Real_gradient_y, Real_gradient_z;
	Real_gradient_x.resize(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	Real_gradient_y.resize(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	Real_gradient_z.resize(ZSIZE(input_map), YSIZE(input_map), XSIZE(input_map));
	Real_gradient_x.setXmippOrigin(), Real_gradient_y.setXmippOrigin(), Real_gradient_z.setXmippOrigin();

	transformer_dx.inverseFourierTransform(Fourier_gradient_x, Real_gradient_x);
	transformer_dy.inverseFourierTransform(Fourier_gradient_y, Real_gradient_y);
	transformer_dz.inverseFourierTransform(Fourier_gradient_z, Real_gradient_z);
	CenterFFT(Real_gradient_x, false), CenterFFT(Real_gradient_y, false), CenterFFT(Real_gradient_z, false);

	transformer_dx.clear(), transformer_dy.clear(), transformer_dz.clear();

	//++ Finally, calculate the output= sqrt(dx^2+dy^2+dz^2)
	FOR_ALL_ELEMENTS_IN_ARRAY3D(output_map)
	{
		A3D_ELEM(output_map, k, i, j) = sqrt(A3D_ELEM(Real_gradient_x, k, i, j) * A3D_ELEM(Real_gradient_x, k, i, j)
											+ A3D_ELEM(Real_gradient_y, k, i, j) * A3D_ELEM(Real_gradient_y, k, i, j)
											+ A3D_ELEM(Real_gradient_z, k, i, j) * A3D_ELEM(Real_gradient_z, k, i, j));
	}

}

// At last output the computed resulting filtered density map to target path
void FilterMap_base::WriteOutput()
{
	FileName fn_tmp;

	fn_tmp = fn_out + "_result.mrc";

	I_result.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_X, pixel_size);
	I_result.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Y, pixel_size);
	I_result.MDMainHeader.setValue(EMDL_IMAGE_SAMPLINGRATE_Z, pixel_size);

	I_result.write(fn_tmp);

	if(calc_fft_convkernel_power)
	{
		std::ofstream fh_convpower;
		FileName fn_convpower = fn_out + "_convolute_kernel_power.star";

		fh_convpower.open((fn_convpower).c_str(), std::ios::out);

		fh_convpower << "# Relion compute power spectrum of convolution kernel" << std::endl;
		fh_convpower << "#";
		parser.writeCommandLine(fh_convpower);

		MetaDataTable MDpower_convkernel;
		MDpower_convkernel.clear();
		MDpower_convkernel.setName("model_convolution_kernel_power_spectrum");

		for (int ii=0; ii < XSIZE(convkernel_power_spectrum); ii++)
		{
			MDpower_convkernel.addObject();
			MDpower_convkernel.setValue(EMDL_SPECTRAL_IDX, ii);
			MDpower_convkernel.setValue(EMDL_RESOLUTION, (DOUBLE)ii/(pixel_size * XSIZE(convkernel_power_spectrum)));
			MDpower_convkernel.setValue(EMDL_RESOLUTION_ANGSTROM, (ii==0)? 999. :(pixel_size * XSIZE(convkernel_power_spectrum))/(DOUBLE)ii );
			MDpower_convkernel.setValue(EMDL_MLMODEL_TAU2_REF, convkernel_power_spectrum(ii));
		}
		MDpower_convkernel.write(fh_convpower);
	}
}

// compute power spectrum of the convolution kernel
void FilterMap_base::ComputeKernelpower(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &power_spectrum)
{
	DOUBLE normfft_convkernel = (DOUBLE)((XSIZE(vol_in)) * (XSIZE(vol_in)));

	getSpectrum(vol_in, power_spectrum, POWER_SPECTRUM);
	power_spectrum *= normfft_convkernel / 2;
}

//+++++++++++++++++++++++++++++++++++++++++
// the next function will implement the external ITK library to process 3D density map, a TESTING FUNCTION:

void FilterMap_base::ITKTestingProcessingFunction(MultidimArray<DOUBLE> &vol_in, MultidimArray<DOUBLE> &vol_out)
{

	//++++++++++++++++++++++++++
	// First, starting an ITKImage, Creating and initialise:
	typedef DOUBLE PixelType;
	const unsigned int Dimension = 3; // because the input density map is 3 dimensional
	typedef itk::Image< PixelType, Dimension > ImageType;
	typedef itk::Image< PixelType, Dimension > OutputImageType;
	// then creating the image object:
	ImageType::Pointer image = ImageType::New();
	OutputImageType::Pointer output_image = OutputImageType::New();

	// set the image size to be equal to the size of input density map along the three dimension:
	const ImageType::SizeType size = {{ XSIZE(vol_in), YSIZE(vol_in), ZSIZE(vol_in)}};
	// set the First Index on the itkImage {X,Y,Z}
	const ImageType::IndexType start = {{ 0, 0, 0 }};

	// set the size and start index of the 3D image
	ImageType::RegionType region;
	region.SetSize( size );
	region.SetIndex( start );

	// Pixel data allocated:
	image->SetRegions( region );
	image->Allocate(true);  // initialise buffer to zero

	// Set the spacing information:
	ImageType::SpacingType spacing;

	spacing[0] = spacing[1] = spacing[2] = pixel_size;
	image->SetSpacing( spacing );

	//++++++++++++++++++++++++++
	// Second, Assignment of the value of input density map to the 3D image
	// plan to use the iterator method to assign value of density map to 3D image:
	typedef itk::ImageRegionIteratorWithIndex< ImageType > IteratorType;
	//IteratorType image_it( image, image->GetRequestedRegion() );
	IteratorType image_it( image, region );

	// set the inital coordinate of the three dim of input density map to ZERO:
	vol_in.zinit = vol_in.yinit = vol_in.xinit = 0;

#define CHECK_ORIGIN
#ifdef CHECK_ORIGIN
std::cerr << "starting coordinate of vol_in:" << "starting z: " 
		<< STARTINGZ(vol_in) << " " << "starting y: "
		<< STARTINGY(vol_in) << " " << "starting x: "
		<< STARTINGX(vol_in) << std::endl;
#endif

#define DEBUG_ASSIGN
#ifdef DEBUG_ASSIGN
	int count = 0;
#endif

	for(image_it.GoToBegin(); !image_it.IsAtEnd(); ++image_it)
	{
		ImageType::IndexType idx = image_it.GetIndex(); // get the index of the current location of the iterator
		int idx_x = (int)(idx[0]); // x index
		int idx_y = (int)(idx[1]); // y index
		int idx_z = (int)(idx[2]); // z index


#ifdef DEBUG_ASSIGN
		count++;
	if(count > 700 && count < 1000)
	{
		std::cerr << "index of the image_it: " << idx << " "
				<< "orininal value: " << image_it.Get() << std::endl;
		std::cerr << " value of the input map: " << A3D_ELEM(vol_in, idx_z, idx_y, idx_x) << std::endl;
	}
#endif
		// assign the value of input density map to the 3D image:
		// do multiply 1e10 to avoid too small value in the density map
		image_it.Set( A3D_ELEM(vol_in, idx_z, idx_y, idx_x) ); //* 1e10 ); -> the commented code has problem?

#ifdef DEBUG_ASSIGN
		if(count > 700 && count < 1000)
		{
			std::cerr << "value after assignment: " << image_it.Get();
		}
#endif

	}

	//++++++++++++++++++++++++++
	// Third, take advantage of mehods inside the ITK library for subsequent processing of 3D image:
	/*
		smoothing the input image, edge detection, transform to a binary image ......
	*/
	// 1. smoothing methods implemented: Gaussian, Bilateral or Gradient Anisotropic Diffusion:

	// Gaussian:
	// do Gaussian smooth to the density map:
	if(do_itk_gauss_smooth)
	{
		typedef itk::DiscreteGaussianImageFilter<
					ImageType, OutputImageType >      FilterType;

		FilterType::Pointer filter = FilterType::New();
		const double gaussianVariance = itk_gauss_sigma* itk_gauss_sigma;

		filter->SetVariance( gaussianVariance );
		filter->SetMaximumKernelWidth( itk_gauss_width );

		filter->SetInput( image );
		output_image = filter->GetOutput();


		// updating filter and writer section:
		try
		{
			filter->Update();
		}
		catch( itk::ExceptionObject & excp )
	    {
	    	std::cerr << "Exception thrown " << excp << std::endl;
	    }

	    // after invoking the Gaussian filter, the output image needs to
	    // be assigned to Multidimarray format density map through iterator:

	    // creation of the output iterator:
	    typedef itk::ImageRegionIteratorWithIndex< OutputImageType > OutputIteratorType;
	    OutputIteratorType outputIt( output_image, output_image->GetRequestedRegion() );

	    // loop to assign output image to multidimarray:
	    OutputImageType::IndexType outputidx;
	    for (outputIt.GoToBegin(); !outputIt.IsAtEnd(); ++outputIt)
	    {
	    	outputidx = outputIt.GetIndex();
	    	int idx_x = (int)(outputidx[0]); // x index
	    	int idx_y = (int)(outputidx[1]); // y index
	    	int idx_z = (int)(outputidx[2]); // z index

	    	A3D_ELEM(vol_out, idx_z, idx_y, idx_x) = output_image->GetPixel(outputidx);
	    }
	}

	// Bilateral:

	// Or Gradient Anisotropic Diffusion:

	//++++++++++++++++++++++++++

}

//+++++++++++++++++++++++++++++++++++++++++

void FilterMap_base::run()
{
	if(!do_use_itk_smooth)
	{
		if(calc_average_smooth || calc_gauss_smooth)
		{
			ComputeFiltermap(I_in(), I_result(), conv_kernel_radius);
		}
		else if(do_Sobel_computation)
		{
			SobelOperationtoDensityMap(I_in(), I_result());
		}
		else if(do_Prewitt_computation)
		{
			PrewittOperationtoDensityMap(I_in(), I_result());
		}

	}
	else if(do_use_itk_smooth)
	{
		ITKTestingProcessingFunction(I_in(), I_result());
	}

	WriteOutput();

}
